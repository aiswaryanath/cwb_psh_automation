--Pre queries MA:
--FX Spot RATE RESPONSE ERRORES
--pre
UPDATE GTB_SYSTEM_PARAM SET SYSTEM_PARAM_VALUE ='Yes' WHERE SYSTEM_PARAM_NAME='IWS_SYSTEM_AVAILABILITY_FLAG';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '99900000' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_SLA_BREACH%';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '100' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_FEED_CHECK%';
update STUB_FXRD set VALDATE = '31-12-25';
update stub_fx_bookrate set code='0',description='Transaction Successful',respone_type='S';
update stub_fxgetrate set code='0',description='Transaction Successful',response_type='S';
UPDATE GTB_CCY_PAIR SET EXPIRY_DATE='31-12-25';
UPDATE GTB_ORIGINATOR SET IS_ACTIVE=0;
update BENE_ACC_BANK_DTLS set mod_chk='N';
update business_date_master set eod_sod_flg=0;  
DELETE FROM OW_HOLIDAY WHERE HOLIDAY_DATE IN (TO_DATE(current_date),TO_DATE(current_date+1),TO_DATE(current_date-1),TO_DATE(current_date+2),TO_DATE(current_date+3),TO_DATE(current_date-2));
commit;

delete from stub_fxgetrate where customer_id in ('FXID2004');
delete from stub_fx_bookrate WHERE quoteid='QUOTEID20004';
--FX Spot Rate - System Timeout
update stub_fxgetrate set description='Timeout',response_type='T' where customer_id='FXID2003';

--FX Spot Contract No Contract ID
update stub_fx_bookrate set code='2301',description='Contract booked but cannot retrieve contract number due to system related issues',respone_type='E'  WHERE quoteid='QUOTEID20002';
commit;

--FX Spot Contract - Response Errors
update stub_fx_bookrate set description='Status Code 0 is received ( But with No Contract ID)',respone_type='E',code='0'  WHERE quoteid='QUOTE6717';
commit;

--FX Spot Contract - Timeout
update stub_fx_bookrate set description='TIMEOUT',respone_type='T'  WHERE quoteid='QUOTEP2001';
commit;
update stub_fx_bookrate set description='TIMEOUT',respone_type='T'  WHERE quoteid='QUOTEP2007';
--CTR
update stub_fx_bookrate set description='TIMEOUT',respone_type='T'  WHERE quoteid='QUOTEID20007';
--Forward contract response error code
update stub_fxrd set errorcode='1',errormsg='Cannot retrieve contract number (system issue)',resptype='E' where contractno='20005';
commit;
--Forward contract timeout
update stub_fxrd set errorcode='0',errormsg='Timeout',resptype='T' where txn_fx_id='09612 WIR';
update stub_fxrd set errorcode='0',errormsg='Timeout',resptype='T' where txn_fx_id='09612 WIR';
commit;
--legacy
update stub_elcm set code='0001',msgstat='SUCCESS',descp='Success',respone_type='S' where cust_id='12000048';
commit;
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('12000056');
commit;

update stub_elcm set code='INVALID-006',msgstat='FAILURE',descp='Client Number: $1 is Invalid',respone_type='F' where cust_id='12000045';
commit;

update stub_elcm set code='0001',msgstat='TIMEOUT',descp='Timeout',respone_type='T' where cust_id='12000060';
commit;
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',
respone_type='W' where cust_id IN ('10000056');
commit;
update stub_elcm set code='ST-SAVE-003',msgstat='FAILURE',descp='PPSL Utilization Failed - Liability Number Not found for Client Number',
respone_type='F' where cust_id='10006695';
commit;
update stub_elcm set code='0001',msgstat='TIMEOUT',descp='Timeout',
respone_type='T' where cust_id='10006692';
commit;

--envoy stub pre
update stub_psh_envoy set balancesavailableamt=0,RESP_TYPE='S',statuscode='DI00'  where  accounttransitnumber in('1111071') AND accounttransitid='11071';
update stub_psh_envoy set balancesavailableamt=0,RESP_TYPE='S',statuscode='DI00'  where  accounttransitnumber in('0364118') AND accounttransitid='00002';

update stub_psh_envoy set statuscode='DI01',cdiagnostictext='0007: System Error',resp_type='E' where accounttransitnumber='1111069' and accounttransitid='11069';
update stub_psh_envoy set statuscode='DI01',cdiagnostictext='0007: System Error',resp_type='E' where accounttransitnumber='0561712' and accounttransitid='00009';
update stub_psh_envoy set statuscode='DI01',cdiagnostictext='0007: System Error',resp_type='E' where accounttransitnumber='6403212' and accounttransitid='00002';
update stub_psh_envoy set statuscode='DI00',cdiagnostictext='Timeout',resp_type='T' where accounttransitnumber='2000051' and accounttransitid='00021';
update stub_psh_envoy set statuscode='DI00',cdiagnostictext='Timeout',resp_type='T' where accounttransitnumber='0561711' and accounttransitid='00009';
update stub_psh_envoy set statuscode='DI00',cdiagnostictext='Timeout',resp_type='T' where accounttransitnumber='6000118' and accounttransitid='00002';
commit;

--umm
update stub_wiredebit set response_code='100',response_type='E' where debit_acc='9000004';
commit;

update stub_wiredebit set response_code='911',response_type='T' where debit_acc='9000002';
commit;
update stub_wiredebit set response_code='000',response_type='S' where debit_acc='9000013';
commit;

update stub_wiredebit set response_code='000',response_type='S' where debit_acc='9000012';
commit;

--PRE QUERIES
update stub_ctr_debit set response_code_desc='TRANSACTION TIMEOUT',response_code='911',response_type='T' where from_account='2000049';
update stub_ctr_debit set response_code_desc='RESPONSE ERROR',response_code='913',response_type='E' where from_account='2000048';

--EMT ELCM QUERIES
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' 
where cust_id IN ('17000004');
commit;

update stub_elcm set code='INVALID-006',msgstat='FAILURE',descp='Client Number: $1 is Invalid',respone_type='F' where cust_id='17000006';
commit;
update stub_elcm set code='0001',msgstat='TIMEOUT',descp='Timeout',respone_type='T' where cust_id='17000005';
commit;
update stub_elcm set code='0001',msgstat='SUCCESS',descp='Success',respone_type='S' where cust_id='23000007';
commit;

--EMT ENVOY QUERIES
--envoy stub pre
update stub_psh_envoy set balancesavailableamt=0 where  accounttransitnumber in('1700041') AND accounttransitid='00017';
update stub_psh_envoy set statuscode='DI01',cdiagnostictext='0007: System Error',resp_type='E' where accounttransitnumber='1700015' and accounttransitid='00017';
update stub_psh_envoy set statuscode='DI00',cdiagnostictext='Timeout',resp_type='T' where accounttransitnumber='1700006' and accounttransitid='00017';
commit;
--EMT UMM STUB
update STUB_EMTDEBIT set response_code='100',response_type='E' where debit_acc='2300043';
commit;

update STUB_EMTDEBIT set response_code='940',response_type='E' where debit_acc='2900011';
commit;
--EMT BATCH DEBIT
update STUB_EMTDEBIT set response_code='100',response_type='E' where debit_acc='2900001';
commit;

update STUB_EMTDEBIT set response_code='940',response_type='E' where debit_acc='2300034';
commit;


--extra
update stub_fx_bookrate set code='0',DESCRIPTION='Transaction Successful',respone_type='S' where quoteid='QUOTEP2002';
update gtb_cust_account_pref set fund_util_type='A' WHERE  company_id='92000100' AND ACCOUNT_NO IN('6000118','0561711','1111069','0364118','1111069','0561712');    
COMMIT;
update STUB_FXRD set VALDATE = '31-12-23';
UPDATE gtb_ccy_pair SET EXPIRY_DATE='31-DEC-25';
commit;
update gtb_cust_account_pref set fund_util_type='L' WHERE  company_id='10006684' AND ACCOUNT_NO IN('0310018');
update gtb_cust_account_pref set fund_util_type='L' WHERE  company_id='10006695' AND ACCOUNT_NO IN('0310514') AND payment_prod_type IN ('CTR');
UPDATE gtb_cust_account_pref  set product_classification='C' WHERE COMPANY_ID LIKE '92000100' AND FUND_UTIL_TYPE='A' AND ACCOUNT_NO='1111071';
COMMIT;
UPDATE GTB_ORIGINATOR SET PROD_CLASS='C' WHERE ORIGINATOR_ID='1920000004';
UPDATE GTB_ORIGINATOR SET IS_ACTIVE=0;

COMMIT;
update gtb_originator set settl_method='A' where  originator_id='1920000004';
update stub_elcm set code='0001',msgstat='SUCCESS',descp='Success',
respone_type='S' where cust_id IN ('10007245','10006684','10000048','20000008');
UPDATE GTB_SYSTEM_PARAM SET SYSTEM_PARAM_VALUE ='Yes' WHERE SYSTEM_PARAM_NAME='IWS_SYSTEM_AVAILABILITY_FLAG';
DELETE FROM ow_workitem_instance WHERE QUEUEID='61261';
commit;
DELETE FROM FIF_DIRECTORY WHERE INSTITUTION_NO='0003' AND TRANSIT_NUMBER='00001';
COMMIT;
Insert into FIF_DIRECTORY(INSTITUTION_NO,TRANSIT_NUMBER,INSTITUTION_TYPE,CANCELLATION_STATUS,CANCELLATION_TRANSIT_NO,CANCELLATION_INSTITUTION_NO,LANGUAGE_CODE
,DESTINATION_DATA_CENTRE,INSTITUTION_NAME,CIVIC_ADDRESS,BRANCH_NAME,POSTAL_ADDRESS,CITY,PROVINCE,POSTAL_CODE,REGION_CODE,PARENT_DATA_CENTRE,CIBC_INDIRECT_CLEARER,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,HEADER_DATE,FIF_SEQ_NO,ROUTING_CD,ROUTING_DATA_CENTRE,RELEASE_CD,RELEASE_TIME,DESIGNATION,COURIER_CD,CREDIT_POSTING_INST,CREDIT_POSTING_BRANCH,CREDIT_POSTING_ACC,DEBIT_POSTING_INST,DEBIT_POSTING_BRANCH,DEBIT_POSTING_ACC,CA_SERVICE_CD,PCA_SERVICE_CD,CHQ_SAV_SERVICE_CD,SAV_SERVICE_CD,LAS_SERVICE_CD,GL_SERVICE_CD,USD_SERVICE_CD,MASTER_FILE_NO,ADMINISTRATOR_REGION,TOTAL_LINES) values ('0003','00001','01','V','00000','0000','E','01020','CANADIAN IMPERIAL BANK OF COMMERCE','5870 MALDEN RD.',null,'5870 MALDEN RD.','LASALLE','ON','N9H 1S4',2,'01021','N','SYSTEM',to_date('15-07-19','DD-MM-RR'),'SYSTEM',to_date('15-07-19','DD-MM-RR'),null,'69605','1','01020','1','1','5870 MALDEN RD BANKING CENTRE','0','000','00000',null,'000','00000',null,'2','2','2','2','2','2','2','1',null,'20155');
COMMIT;
