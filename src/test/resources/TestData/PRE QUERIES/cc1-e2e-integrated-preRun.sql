update STUB_FXRD set VALDATE = '31-12-23';
update stub_fx_bookrate set code='0',description='Transaction Successful',respone_type='S';
update stub_fxgetrate set code='0',description='Transaction Successful',response_type='S';
commit;
--Before Pre Run Stub ---
UPDATE STUB_PSH_ENVOY SET STATUSCODE='DI00',CDIAGNOSTICTEXT='TRANSACTION SUCCESSFUL',RESP_TYPE='S',BALANCESAVAILABLEAMT='0' WHERE ACCOUNTTRANSITNUMBER IN ('2000053','','','','');
UPDATE STUB_PSH_ENVOY SET STATUSCODE='DI00', CDIAGNOSTICTEXT='TRANSACTION SUCCESSFUL', RESP_TYPE='S',BALANCESAVAILABLEAMT='900000' WHERE ACCOUNTTRANSITNUMBER in ('2000054','');
UPDATE STUB_ELCM SET MSGSTAT='SUCCESS',CODE='0001',DESCP='SUCCESS',RESPONE_TYPE='S' WHERE CUST_ID IN ('20000009','');
UPDATE STUB_FXGETRATE SET RESPONSE_TYPE = 'T' WHERE CUSTOMER_ID = 'FXID2009';

Delete FROM STUB_WIREDEBIT where DEBIT_ACC IN (2000059,2000054,2000055,2000057,2000059,2000054,2000059,2000053,2000055);
Delete FROM STUB_FXRD where TXN_FX_ID = 'FXID2009';
commit;
Insert into STUB_WIREDEBIT (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values (2000000000000009,'2000053','00021','1234','000','S');
Insert into STUB_WIREDEBIT (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values (2000000000000009,'2000054','00021','1234','000','S');
Insert into STUB_WIREDEBIT (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values (2000000000000009,'2000055','00021','1234','000','S');
Insert into STUB_WIREDEBIT (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values (2000000000000009,'2000057','00021','1234','000','S');
Insert into STUB_WIREDEBIT (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values (2000000000000009,'2000059','00021','1234','000','S');
Insert into STUB_WIREDEBIT (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values (2000000000000009,'2000054','00021','1234','000','S');
Insert into STUB_FXRD (CONSUMERID,CONTRACTNO,CONTRACTRATE,VALDATE,TRADEDATE,STARTDATE,ERRORCODE,ERRORMSG,RESPTYPE,TXN_FX_ID) values ('PSH','2',1.1,to_date('31-DEC-20','DD-MON-RR'),to_date('04-JAN-18','DD-MON-RR'),to_date('01-JAN-19','DD-MON-RR'),0,'Success','S','FXID2009');

commit;