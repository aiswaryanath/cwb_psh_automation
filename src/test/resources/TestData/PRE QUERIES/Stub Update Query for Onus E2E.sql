update stub_elcm set msgstat='WARNING',code='LM-00113',descp='Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id='20180427';
commit;
update stub_psh_envoy set statuscode='DI01',cdiagnostictext='0007: System Error',resp_type='E',balancesavailableamt='15' where accounttransitnumber in ('0270407','0270405');
commit;
Update Stub_Psh_Envoy Set Statuscode = 'DI01', Cdiagnostictext = '0004: Invalid Data', Resp_Type = 'E' Where Accounttransitnumber = '0270401';
commit;


UPDATE GTB_SYSTEM_PARAM SET SYSTEM_PARAM_VALUE ='Yes' WHERE SYSTEM_PARAM_NAME='IWS_SYSTEM_AVAILABILITY_FLAG';
DELETE FROM ow_workitem_instance WHERE QUEUEID='61261';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '99900000' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_SLA_BREACH%';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '100' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_FEED_CHECK%';
update STUB_FXRD set VALDATE = '31-12-25';
update stub_fx_bookrate set code='0',description='Transaction Successful',respone_type='S';
update stub_fxgetrate set code='0',description='Transaction Successful',response_type='S';
UPDATE GTB_CCY_PAIR SET EXPIRY_DATE='31-12-25';
UPDATE GTB_ORIGINATOR SET IS_ACTIVE=0;
update BENE_ACC_BANK_DTLS set mod_chk='N';
update business_date_master set eod_sod_flg=0;  
DELETE FROM OW_HOLIDAY WHERE HOLIDAY_DATE IN (TO_DATE(current_date),TO_DATE(current_date+1),TO_DATE(current_date-1),TO_DATE(current_date+2),TO_DATE(current_date+3),TO_DATE(current_date-2));
commit;

