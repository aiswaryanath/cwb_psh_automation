update business_date_master set eod_sod_flg=0;  
commit; 
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('12000048');
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('10006722');
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('10000056');
update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='12000056'; 
update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='20200821';
update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='10006687'; 
update stub_psh_envoy set balancesavailableamt=1000000000 where accounttransitnumber='2012008'; 
update stub_wire_pmt  set statuscode='000', resp_type='S',APPROVALCODE='APPROV' where pan='4591237894251369';
update stub_wiredebit set response_code='911',response_type='T' where debit_acc='0310611';
commit;
delete from STUB_CONFIG_RESPNSE_FILES where ORIGINATOR_ID IN ('9708234614','0100777006');
COMMIT;
UPDATE GTB_SYSTEM_PARAM SET SYSTEM_PARAM_VALUE ='Yes' WHERE SYSTEM_PARAM_NAME='IWS_SYSTEM_AVAILABILITY_FLAG';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '99900000' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_SLA_BREACH%';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '100' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_FEED_CHECK%';
update STUB_FXRD set VALDATE = '31-12-25';
update stub_fx_bookrate set code='0',description='Transaction Successful',respone_type='S';
update stub_fxgetrate set code='0',description='Transaction Successful',response_type='S';
UPDATE GTB_CCY_PAIR SET EXPIRY_DATE='31-12-25';
UPDATE GTB_ORIGINATOR SET IS_ACTIVE=0;
update BENE_ACC_BANK_DTLS set mod_chk='N';
commit;


