package Utilities;


public class APIResponse {
	
	String RequestCode;
	String process;
	String dataResponse;
	String response;
	public String getRequestCode() {
		return RequestCode;
	}
	public void setRequestCode(String requestCode) {
		RequestCode = requestCode;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getDataResponse() {
		return dataResponse;
	}
	public void setDataResponse(String dataResponse) {
		this.dataResponse = dataResponse;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	

}


