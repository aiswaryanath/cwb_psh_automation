/***Class written for Wires  API call
 * written by Aishwarya Nath
 * 
 */

package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;
import Logger.LoggerUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import com.google.gson.Gson;

import UIOperations.Keywordoperations;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PSHWiresCancellation {

	public String Status;
	public static Response response;
	static Logger log = LoggerUtils.getLogger();
	static FileUtilities Fileutil = new FileUtilities();
	public static String POSTToAPI(String Body_Json) throws Exception{
		String status=null;
		try{
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setBody(Body_Json);
			builder.setContentType("application/json; charset=UTF-8");
			builder.addHeader("channelId", "CBX");
			builder.addHeader("RETRY_FLAG", "N");
			String WiresCancellationURL = PropertyUtils.readProperty("WiresCancellationURL");
			RequestSpecification request = builder.build();
			response = RestAssured.given().spec(request).when().post(WiresCancellationURL);
			Thread.sleep(5000);
			System.out.println(response.getStatusCode());
			log.info(response.getStatusCode());
			status = response.body().asString();
			System.out.println("Response Body"+status);
			log.info("Response Body"+status);
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		return status;
	}
	
	public String getresponse(String Custid,String TXNREFNUM,String Criteria) throws Exception{
		String Result_Status=null;
		try {


			String input1=null;

	        //if(api.equals("CONTRACT"))
	        {
	        input1 ="{\"Document\":{\"CstmrPmtCxlReq\":{\"Assgnmt\":{\"Id\":\"#VAR1\",\"Assgnr\":{\"Pty\":{\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"#VAR2\"}}}}}},\"Undrlyg\":{\"OrgnlPmtInfAndCxl\":{\"OrgnlPmtInfId\":\"AABETE2E8694458931\",\"TxInf\":{\"CxlId\":\"#VAR3\",\"OrgnlEndToEndId\":\"#VAR4\",\"CxlRsnInf\":{\"AddtlInf\":\"DO IT.\"}}}}}}}";
	        }

			
			Keywordoperations key=new Keywordoperations();
			String VAR_Reqid = key.getSaltString("Y", 10,"Reqid");
			String CANCid = key.getSaltString("Y", 10,"Cancid");
			System.out.println(VAR_Reqid);
			log.info(VAR_Reqid);
			String CUSTREFNUM=Fileutil.GetFilevalue(TXNREFNUM);

			String input2 = input1.replace("#VAR1",VAR_Reqid);
			input2= input2.replace("#VAR2",Custid);
			input2= input2.replace("#VAR3",CANCid);
			input2= input2.replace("#VAR4",CUSTREFNUM);
			System.out.println("Final:" + input2);
			log.info("Final:" + input2);

			String Finalstatus=null;
		
			Finalstatus=POSTToAPI(input2);
			if(Finalstatus.contains(Criteria))
			{
				Result_Status="PASS";
				System.out.println("PASS");
				log.info("PASS");
			}
			else
			{
				Result_Status="FAIL";
				System.out.println("FAIL");	
				log.info("FAIL");
			}


			
		}catch (Exception e) {
			e.printStackTrace();
			Result_Status="FAIL";
		}
		return Result_Status;
	}


	
	public static void main(String[] args) throws Exception{
		String Result_Status=null;
		try {


			String input1=null;

	        //if(api.equals("CONTRACT"))
	        {
	        input1 ="{\"Document\":{\"CstmrPmtCxlReq\":{\"Assgnmt\":{\"Id\":\"#VAR1\",\"Assgnr\":{\"Pty\":{\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"#VAR2\"}}}}}},\"Undrlyg\":{\"OrgnlPmtInfAndCxl\":{\"OrgnlPmtInfId\":\"AABETE2E8694458931\",\"TxInf\":{\"CxlId\":\"#VAR3\",\"OrgnlEndToEndId\":\"#VAR4\",\"CxlRsnInf\":{\"AddtlInf\":\"DO IT.\"}}}}}}}";
	        }

			
			Keywordoperations key=new Keywordoperations();
			String VAR_Reqid = key.getSaltString("Y", 10,"Reqid");
			String CANCid = key.getSaltString("Y", 10,"Cancid");
			System.out.println(VAR_Reqid);
			String CUSTREFNUM=Fileutil.GetFilevalue("VAR_CUSTREFNUM");

			String input2 = input1.replace("#VAR1",VAR_Reqid);
			input2= input2.replace("#VAR2","10000048");
			input2= input2.replace("#VAR3",CANCid);
			input2= input2.replace("#VAR4",CUSTREFNUM);
			System.out.println("Final:" + input2);


			String Finalstatus=null;
		
			Finalstatus=POSTToAPI(input2);
			if(Finalstatus.contains("CNCL"))
			{
				Result_Status="PASS";
				System.out.println("PASS");
			}


			
		}catch (Exception e) {
			e.printStackTrace();
			Result_Status="FAIL";
		}
	

	}
}

