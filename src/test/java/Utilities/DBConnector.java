package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.testng.reporters.jq.Main;

import com.ibatis.common.jdbc.ScriptRunner;

import Logger.LoggerUtils;
import UIOperations.Keywordoperations;

public class DBConnector {
	static Logger log = LoggerUtils.getLogger();
	static FileUtilities Fileutil = new FileUtilities();
	public static ResultSet _ResultSetDBConnect(String Excel_dataOperations) throws Exception{
		ResultSet _ResultSet = null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.11.16.30:1521:SIR13199","CIBC_IR_162_BASE","CIBC_IR_162_BASE");
			_ResultSet=DBConnector.getResultString(connection, "E2E_AUTOMATION_TESTING",Excel_dataOperations);
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
			
		}
		return _ResultSet;
	}
	
	public static Connection getConnection(String Excel_dataOperations) throws Exception{
		ResultSet _ResultSet = null;
		Connection connection=null;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURLC");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection=DriverManager.getConnection(PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			//_ResultSet=DBConnector.getResultString(connection, "E2E_AUTOMATION_TESTING",Excel_dataOperations);
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
			
		}
		return connection;
	}

	public static ResultSet getResultString(final Connection connection, final String tableName,String Testcaseid) {
	     String query = "Select OPERATION_MODE,KEYNAME,TYPEOFATTRIBUTE from  "+tableName+" where OPERATION_MODE = '"+Testcaseid+"'";
	    return executeQuery(connection, query);
	}

	
	public static ResultSet getResultStringPSH(final Connection connection, final String tableName,String Testcaseid) {
	     String query = "select workitemid,file_status,message_id,file_name,creation_date,file_processing_date,FILE_SOURCE,INIT_PRTY_ID,CUST_ID from "+tableName+" where message_id = '"+Testcaseid+"'";
	    return executeQuery(connection, query);
	}
	
	private static final ResultSet executeQuery(final Connection connection, final String query) {
	    try {
	    	System.out.println("TEST:"+query);
	    	log.info("TEST:"+query);
	        return connection.createStatement().executeQuery(query);
	    } catch (SQLException e) {
	        throw new RuntimeException(e);
	    }
	}

	public static String GetXPATH(String KeywordOperation) throws Exception, SQLException {
		String XPATH_NAV = null;
		ResultSet rs1= DBConnector._ResultSetDBConnect(KeywordOperation);
		while (rs1.next()) {
			XPATH_NAV = rs1.getString("KEYNAME");
			System.out.println(XPATH_NAV);
		}
		return XPATH_NAV;
	}
	
/*	public static ArrayList GetValues(String KeywordOperation) throws Exception, SQLException {
		ArrayList<String> _ObVal = new ArrayList<String>(); 
		String XPATH_NAV = null;
		String Return_Operation=null;
		String TYPEOFATTRIBUTE = null;
		ResultSet rs1= DBConnector._ResultSetDBConnect(KeywordOperation);
		while (rs1.next()) {
			XPATH_NAV = rs1.getString("KEYNAME");
			Return_Operation= rs1.getString("OPERATION_MODE");
			TYPEOFATTRIBUTE = rs1.getString("TYPEOFATTRIBUTE");
			_ObVal.add(XPATH_NAV);
			_ObVal.add(Return_Operation);
			_ObVal.add(TYPEOFATTRIBUTE);
			System.out.println(XPATH_NAV);
		}
		return _ObVal;
	}
*/
	public static String _GetWorkItemIDPSH_C(String Excel_dataOperations) throws Exception{
		String Return_Operation = null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.200.12.2:1521:CIBCDEV3","DITPSH","DITPSH");
			ResultSet _ResultSet=DBConnector.getResultStringPSH(connection, "File_Proc_Opn",Excel_dataOperations);
		  while (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString("workitemid");
			  System.out.println(Return_Operation);
			  log.info("workitemid:"+Return_Operation);
		  }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Return_Operation;
	}
	
	public static String GetValueFromDBConnector(String SQL,String ColumnValue,String Prop_WiiD,String StoreValueinPropWiiD) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		Keywordoperations _Storevalue=new Keywordoperations();
		String Return_Operation = null;		
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String St_Prop_WiiD = null;
		if (Prop_WiiD.contains("VAR_"))
		{
		St_Prop_WiiD = FileUtilities.GetFilevalue(Prop_WiiD);
		}
		else
		{
		St_Prop_WiiD = Prop_WiiD;
		}
		try{
			String query = SQL+" IN '"+St_Prop_WiiD+"'";
			System.out.println(query);
			log.info("query:"+query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet _ResultSet=getResultStringSQL(connection, SQL,St_Prop_WiiD);
		  if (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString(ColumnValue);
			  System.out.println(Return_Operation);
			  log.info("from db:"+Return_Operation);
			  _Storevalue.StoreValue(StoreValueinPropWiiD, Return_Operation); 
			  log.info("workitemid:"+StoreValueinPropWiiD);
		  }
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Return_Operation;
	}
	
	public static String GetValueFromDBConnector2(String SQL,String ColumnValue,String Prop_WiiD,String StoreValueinPropWiiD) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		Keywordoperations _Storevalue=new Keywordoperations();
		String Return_Operation = null;		
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String St_Prop_WiiD = null;
		if (Prop_WiiD.contains("VAR_"))
		{
		St_Prop_WiiD = FileUtilities.GetFilevalue(Prop_WiiD);
		}
		else
		{
		St_Prop_WiiD = Prop_WiiD;
		}
		try{
			String query = SQL+" = '"+St_Prop_WiiD+"'";
			System.out.println(query);
			log.info("query:"+query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet _ResultSet=getResultStringSQL(connection, SQL,St_Prop_WiiD);
		  if (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString(ColumnValue);
			  System.out.println(Return_Operation);
			  log.info("from db:"+Return_Operation);
			  _Storevalue.StoreValue(StoreValueinPropWiiD, Return_Operation); 
			  //log.info("workitemid:"+StoreValueinPropWiiD);
		  }
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Return_Operation;
	}
	
	
	public static void GetValueFromOLIFABRICConnector(String SQL,String ColumnValue,String Prop_WiiD,String StoreValueinPropWiiD) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		Keywordoperations _Storevalue=new Keywordoperations();
		String Return_Operation = null;		
		String PSH_DBURL=PropertyUtils.readProperty("OLIFAB_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("OLIFAB_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("OLIFAB_DBUserPass");
		String St_Prop_WiiD = FileUtilities.GetFilevalue(Prop_WiiD);
		try{
			String query = SQL+" = '"+St_Prop_WiiD+"'";
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet _ResultSet=getResultStringSQL(connection, SQL,St_Prop_WiiD);
		  while (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString(ColumnValue);
			  System.out.println(Return_Operation);
			  _Storevalue.StoreValue(StoreValueinPropWiiD, Return_Operation); 
		  }
		}catch (Exception e) {
			e.printStackTrace();
		}
//		return Return_Operation;
	}

public static String DBConnectorStatusCheck(String SQL,String ColumnValue,String ColumnValue2,String Prop_WiiD,String StoreValueinPropWiiD) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		Keywordoperations _Storevalue=new Keywordoperations();
		String Return_Operation = null;
		String Return_Operation2 = null;
		String Result = null;
		String PSH_DBURL=PropertyUtils.readProperty("OLIFAB_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("OLIFAB_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("OLIFAB_DBUserPass");
		String St_Prop_WiiD = FileUtilities.GetFilevalue(Prop_WiiD);
		LinkedHashMap<String, String> map=new LinkedHashMap<String, String>();
		StringBuffer errorLogs= new StringBuffer();
		try{
			String query = SQL+" = '"+St_Prop_WiiD+"'";
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet _ResultSet=getResultStringSQL(connection, SQL,St_Prop_WiiD);
		  while (_ResultSet.next()) {
			  if (_ResultSet.getString(ColumnValue).equals("ERROR")) {
				  _Storevalue.StoreValue(StoreValueinPropWiiD, _ResultSet.getString(ColumnValue)+":"+_ResultSet.getString(ColumnValue2));
				 String val1 = _ResultSet.getString(ColumnValue);
				 String val2 =_ResultSet.getString(ColumnValue2);
				 if ( val1!=null &&  val2!=null) {
					 errorLogs.append(val1).append(":").append(val2).append(",");
				}
			  }
		  }
		  Result  = errorLogs.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Result;
	}

	public static ResultSet getResultStringPSHRetro(final Connection connection, final String tableName,String Testcaseid) {
	     String query = "SELECT WORKITEMID, FILE_WORKITEM_ID, TXN_STATUS, TRN_STATUS_CODE from "+tableName+" where file_WORKITEM_ID = '"+Testcaseid+"'";
	    return executeQuery(connection, query);
	}
	
	
	public static ResultSet getResultStringSQL(final Connection connection, final String SQL,String key) {
		 String query = SQL+" IN ('"+key+"')";
		if (key.contains(","))
		{	
	     query = SQL+" IN ("+key+")";
		}
		else
		{
			  query = SQL+" IN ('"+key+"')";
		}
	    return executeQuery(connection, query);
	}
	public static ResultSet getResultStringSQL2(final Connection connection, final String SQL,String key,String key1) {
	     String query = SQL+" = '"+key+"'"+" AND '"+key1+"'";
	    return executeQuery(connection, query);
	}
	
	public static ArrayList<String> _DBConnectPSHretroJYO(String Excel_dataOperations) throws Exception{
		String Return_Operation = null;
		ArrayList<String> arr=new ArrayList<String>(); 
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.32:1521:SIR18362","PSH","PSHRetroJoyo");
			ResultSet _ResultSet=DBConnector.getResultStringPSHRetro(connection, "TXN_PROC_OPN",Excel_dataOperations);
		  while (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString("WORKITEMID");
			  arr.add(Return_Operation);
		  }  
		}catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}

	public String DBConnector(String TestData) throws Exception {
		String str=null;
		Keywordoperations Keywordoperations=new Keywordoperations();
		String DB_Env = Keywordoperations.get_DataValue(TestData).get(0);
		System.out.println("DB_Env"+Keywordoperations.get_DataValue(TestData).get(0));
		String SQL = Keywordoperations.get_DataValue(TestData).get(1);
		System.out.println("SQL"+Keywordoperations.get_DataValue(TestData).get(1));
		String COl_value  = Keywordoperations.get_DataValue(TestData).get(2);
		System.out.println("COl_value"+Keywordoperations.get_DataValue(TestData).get(2));
		String Prop_WiiD = Keywordoperations.get_DataValue(TestData).get(3);
		System.out.println("Prop_WiiD"+Keywordoperations.get_DataValue(TestData).get(3));
		String StoreValueinPropWiiD = Keywordoperations.get_DataValue(TestData).get(4);
		System.out.println("StoreValueinPropWiiD"+Keywordoperations.get_DataValue(TestData).get(4));
		switch (DB_Env) {
		case "PSH":
				GetValueFromDBConnector(SQL,COl_value,Prop_WiiD,StoreValueinPropWiiD);
			break;
		case "CBX":
				GetValueFromDBConnector(SQL,COl_value,Prop_WiiD,StoreValueinPropWiiD);
			break;
		case "OLIFABRIC":
			/*str = */GetValueFromOLIFABRICConnector(SQL,COl_value,Prop_WiiD,StoreValueinPropWiiD);
			break;
//		case "OLIFABRICStatus":
//			str = DBConnectorStatusCheck(Keywordoperations.get_DataValue(TestData).get(1),Keywordoperations.get_DataValue(TestData).get(2),Keywordoperations.get_DataValue(TestData).get(3),Keywordoperations.get_DataValue(TestData).get(4),Keywordoperations.get_DataValue(TestData).get(5));
//			break;
		default:
			break;
		}
		return str;
	}
	public String DBConnector2(String TestData) throws Exception {
		String str=null;
		Keywordoperations Keywordoperations=new Keywordoperations();
		String DB_Env = Keywordoperations.get_DataValue(TestData).get(0);
		System.out.println("DB_Env"+Keywordoperations.get_DataValue(TestData).get(0));
		String SQL = Keywordoperations.get_DataValue(TestData).get(1);
		System.out.println("SQL"+Keywordoperations.get_DataValue(TestData).get(1));
		String COl_value  = Keywordoperations.get_DataValue(TestData).get(2);
		System.out.println("COl_value"+Keywordoperations.get_DataValue(TestData).get(2));
		
		String Prop_WiiD = Keywordoperations.get_DataValue(TestData).get(3);
		System.out.println("Prop_WiiD"+Keywordoperations.get_DataValue(TestData).get(3));
		String StoreValueinPropWiiD = Keywordoperations.get_DataValue(TestData).get(4);
		System.out.println("StoreValueinPropWiiD"+Keywordoperations.get_DataValue(TestData).get(4));
		switch (DB_Env) {
		case "PSH":
				GetValueFromDBConnector2(SQL,COl_value,Prop_WiiD,StoreValueinPropWiiD);
			break;
		
		default:
			break;
		}
		return str;
	}

	public static ArrayList<String> get_DataValue(String User_Name) {
		ArrayList<String> _arr_str = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(User_Name);
		while (st.hasMoreTokens()) {
			_arr_str.add(st.nextToken("|"));
		}
		return _arr_str;
	}

	public  void UpdateDBEnvironment(String TestData){
		String DB_Env = Keywordoperations.get_DataValue(TestData).get(0);
		String UPDATE_Query = Keywordoperations.get_DataValue(TestData).get(1);
		try{
			switch (DB_Env) {
			case "PSH":
					DBConnector.Execute_Update_PSHQuery(UPDATE_Query);
				break;
			case "OLIFABRIC":
				DBConnector.Execute_Update_CBXQuery(UPDATE_Query);
			break;
			default:
				break;
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**public  void UpdateDBEnvironment2(String TestData){
		String DB_Env = Keywordoperations.get_DataValue(TestData).get(0);
		String UPDATE_Query = Keywordoperations.get_DataValue(TestData).get(1);

					try {
						DBConnector.Execute_Update_PSHQuery2(UPDATE_Query);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

	}
	**/
	public static int Execute_Update_PSHQueryDate(String TESTDATA) throws Exception {
		String URL = PropertyUtils.readProperty("PSH_DBURL");
		String user = PropertyUtils.readProperty("PSH_DBUserName");
		String pass = PropertyUtils.readProperty("PSH_DBUserPass");
		int row=0;
		Connection connection=null;
		Statement stmt=null;
		if (TESTDATA != null) {
			String[] Data = TESTDATA.split("\\|");
			String ENV=  Data[0];
			String Query = Data[1];
			String txnid =Data[3];
			java.util.Date utilDate = new java.util.Date();
		    System.out.println("Util date in Java : " + utilDate);

		    // contains only date information without time
		    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		   // java.sql.Date sqlDate2 = (java.sql.Date) subtractDays(sqlDate,1);
		    System.out.println("SQL date in Java : " + sqlDate);
		    log.info("SQL date in Java : " + sqlDate);
			String TxnId=Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
			System.out.println("Current Txnsid from file"+TxnId);
			log.info("Current Txnsid from file"+TxnId);
		try {	
					Class.forName("oracle.jdbc.driver.OracleDriver");
					connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);

					  PreparedStatement preparedStatement = connection.prepareStatement(Query); 

				            preparedStatement.setDate(1,sqlDate);
				            preparedStatement.setDate(2,sqlDate);
				            preparedStatement.setString(3,TxnId);
				            
					System.out.println("Udpate query executed Successfully!!"+TESTDATA);
					log.info("Udpate query executed Successfully!!"+TESTDATA);
				    row = preparedStatement.executeUpdate();

		            // rows affected
		            System.out.println("No of rows affected"+row);
		            log.info("No of rows affected"+row);
		            preparedStatement.close();

		           
		            }
			catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage());
			}finally {
				 connection.close();
			}	
		}
		return row;
		}
	
	public static int Update_PSH_Dates(String TESTDATA) throws Exception {
		String URL = PropertyUtils.readProperty("PSH_DBURL");
		String user = PropertyUtils.readProperty("PSH_DBUserName");
		String pass = PropertyUtils.readProperty("PSH_DBUserPass");
		int row=0;
		Connection connection=null;
		Statement stmt=null;
		if (TESTDATA != null) {
			String[] Data = TESTDATA.split("\\|");
			String ENV=  Data[0];
			String Query = Data[1];
			String txnid =Data[3];
			java.util.Date utilDate = new java.util.Date();
		    System.out.println("Util date in Java : " + utilDate);
		    log.info("Util date in Java : " + utilDate);
		    // contains only date information without time
		    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		   // java.sql.Date sqlDate2 = (java.sql.Date) subtractDays(sqlDate,1);
		    System.out.println("SQL date in Java : " + sqlDate);
		    log.info("SQL date in Java : " + sqlDate);
			String TxnId=Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
			System.out.println("Current Txnsid from file"+TxnId);
		try {	
					Class.forName("oracle.jdbc.driver.OracleDriver");
					connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);

					  PreparedStatement preparedStatement = connection.prepareStatement(Query); 

				            preparedStatement.setDate(1,sqlDate);
				            //preparedStatement.setDate(2,sqlDate);
				            preparedStatement.setString(2,TxnId);
				            
					System.out.println("Udpate query executed Successfully!!"+TESTDATA);
					log.info("Udpate query executed Successfully!!"+TESTDATA);
				    row = preparedStatement.executeUpdate();

		            // rows affected
		            System.out.println("No of rows affected"+row);
		            log.info("No of rows affected"+row);
		            preparedStatement.close();

		           
		            }
			catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage());
			}finally {
				 connection.close();
			}	
		}
		return row;
		}
	public static Date subtractDays(Date date, int days)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -days);
        return new Date(c.getTimeInMillis());
    }
	public static int InsertHoliday(String TESTDATA) throws Exception {
		String URL = PropertyUtils.readProperty("PSH_DBURL");
		String user = PropertyUtils.readProperty("PSH_DBUserName");
		String pass = PropertyUtils.readProperty("PSH_DBUserPass");
		int row=0;
		Connection connection=null;
		Statement stmt=null;
		if (TESTDATA != null) {
			String[] Data = TESTDATA.split("\\|");
			String adddate=  Data[0];
			String type = Data[1];
			
			java.util.Date utilDate = new java.util.Date();
		    System.out.println("Util date in Java : " + utilDate);
		    //Query="update ow_holiday set holiday_date=? where seqno=15";
		    String Query="INSERT INTO ow_holiday VALUES (?,?,?,?,?,?,?,?,?)";
		    // contains only date information without time
		    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		    System.out.println("SQL date in Java : " + sqlDate);
			log.info("SQL date in Java : " + sqlDate);
		try {	
					Class.forName("oracle.jdbc.driver.OracleDriver");
					connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);

					  PreparedStatement preparedStatement = connection.prepareStatement(Query); 
					        preparedStatement.setString(1,"CALCA");
				            preparedStatement.setDate(2,sqlDate);
				            preparedStatement.setString(3,"Holiday-Automation");
				            preparedStatement.setString(4,type);
				            preparedStatement.setString(5,"BCCMKR1");
				            preparedStatement.setDate(6,sqlDate);
				            preparedStatement.setString(7,"BCCCKR1");
				            preparedStatement.setDate(8,sqlDate);
				            preparedStatement.setInt(9,214);
				       
					System.out.println("Udpate query executed Successfully!!"+TESTDATA);
					log.info("Udpate query executed Successfully!!"+TESTDATA);
				    row = preparedStatement.executeUpdate();

		            // rows affected
		            System.out.println("No of rows affected"+row);
		            log.info("No of rows affected"+row);
		            preparedStatement.close();
			}catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage());
			}finally {
				
				 connection.close();
			}	
		}
		return row;
		}
	public static void Execute_Update_PSHQuery(String TESTDATA) throws Exception {
		String URL = PropertyUtils.readProperty("PSH_DBURL");
		String user = PropertyUtils.readProperty("PSH_DBUserName");
		String pass = PropertyUtils.readProperty("PSH_DBUserPass");
		Connection connection=null;
		Statement stmt=null;
		try {	
					Class.forName("oracle.jdbc.driver.OracleDriver");
					connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);
					stmt = connection.createStatement();
					System.out.println("Udpate query executed Successfully!!"+TESTDATA);
					log.info("Udpate query executed Successfully!!"+TESTDATA);
					stmt.executeUpdate(TESTDATA);
			}catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage());
			}finally {
				 stmt.close();
				 connection.close();
			}	
		}
	

	
	public static void Execute_Update_CBXQuery(String TESTDATA) throws Exception {
		String URL = PropertyUtils.readProperty("OLIFAB_DBURL");
		String user = PropertyUtils.readProperty("OLIFAB_DBUserName");
		String pass = PropertyUtils.readProperty("OLIFAB_DBUserPass");
		Connection connection=null;
		Statement stmt=null;
		try {	
					Class.forName("oracle.jdbc.driver.OracleDriver");
					connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);
					stmt = connection.createStatement();
					System.out.println("Udpate query executed Successfully!!");
					log.info("Udpate query executed Successfully!!"+TESTDATA);
					stmt.executeUpdate(TESTDATA);
			}catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage());
			}finally {
				 stmt.close();
				 connection.close();
			}	
		}
	public HashMap<String, String> PSHInterfaceDB(String TestData) throws Exception {
		String str=null;
		Keywordoperations Keywordoperations=new Keywordoperations();
		String InterfaceId = Keywordoperations.get_DataValue(TestData).get(0);
		System.out.println("DB_Env"+Keywordoperations.get_DataValue(TestData).get(0));
		String SQL = Keywordoperations.get_DataValue(TestData).get(1);
		System.out.println("SQL"+Keywordoperations.get_DataValue(TestData).get(1));
		String[] COl_value = {"STATUS", "TXN_RSN_DESC", "TXN_RSN_CD","INTERFACE_RESPONSE_CODE","OUTGOING_MSG"};
		System.out.println("COl_value"+Keywordoperations.get_DataValue(TestData).get(2));
		String Prop_WiiD = Keywordoperations.get_DataValue(TestData).get(3);
		System.out.println("Prop_WiiD"+Keywordoperations.get_DataValue(TestData).get(3));
		String StoreValueinPropWiiD = Keywordoperations.get_DataValue(TestData).get(4);
		System.out.println("InterfaceID"+Keywordoperations.get_DataValue(TestData).get(4));

		HashMap<String, String> InterfaceMessage=GetMultiFromDBConnector(InterfaceId,SQL,COl_value ,Prop_WiiD,StoreValueinPropWiiD);
	
		return InterfaceMessage;
		}
	public static HashMap<String, String> GetMultiFromDBConnector(String InterfaceId,String SQL,String ColumnValue[],String Prop_WiiD,String StoreValueinPropWiiD) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		  HashMap<String,String> Result=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		  String InterfaceStatus=ColumnValue[0];
		  String TXN_RSN_DESC=ColumnValue[1];
		  String TXN_RSN_CD=ColumnValue[2];
		  String INTERFACE_RESPONSE_CODE=ColumnValue[3];
		  String Incoming_msg=ColumnValue[4];	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String St_Prop_WiiD = FileUtilities.GetFilevalue(Prop_WiiD);
		try{
			String query = SQL;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,St_Prop_WiiD);
			p.setString(2,InterfaceId);
			ResultSet _ResultSet=p.executeQuery();
		
	
		  while (_ResultSet.next()) {

			  InterfaceStatus = _ResultSet.getString(InterfaceStatus);
			  System.out.println(InterfaceStatus);
			  TXN_RSN_DESC = _ResultSet.getString(TXN_RSN_DESC);
			  System.out.println(InterfaceStatus);
			  log.info(InterfaceStatus);
			  TXN_RSN_CD = _ResultSet.getString(TXN_RSN_CD);
			  System.out.println(TXN_RSN_CD);
			  log.info(TXN_RSN_CD);
			  INTERFACE_RESPONSE_CODE = _ResultSet.getString(INTERFACE_RESPONSE_CODE);
			  System.out.println(INTERFACE_RESPONSE_CODE);
			  log.info(INTERFACE_RESPONSE_CODE);
			  Incoming_msg = _ResultSet.getString(Incoming_msg);
			  System.out.println(Incoming_msg);
			  log.info(Incoming_msg);
			  _Storevalue.StoreValue("VAR_OUTGOINGMSG", Incoming_msg); 
				Result.put("InterfaceStatus",InterfaceStatus);
				  Result.put("TXN_RSN_DESC",TXN_RSN_DESC);
				  Result.put("TXN_RSN_CD",TXN_RSN_CD);
				  Result.put("INTERFACE_RESPONSE_CODE",INTERFACE_RESPONSE_CODE);
				  Result.put("Incoming_msg",Incoming_msg);
		
			}
		  /*
			if(_ResultSet.next()==false)
			{
				Result.put("InterfaceStatus","NO RECORD");
				System.out.println("NO RECORD FOUND");
			}
			 */ 
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}

	  
		return Result;
	}

	public static HashMap<String, String> GetTxnDetails(String TESTDATA) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		  HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		
		if (TESTDATA != null) {
			String[] Data = TESTDATA.split("\\|");
			String ENV=  Data[0];
			String QUERY = Data[1];
            String FILEWORKOITEMID=Data[5]; 
            //String CUSTREFNUM=Data[6];  
            String Taskid=Data[2];
            String Queueid=Data[3];
            String TxnStatusCode=Data[4];
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String St_Prop_WiiD = FileUtilities.GetFilevalue(FILEWORKOITEMID);
		ResultSet _ResultSet=null;
		System.out.println("Workitmeid:"+St_Prop_WiiD);
		
		try{
			String query = QUERY;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,St_Prop_WiiD);
			//p.setString(2,CUSTREFNUM);
			_ResultSet=p.executeQuery();
		
	
		  while (_ResultSet.next()) {

			  Taskid = _ResultSet.getString("TASKID");

			  Queueid = _ResultSet.getString("QUEUEID");

			  TxnStatusCode = _ResultSet.getString("TRN_STATUS_CODE");


			  //_Storevalue.StoreValue(StoreValueinPropWiiD, InterfaceStatus); 
				Result1.put("TASKID",Taskid);
				  Result1.put("QUEUEID",Queueid);
				  Result1.put("TXN_STATUS_CODE",TxnStatusCode);
			}
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		finally{
			  
		}
		}
		return Result1;
	}
	public static HashMap<String, String> GetTxnDetails2(String TESTDATA) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		  HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		
		if (TESTDATA != null) {
			String[] Data = TESTDATA.split("\\|");
			
            String FileStatus=Data[0];
            String Exception_ID=Data[1];
            String Exception_Desc=Data[2];
            String Ext_Exception_ID=Data[3];
            String Ext_Exception_Desc=Data[4];
            String Tag_Name=Data[5];
            String FILE_NAME=Data[6]; 
            String SeqNo=Data[7];  
            
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String St_Prop_WiiD = FileUtilities.GetFilevalue(FILE_NAME);
		//System.out.println(St_Prop_WiiD);
		ResultSet _ResultSet=null;
		System.out.println("Workitmeid:"+St_Prop_WiiD);
		try{
			
			String query;
			
			query="SELECT A.FILE_STATUS,B.EXCEPTION_ID,B.EXCEPTION_DESC,B.external_exception_id,B.external_exception_desc, B.TAG_NAME FROM FILE_PROC_OPN A LEFT JOIN gtb_excep_grid_detail B ON A.WORKITEMID=B.WORKITEMID"
					+" WHERE A.WORKITEMID in"
					+ " (Select WORKITEMID from file_proc_opn where	file_name like'" + St_Prop_WiiD+ "%')and B.seqno in '" +SeqNo+"'";
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			_ResultSet=p.executeQuery();
		
	
		  while (_ResultSet.next()) {

			  FileStatus = _ResultSet.getString("FILE_STATUS");
			  System.out.println("file status="+FileStatus);
			  log.info("file status="+FileStatus);
			  Exception_ID = _ResultSet.getString("EXCEPTION_ID");
			  System.out.println("Exception-id="+Exception_ID);
			  log.info("Exception-id="+Exception_ID);
			  Exception_Desc = _ResultSet.getString("Exception_Desc");
			  System.out.println("Exception_desc="+Exception_Desc);
			  log.info("Exception_desc="+Exception_Desc);
			  Ext_Exception_ID = _ResultSet.getString("external_exception_id");
			  System.out.println("Ext_exception id="+Ext_Exception_ID);
			  log.info("Ext_exception id="+Ext_Exception_ID);
			  Ext_Exception_Desc = _ResultSet.getString("external_exception_desc");
			  System.out.println("Ext_exception_desc="+Ext_Exception_Desc);
			  log.info("Ext_exception_desc="+Ext_Exception_Desc);
			  Tag_Name = _ResultSet.getString("TAG_NAME");
			  System.out.println("Tag name="+Tag_Name);
			  log.info("Tag name="+Tag_Name);
			  //_Storevalue.StoreValue(StoreValueinPropWiiD, InterfaceStatus); 
				Result1.put("FILE_STATUS",FileStatus);
				Result1.put("EXCEPTION_ID",Exception_ID);
				Result1.put("EXCEPTION_DESC",Exception_Desc);
				Result1.put("EXT_EXCEPTION_ID",Ext_Exception_ID);
				Result1.put("EXT_EXCEPTION_DESC",Ext_Exception_Desc);
				Result1.put("TAG_NAME", Tag_Name);
			}
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		finally{

		}
		}
		return Result1;
	}
	public static HashMap<String, String> GetTxnFieldDetails(String TESTDATA) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		  HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		
		if (TESTDATA != null) {
			String[] Data = TESTDATA.split("\\|");
			String ENV=  Data[0];
			String QUERY = Data[1];
          String FILEWORKITEMID=Data[4]; 
          String St_Prop_WiiD = null;
          String Taskid=Data[2];
          String field=Data[3];
          System.out.println(field);
        	String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		if(FILEWORKITEMID.equalsIgnoreCase("VAR_PSHTXNWORKITEMID")||FILEWORKITEMID.equalsIgnoreCase("VAR_PSHFILEWORKITEMID"))
		{
		St_Prop_WiiD = FileUtilities.GetFilevalue(FILEWORKITEMID);
		}
		else
		{
			St_Prop_WiiD = FILEWORKITEMID;
		}
		ResultSet _ResultSet=null;
		System.out.println("Workitmeid:"+St_Prop_WiiD);
		try{
			String query = QUERY;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,St_Prop_WiiD);
			//p.setString(2,CUSTREFNUM);
			_ResultSet=p.executeQuery();
		
		System.out.println(_ResultSet);
	
		  while (_ResultSet.next()) {

			  Taskid = _ResultSet.getString(field);
			  log.info(Taskid);
			  System.out.println(_ResultSet);
			  System.out.println(Taskid);
			  System.out.println(field);
			 
				Result1.put(field,Taskid);
				 			}
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{

		}
		}
		return Result1;
	}
	
	
	public static HashMap<String, String> GetWiresDetails() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		  HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String srcrefnum;
		String wirerefnum;
		String FCREQUESTID;
		String txngowid;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHSQLWIRESMT");
		String workitemid = FileUtilities.GetFilevalue("VAR_PSHTXNWORKITEMID");
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query = PSH_Query;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,workitemid);
			ResultSet _ResultSet=p.executeQuery();
		
	
		  while (_ResultSet.next()) {

			  srcrefnum = _ResultSet.getString("SRC_REF_NUM");
			  System.out.println("SRC_REF_NUM"+srcrefnum);
			  log.info("SRC_REF_NUM"+srcrefnum);
			  wirerefnum = _ResultSet.getString("WIRE_EXT_REF_NUM");
			  System.out.println("WIRE_EXT_REF_NUM"+wirerefnum);
			  log.info("WIRE_EXT_REF_NUM"+wirerefnum);
			  FCREQUESTID = _ResultSet.getString("FC_REQUEST_ID");
			  txngowid = _ResultSet.getString("TXN_GOW_REQID");
			  //_Storevalue.StoreValue(StoreValueinPropWiiD, InterfaceStatus); 
				Result1.put("SRC_REF_NUM",srcrefnum);
				  Result1.put("WIRE_EXT_REF_NUM",wirerefnum);
				  Result1.put("FC_REQUEST_ID",FCREQUESTID);
				  Result1.put("TXN_GOW_REQID",txngowid);

			}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		
		return Result1;
	}
	public static HashMap<String, String> ValidateFileBV() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String file_status;
		String internal_ex_id;
		String internal_ex_desc;
		String external_ex_id;
		String external_ex_desc;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHFILEBV");
		String workitemid = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query = PSH_Query;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,workitemid);
			ResultSet _ResultSet=p.executeQuery();
		
	
		  while (_ResultSet.next()) {

			  file_status = _ResultSet.getString("FILE_STATUS");
			  System.out.println("FILE_STATUS"+ file_status);
			  log.info("FILE_STATUS"+ file_status);
				Result1.put("FILE_STATUS",file_status);

			}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
		log.debug(e.getMessage(),e.getCause());
		}
		
		return Result1;
	}
	public static HashMap<String, String> ValidateFileBVFromFileName() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String file_status;
		String internal_ex_id;
		String internal_ex_desc;
		String external_ex_id;
		String external_ex_desc;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHFILEBV");
		String workitemid = FileUtilities.GetFilevalue("VAR_FILENAME");
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query			= PSH_Query;
			query="SELECT A.FILE_STATUS,B.EXCEPTION_ID,B.EXCEPTION_DESC,B.external_exception_id,B.external_exception_desc, B.TAG_NAME FROM FILE_PROC_OPN A LEFT JOIN gtb_excep_grid_detail B ON A.WORKITEMID=B.WORKITEMID"
					+" WHERE A.WORKITEMID in"
					+ " (Select WORKITEMID from file_proc_opn where	file_name like'" + workitemid+ "%')";
			System.out.println(query);
			log.info(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			//p.setString(1,workitemid);
			ResultSet _ResultSet=p.executeQuery();
		
	
		  while (_ResultSet.next()) {

			  file_status = _ResultSet.getString("FILE_STATUS");
			  System.out.println("FILE_STATUS"+ file_status);
			  log.info("FILE_STATUS"+ file_status);
				Result1.put("FILE_STATUS",file_status);

			}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		
		return Result1;
	}
	
	public static HashMap<String, String> ValidateBV(String WORKITEMID) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String internal_ex_id;
		String internal_ex_desc;
		String external_ex_id;
		String external_ex_desc;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHBV");
		String workitemid = FileUtilities.GetFilevalue(WORKITEMID);
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query = PSH_Query;
			System.out.println(query);
			log.info(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,workitemid);
			ResultSet _ResultSet=p.executeQuery();
		
			System.out.println("here");
			log.info("here");
		  if(_ResultSet.next()) {
				System.out.println("here1");
	
	
			  internal_ex_id= _ResultSet.getString("EXCEPTION_ID");
			  System.out.println("EXCEPTION_ID:"+internal_ex_id);
			  log.info("EXCEPTION_ID:"+internal_ex_id);
			  internal_ex_desc = _ResultSet.getString("EXCEPTION_DESC");
			  System.out.println("EXCEPTION_DESC:"+ internal_ex_desc);
			  log.info("EXCEPTION_DESC:"+ internal_ex_desc);
			  external_ex_id= _ResultSet.getString("external_exception_id");
			  System.out.println("external_exception_id:"+external_ex_id);
			  log.info("external_exception_id:"+external_ex_id);
			  external_ex_desc = _ResultSet.getString("external_exception_desc");
			  System.out.println("external_exception_desc:"+ external_ex_desc);
			  log.info("external_exception_desc:"+ external_ex_desc);
			  //_Storevalue.StoreValue(StoreValueinPropWiiD, InterfaceStatus); 
			
				  Result1.put("EXCEPTION_ID",internal_ex_id);
				  Result1.put("EXCEPTION_DESC",internal_ex_desc);
				  Result1.put("EXT_EXCEPTION_ID",external_ex_id);
				  Result1.put("EXT_EXCEPTION_DESC",external_ex_desc);

			}
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		
		return Result1;
	}
	
	public static HashMap<String, String> ValidateBVwithClause(String WORKITEMID, String Clause) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String internal_ex_id;
		String internal_ex_desc;
		String external_ex_id;
		String external_ex_desc;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHBVwithClause");
		String workitemid = FileUtilities.GetFilevalue(WORKITEMID);
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query = PSH_Query;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,workitemid);
			p.setString(2,Clause);
			ResultSet _ResultSet=p.executeQuery();
		
			System.out.println("here");
		  if(_ResultSet.next()) {
				System.out.println("here1");
	
	
			  internal_ex_id= _ResultSet.getString("EXCEPTION_ID");
			  System.out.println("EXCEPTION_ID:"+internal_ex_id);
			  internal_ex_desc = _ResultSet.getString("EXCEPTION_DESC");
			  System.out.println("EXCEPTION_DESC:"+ internal_ex_desc);
			  external_ex_id= _ResultSet.getString("external_exception_id");
			  System.out.println("external_exception_id:"+external_ex_id);
			  external_ex_desc = _ResultSet.getString("external_exception_desc");
			  System.out.println("external_exception_desc:"+ external_ex_desc);
			  //_Storevalue.StoreValue(StoreValueinPropWiiD, InterfaceStatus); 
			
				  Result1.put("EXCEPTION_ID",internal_ex_id);
				  Result1.put("EXCEPTION_DESC",internal_ex_desc);
				  Result1.put("EXT_EXCEPTION_ID",external_ex_id);
				  Result1.put("EXT_EXCEPTION_DESC",external_ex_desc);

			}
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Result1;
	}

	public static HashMap<String, String> GetBatchWorkitemid(String WORKITEMID) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String, String> Result1=new HashMap();   
		String BatchWorkitemid;  
		String BatchStatus;  
		Keywordoperations _Storevalue=new Keywordoperations();
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHBATCH");
		String workitemid = FileUtilities.GetFilevalue(WORKITEMID);
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query = PSH_Query;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,workitemid);
			ResultSet _ResultSet=p.executeQuery();
		
			System.out.println("here");
		  while (_ResultSet.next()) {
				System.out.println("here1");
	
	
				BatchStatus= _ResultSet.getString("BATCH_STS_CD");
			  System.out.println("batch status:"+BatchStatus);
			  log.info("batch status:"+BatchStatus);
			  BatchWorkitemid = _ResultSet.getString("WORKITEMID");
			  System.out.println("WORKITEMID:"+ BatchWorkitemid);
			  log.info("WORKITEMID:"+ BatchWorkitemid);
			    _Storevalue.StoreValue("VAR_BATCHWORKITEMID", BatchWorkitemid); 
			
				  Result1.put("BatchStatus",BatchStatus);

			}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		
		return Result1;
	}
	
	public static void GetpshFileId() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();

		String fileid;  
	
		Keywordoperations _Storevalue=new Keywordoperations();
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHFILEID");
		String filename = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
	
		try{
			String query = PSH_Query;
			System.out.println(query);
			log.info(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,filename);
			ResultSet _ResultSet=p.executeQuery();
		
			System.out.println("here");
		  while (_ResultSet.next()) {
				System.out.println("here1");
	
	
			fileid= _ResultSet.getString("MESSAGE_ID");
			  System.out.println("FILEID:"+fileid);
	log.info("FILEID:"+fileid);
			    _Storevalue.StoreValue("VAR_PSHFILEID", fileid); 
			
				  

			}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
	}
	public static String GetTXNWORKITEMID() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();

		String fileid = null;  
		String trn_status=null;
		Keywordoperations _Storevalue=new Keywordoperations();
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query="Select workitemid,trn_status_code from txn_proc_opn where file_workitem_id =?";
		String filename = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
	System.out.println(filename);
		try{
			String query = PSH_Query;
			System.out.println(query);
			log.info(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,filename);
			ResultSet _ResultSet=p.executeQuery();

		  while (_ResultSet.next()) {
				System.out.println("here1");
			  fileid= _ResultSet.getString("WORKITEMID");
			  trn_status=_ResultSet.getString("TRN_STATUS_CODE");
			  System.out.println("TXN_WORKITEMID:"+fileid);
			  System.out.println("TXN_STATUS:"+trn_status);
	log.info("PSHTXNWORKITEMID:"+fileid);
			    _Storevalue.StoreValue("VAR_PSHTXNWORKITEMID", fileid); 
		}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
			trn_status="FAIL";
		}
		return trn_status;
	}

		public static void GetTextQuoteId(String custrefnum) throws Exception{
			FileUtilities FileUtilities=new FileUtilities();

			String fileid;  
		
			Keywordoperations _Storevalue=new Keywordoperations();
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			String PSH_Query=PropertyUtils.readProperty("PSHFQUOTEID");
			String fileID = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
		
			try{
				String query = PSH_Query;
				System.out.println(query);
				log.info(query);
				Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
				PreparedStatement p = connection.prepareStatement(query);
				p.setString(1,custrefnum);
				p.setString(2,fileID);
				ResultSet _ResultSet=p.executeQuery();

			  while (_ResultSet.next()) {
					System.out.println("here1");
				String quoteid= _ResultSet.getString("FX_QUOTE_ID");
				  System.out.println("FX_QUOTE_ID:"+quoteid);
				  log.info("FX_QUOTE_ID:"+quoteid);
		
				    _Storevalue.StoreValue("VAR_QUOTEID", quoteid); 
				}
			  _ResultSet.close();
			  connection.close();
			}catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage(),e.getCause());
			}
			
			
			}
		
		public static String GetTestSeq() throws Exception{
		
		
			Keywordoperations _Storevalue=new Keywordoperations();
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			String PSH_Query="select TEST_SEQ.nextval AS TEST_SEQ from dual";
			String testseq =null;
		
			try{
				String query = PSH_Query;
				System.out.println(query);
				Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
				PreparedStatement p = connection.prepareStatement(query);
			
				ResultSet _ResultSet=p.executeQuery();

			  while (_ResultSet.next()) {
					System.out.println("here1");
				testseq= _ResultSet.getString("TEST_SEQ");
				  System.out.println("TEST_SEQ:"+testseq); 
				  log.info("TEST_SEQ:"+testseq);
				}
			  _ResultSet.close();
			  connection.close();
			}catch (Exception e) {
				e.printStackTrace();
				log.debug(e.getMessage(),e.getCause());
			}
			
			return testseq;
			}
		
		public static void PREExecute_QueriesCC2(String SQL_Path) throws Exception {
		Connection connection=null;
		String scriptFilePath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\PRE QUERIES\\"+SQL_Path;
		Reader reader = null;
		Statement stmt=null;

		String classname = PropertyUtils.readProperty("PSH_CLASS");
		
		String URL=PropertyUtils.readProperty("PSH_DBURLC");
		String user=PropertyUtils.readProperty("PSH_DBUserName");
		String pwd=PropertyUtils.readProperty("PSH_DBUserPass");

		try {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		connection=DriverManager.getConnection(URL,user,pwd);
		stmt = connection.createStatement();
		ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
		reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
		scriptExecutor.runScript(reader);
		System.out.println("Executed Pre Queries Successfully");
		log.info("Executed Pre Queries Successfully");
		}catch (Exception e) {
		e.printStackTrace();
		log.debug(e.getMessage(),e.getCause());
		}finally {
		stmt.close();
		connection.close();
		}
		}
	
	public static HashMap<String, String> GetCorrFeeDetails () throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String, String> Result1=new HashMap();   
		String TxnCorrFileStatus;  
		String TxnCorrFeeStatusCd;  
		String TxnCorrFeeStatusDesc; 
		String TxnCorrStatusCd;  
		String TxnCorrStatusDesc; 
		Keywordoperations _Storevalue=new Keywordoperations();
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String PSH_Query=PropertyUtils.readProperty("PSHCORRFEE");
		String workitemid = FileUtilities.GetFilevalue("VAR_PSHTXNWORKITEMID");
		System.out.println("Workitmeid:"+workitemid);
		try{
			String query = PSH_Query;
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			PreparedStatement p = connection.prepareStatement(query);
			p.setString(1,workitemid);
			ResultSet _ResultSet=p.executeQuery();
		  while (_ResultSet.next()) {
				System.out.println("here1");
	
	
			  TxnCorrFileStatus= _ResultSet.getString("TXN_CORR_FEE_STATUS");
			  System.out.println("TxnCorrFileStatus:"+TxnCorrFileStatus);
			  log.info("TxnCorrFileStatus:"+TxnCorrFileStatus);
			  Result1.put("TxnCorrFileStatus",TxnCorrFileStatus);

			  TxnCorrFeeStatusCd = _ResultSet.getString("RBFEE_STATUS_RSN_CD");
			  System.out.println("TxnCorrFeeStatusCd:"+ TxnCorrFeeStatusCd);
			  log.info("TxnCorrFeeStatusCd:"+ TxnCorrFeeStatusCd);
			  Result1.put("TxnCorrFeeStatusCd",TxnCorrFeeStatusCd);
			  
			  TxnCorrFeeStatusDesc = _ResultSet.getString("RBFEE_STATUS_RSN_DESC");
			  System.out.println("TxnCorrFeeStatusDesc:"+ TxnCorrFeeStatusDesc);
			  log.info("TxnCorrFeeStatusDesc:"+ TxnCorrFeeStatusDesc);
			  Result1.put("TxnCorrFeeStatusDesc",TxnCorrFeeStatusDesc);
			  
			  TxnCorrStatusCd = _ResultSet.getString("RBFEE_STATUS_CD");
			  System.out.println("TxnCorrStatusCd:"+ TxnCorrStatusCd);
			  log.info("TxnCorrStatusCd:"+ TxnCorrStatusCd);
			  Result1.put("TxnCorrStatusCd",TxnCorrStatusCd);
			  
			  TxnCorrStatusDesc = _ResultSet.getString("RBFEE_STATUS_DESC");
			  System.out.println("TxnCorrStatusDesc:"+ TxnCorrStatusDesc);
			  log.info("TxnCorrStatusDesc:"+ TxnCorrStatusDesc);
			  Result1.put("TxnCorrStatusDesc",TxnCorrStatusDesc);
			
	
			}
		  _ResultSet.close();
		  connection.close();

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
		}
		
		return Result1;
	}
	
	

public static String CheckGOWStatus(String custrefnum) throws IOException, SQLException {
	FileUtilities FileUtilities=new FileUtilities();
	HashMap<String, String> Result1=new HashMap();   
	String TxnGowStatus="NA";   
	Keywordoperations _Storevalue=new Keywordoperations();
	String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
	String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
	String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
	String PSH_Query=PropertyUtils.readProperty("PSHGOWSTATUS");
	String workitemid = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
	System.out.println("Workitmeid:"+workitemid);
	Connection connection=null;
	try{
		String query = PSH_Query;
		System.out.println(query);
		log.info(query);
		Class.forName("oracle.jdbc.driver.OracleDriver");
		connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
		PreparedStatement p = connection.prepareStatement(query);
		p.setString(1,workitemid);
		p.setString(2,custrefnum);
		ResultSet _ResultSet=p.executeQuery();
	  while (_ResultSet.next()) {
			System.out.println("here1");


			TxnGowStatus= _ResultSet.getString("TXN_GOW_ACCT_STAT");
		  System.out.println("TxnGowStatus:"+TxnGowStatus);
		log.info("TxnGowStatus:"+TxnGowStatus);
		  
		}
	  _ResultSet.close();
	  connection.close();
	}catch (Exception e) {
		e.printStackTrace();
		log.debug(e.getMessage(),e.getCause());
		  connection.close();
	}
	
	return TxnGowStatus;
}	

public static int Update_FXID(String TESTDATA) throws Exception {
	String URL = PropertyUtils.readProperty("PSH_DBURL");
	String user = PropertyUtils.readProperty("PSH_DBUserName");
	String pass = PropertyUtils.readProperty("PSH_DBUserPass");
	int row=0;
	Connection connection=null;
	Statement stmt=null;
	if (TESTDATA != null) {
		String[] Data = TESTDATA.split("\\|");
		String contractno=  Data[0];
		String Query = "UPDATE STUB_FXSPOT_CONT SET QUOTEID=? WHERE CONTRACTNO=?";
		String QUOTEIid =FileUtilities.GetFilevalue("VAR_QUOTEID");

	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);

				  PreparedStatement preparedStatement = connection.prepareStatement(Query); 

			            preparedStatement.setString(1,QUOTEIid);
			            preparedStatement.setString(2,contractno);
			   
				System.out.println("Udpate query executed Successfully!!"+TESTDATA);
				log.info("Udpate query executed Successfully!!"+TESTDATA);
			    row = preparedStatement.executeUpdate();

	            // rows affected
	            System.out.println("No of rows affected"+row);
	            log.info("No of rows affected"+row);
	            

		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
			
		}finally {
			 connection.close();
		}	
	}
	return row;
	}

public static int Set_PSH_Dates(String TESTDATA) throws Exception {
	String URL = PropertyUtils.readProperty("PSH_DBURL");
	String user = PropertyUtils.readProperty("PSH_DBUserName");
	String pass = PropertyUtils.readProperty("PSH_DBUserPass");
	
	int row=0;
	Connection connection=null;
	Statement stmt=null;
	if (TESTDATA != null) {
		String[] Data = TESTDATA.split("\\|");
		String ENV=  Data[0];
		String Query = Data[1];
		String date = Data[2];
		String txnid =Data[3];
		String TxnId = null;
		java.util.Date utilDate = new java.util.Date();
	    System.out.println("Util date in Java : " + utilDate);

	   
	    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();
		System.out.println(dateFormat.format(currentDate));
		log.info(dateFormat.format(currentDate));
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
	    
	    if(date.equals("CBD"))
	    {
	    	
	    	c.setTime(currentDate);
	    }
	    else if(date.contains("+"))
	    {
	    	String NoOfDays = date.substring(4);
			   int Days=Integer.parseInt(NoOfDays); 
	    	   	c.add(Calendar.DATE, Days);
	    }
	    else
	    {
	    	
		    	String NoOfDays = date.substring(4);
				   int Days=Integer.parseInt(NoOfDays); 
		    	   c.add(Calendar.DATE, -Days);
		    
	    }
	    Date finaldatevalue = c.getTime();
	   String finaldate = dateFormat.format(finaldatevalue).toString();
	
	    System.out.println("SQL date in Java : " + finaldate);
	 
	    if(txnid.contains("VAR_"))
	    {
		TxnId=Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
		System.out.println("Current Txnsid from file"+TxnId);
	    }
	    else
	    {
	    	TxnId = txnid;
			System.out.println("Current Txnsid from file"+TxnId);
	    }
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@"+URL,user,pass);

				  PreparedStatement preparedStatement = connection.prepareStatement(Query); 

			          
			            preparedStatement.setString(1,finaldate);
			            preparedStatement.setString(2,TxnId);
			            System.out.println(preparedStatement.toString());
			            
				System.out.println("Udpate query executed Successfully!!"+TESTDATA);
			    row = preparedStatement.executeUpdate();

	            // rows affected
	            System.out.println("No of rows affected"+row);
	            log.info("No of rows affected"+row);
	            preparedStatement.close();

	           
	            }
		catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage(),e.getCause());
			 connection.close();
		}finally {
			 connection.close();
		}	
	}
	return row;
	}

public static Date addDays(Date date, int days)
{
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.add(Calendar.DATE, days);
    return new Date(c.getTimeInMillis());
}


public static void main(String[] args) throws Exception{
	DBConnector DBConnect=new DBConnector();
	DBConnect.GetTXNWORKITEMID();
	//getSourceRefNum("SANITY_TC024");
	//DBConnect.Execute_Update_PSHQueryDate("PSH|update txn_proc_opn set dr_exec_date =? where workitemid IN (?)|VAR_CURRDATE|VAR_PSHTXNWORKITEMID");
	//HashMap<String, String> status=DBConnect.GetTxnDetails("PSH|select owi.taskid,owi.queueid,tpo.trn_status_code,tpo.cust_ref_num,tpo.file_workitem_id,tpo.workitemid,oq.queuelabel,oq.queuedescription from txn_proc_opn tpo,ow_workitem_instance owi,ow_queue oq where tpo.workitemid=owi.workitemid  and oq.queueid=owi.queueid and tpo.File_Workitem_Id in (select workitemid From File_Proc_Opn where file_workitem_id in (?))|423|61261|TXSUSP|VAR_PSHFILEWORKITEMID");
	//DBConnect.DBConnector("PSH|SELECT WORKITEMID, FILE_WORKITEM_ID, TXN_STATUS, TRN_STATUS_CODE FROM TXN_PROC_OPN WHERE WORKITEMID|TRN_STATUS_CODE|VAR_WORKITEMID|VAR_DBWORKITEMID");
	//HashMap<String, String> status=DBConnect.PSHInterfaceDB("ENVOYGETBAL|SELECT STATUS,TXN_RSN_DESC,TXN_RSN_CD,INTERFACE_RESPONSE_CODE,INCOMING_MSG FROM PSH_C.INTERFACE_LOG WHERE CORRELATION_ID=? AND INTERFACE_ID=?|STATUS|VAR_CORRID|VAR_INTERFACESTATUS");
	//HashMap<String, String> status=DBConnect.ValidateFileBV();
	//HashMap<String, String> status=DBConnect.ValidateBV("VAR_PSHTXNWORKITEMID");
	//HashMap<String, String> status=DBConnect.GetBatchWorkitemid("VAR_PSHFILEWORKITEMID");
//	 for(Map.Entry m:status.entrySet()){    
	//      System.out.println("Values from Resultset"+m.getKey()+" "+m.getValue());    
	//     }  
	//GetpshFileId();
	// GetTextQuoteId("YTAAJJJE2E6");
	//PREExecute_QueriesCC2("CC2 E2E.sql");
	//GetCorrFeeDetails();
	//InsertHoliday("CBD|W");
	//CheckGOWStatus("YTAAJJJE2E1");
/*	
	FileUtilities FileUtilities=new FileUtilities();

	String fileid;  

	Keywordoperations _Storevalue=new Keywordoperations();
	String PSH_DBURL="10.240.128.40:CIBCDIT1";
	String PSH_DBUserName="PSH_C";
	String PSH_DBUserPass="PSH_C";
	String PSH_Query=PropertyUtils.readProperty("PSHFILEID");
	String filename = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");

	try{
		String query = PSH_Query;
		System.out.println(query);
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.240.128.40:1521:CIBCDIT1","PSH_C","PSH_C");
		PreparedStatement p = connection.prepareStatement(query);
		p.setString(1,filename);
		ResultSet _ResultSet=p.executeQuery();
	
		System.out.println("here");
	  while (_ResultSet.next()) {
			System.out.println("here1");


		fileid= _ResultSet.getString("MESSAGE_ID");
		  System.out.println("FILEID:"+fileid);

		    _Storevalue.StoreValue("VAR_PSHFILEID", fileid); 
		
			  

		}
	  _ResultSet.close();
	  connection.close();

	}catch (Exception e) {
		e.printStackTrace();
	}
	*/
}

}
