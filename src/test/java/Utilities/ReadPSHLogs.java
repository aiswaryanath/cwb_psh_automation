package Utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Scanner;

import org.apache.poi.util.SystemOutLogger;

public class ReadPSHLogs{  
    public static void main(String[] args) throws IOException {  
            StringBuilder s=new StringBuilder("");
      
        	File file = new File("D:\\CBX_Automation\\Download Log\\ipshlog.log"); 
        	BufferedReader br;

				br = new BufferedReader(new FileReader(file));
				String st; 
				checkdata("D:\\CBX_Automation\\Download Log\\ipshlog.log","lFileName::10006732_MT101xSANx014_2020-01-31_111213_MT104xSANx014xxxxx003652.MT104.P.dat.001.FTS2020013114271480934.D310120.T142730");
	        	  while ((st = br.readLine()) != null) 
	        	  {
	        	  //  System.out.println(st); 
	        	    
	        	  }
	
    }    
    

    public static int checkdata(String filename,String testWord) {

        String path = ""; //ADD YOUR PATH HERE
        //String fileName = "D:\\CBX_Automation\\Download Log\\ipshlog.log";
        String fileName = filename;
        //String testWord = "lFileName::10006732_MT101xSANx014_2020-01-31_111213_MT104xSANx014xxxxx003652.MT104.P.dat.001.FTS2020013114271480934.D310120.T142730"; //CHANGE THIS IF YOU WANT
        int tLen = testWord.length();
        int wordCntr = 0;
        String file = path + fileName;
        boolean check;

        try{
        	File file2 = new File(filename); 
            BufferedReader br = new BufferedReader(new FileReader(file2));
            String strLine;        
            //Read File Line By Line
            while((strLine = br.readLine()) != null){                
                //check to see whether testWord occurs at least once in the line of text
                check = strLine.toLowerCase().contains(testWord.toLowerCase());
                if(check){                    
                    //get the line, and parse its words into a String array
                    String[] lineWords = strLine.split("\\s+");                    
                    for(String w : lineWords){
                        //first see if the word is as least as long as the testWord
                        if(w.length() >= tLen){
                            /*
                            1) grab the specific word, minus whitespace
                            2) check to see whether the first part of it having same length
                                as testWord is equivalent to testWord, ignoring case
                            */
                           // String word = w.substring(0,tLen).trim();   
                            String word = w; 
                            System.out.println(word); 
                            if(word.equalsIgnoreCase(testWord)){                                
                                wordCntr++;
                            }                            
                        }
                    }                    
                }   
            }            
            System.out.println("total is: " + wordCntr);
        //Close the input stream
        br.close();
        } catch(Exception e){
            e.printStackTrace();
        }
        return wordCntr;
    }
    public static void checkdata1() {

        String path = ""; //ADD YOUR PATH HERE
        String fileName = "D:\\CBX_Automation\\Download Log\\ipshlog.log";
        String testWord = "lFileName::10006732_MT101xSANx014_2020-01-31_111213_MT104xSANx014xxxxx003652.MT104.P.dat.001.FTS2020013114271480934.D310120.T142730"; //CHANGE THIS IF YOU WANT
        int tLen = testWord.length();
        int wordCntr = 0;
        String file = path + fileName;
        boolean check;

        try{
        	File file2 = new File("D:\\CBX_Automation\\Download Log\\ipshlog.log"); 
            BufferedReader br = new BufferedReader(new FileReader(file2));
            String strLine;   
            String v="";
            //Read File Line By Line
            while((strLine = br.readLine()) != null){                
                //check to see whether testWord occurs at least once in the line of text
                check = strLine.toLowerCase().contains(testWord.toLowerCase());
      
                if(check){      
                    v=v+strLine.toLowerCase();
                    System.out.println(v);
                    
                               if(v.equalsIgnoreCase(testWord)){                                
                                wordCntr++;
                            }                            
              
            }   
            }
            System.out.println("total is: " + wordCntr);
        //Close the input stream
        br.close();
    
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}  