package Utilities;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class ExcelConnect {

	public static Connection getConnection(Connection connection,String Excel_path) throws FileNotFoundException, IOException {
		try{
			Fillo fill=new Fillo();
			connection = fill.getConnection(Excel_path);
		}catch (Exception e) {
			e.getStackTrace();
		}	
		return connection;
	}

	public static final Recordset executeQuery(final Connection connection,String _str) {
    try {
        return connection.executeQuery(_str);
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
}
	
}
