package Utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Test_FILE {

	public static void main(String[] args) throws Exception{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date myDate = new Date(System.currentTimeMillis());
		String From = dateFormat.format(myDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		 cal.add(Calendar.DATE, 2);
		//cal.add(Calendar.DATE, -60);
		String To = dateFormat.format(cal.getTime());

		String Todate = new SimpleDateFormat("dd").format(cal.getTime()); 
		System.out.println("Todate--->" + Todate);
		System.out.println("DateTo--->" + To);
		Fillo fill=new Fillo();
		Connection conn=fill.getConnection(System.getProperty("user.dir")+"\\TestData\\TestCase_Steps.xls");
		Recordset rs=conn.executeQuery("Select * from Main_Controller where RunStatus ='Yes'");
		while (rs.next()) {
				String _TestCases=rs.getField("TestCases");
				System.out.println(_TestCases);
		}		
	}

}
