package Utilities;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import Utilities.DBConnector;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import Logger.LoggerUtils;
import UIOperations.Keywordoperations;

public class WiresAckMT103MT199 {
	static Logger logger = LoggerUtils.getLogger();
	public static HashMap<String,String> Result=new HashMap<String,String>(); 
	public static WebDriver driver=null;
	static String srcrefnum;
	static String wirerefnum;
	static String txngowid;
	static String fcrequestid;
	public static String dbconnect()
	{
	DBConnector db=new DBConnector();
	try {
		Result=db.GetWiresDetails();
		srcrefnum=Result.get("SRC_REF_NUM");
		wirerefnum=Result.get("WIRE_EXT_REF_NUM");
		txngowid=Result.get("TXN_GOW_REQID");
		fcrequestid=Result.get("FC_REQUEST_ID");		
		System.out.println("srcrefnum:"+srcrefnum+"   "+"wirerefnum:"+wirerefnum);
		System.out.println("txngowreqid:"+txngowid+"   "+"fcreqid:"+fcrequestid);
		logger.info("srcrefnum:"+srcrefnum+"wirerefnum:"+wirerefnum);
	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return wirerefnum;
	}
	
	
	public static void AsyncAction(String testData) throws Exception {
		FileUtilities FileUtilities=new FileUtilities();
		String txn_ref_num=FileUtilities.GetFilevalue("VAR_PSHTXNWORKITEMID");
		String prefix="<?xml version=\"1.0\" encoding=\"UTF-8\"?><s11:Envelope xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\" xmlns:payhb=\"http://emf.cibc.com/emb/slms/\" xmlns:psh=\"http://emf.cibc.com/emb/slms/\" xmlns:fir=\"http://fircosoft.com/\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:date=\"http://exslt.org/dates-and-times\" xmlns:s11=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><s11:Header><wsa:To ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\">http://emf.cibc.com/emb/psh/Adapter/</wsa:To><wsa:From ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsa:Address>http://emf.cibc.com/emb/slms/Adapter/</wsa:Address></wsa:From><wsa:MessageID ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\">a97eb6f61-741b-488d-ac12-117bcce6494d</wsa:MessageID><wsa:ReplyTo ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsa:Address>http://schemas.xmlsoap.org/ws/2004/03/addressing/role/anonymous</wsa:Address></wsa:ReplyTo><wsa:FaultTo ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsa:Address>http://schemas.xmlsoap.org/ws/2004/03/addressing/role/anonymous</wsa:Address></wsa:FaultTo><wsa:Action ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\">http://services.cibc.com/psh/#UpdateTransactionStatus/Notify/</wsa:Action><wsu:Timestamp ns0:mustUnderstand=\"0\" xmlns:ns0=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsu:Created>2017-01-13T14:36:34-05:00</wsu:Created></wsu:Timestamp></s11:Header><s11:Body><psh:msg_WMQOUT xmlns:psh=\"http://emf.cibc.com/emb/psh/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><psh:VersionTag>1</psh:VersionTag><psh:MessageID>";
		String sufix="</psh:MessageID><psh:Unit>BNYM-ACH</psh:Unit><psh:BusinessUnit>ROOT</psh:BusinessUnit><psh:ApplicationCode>34358</psh:ApplicationCode><psh:Bypass>0</psh:Bypass><psh:BypassRecovery>0</psh:BypassRecovery><psh:MessageStatus>";
		String end="</psh:MessageStatus><psh:StatusLabel>FAILED</psh:StatusLabel><psh:StatusComment>Async Test</psh:StatusComment><psh:StatusOwner>rsingh</psh:StatusOwner><psh:MessageChechsum/><psh:SystemID>DIN20170112161430-01161-1622</psh:SystemID><psh:AlertsDetail/><psh:MessageData/></psh:msg_WMQOUT></s11:Body></s11:Envelope>";
		Keywordoperations _Storevalue=new Keywordoperations();
			
				String Final_msg = prefix + txn_ref_num + sufix+testData+end;
				System.out.println("Async message final" + Final_msg);
			logger.info("Async message final" + Final_msg);

				Toolkit toolkit = Toolkit.getDefaultToolkit();
				Clipboard clipboard = toolkit.getSystemClipboard();
				StringSelection strSel = new StringSelection(Final_msg);
				clipboard.setContents(strSel, null);
				Robot rb = new Robot();


				ChromeOptions options = new ChromeOptions();
				//options.setProxy(null);
				WebDriverManager.chromedriver().setup();
				//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
				//options.addArguments("headless");
				  driver = new ChromeDriver(options);
				
				driver.manage().window().maximize();
				driver.get(PropertyUtils.readProperty("ASYNCJSP"));
				Nfocusonwindow("Testing Async Sanctions");
				driver.findElement(By.xpath("//textarea[@name='txtName']")).click();
				rb.keyPress(KeyEvent.VK_CONTROL);
				rb.keyPress(KeyEvent.VK_V);
				rb.keyRelease(KeyEvent.VK_CONTROL);
				rb.keyRelease(KeyEvent.VK_V);
				logger.info("msg done");
				Thread.sleep(3000);
				driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
				Thread.sleep(3000);

				
			}
	public static void ackMt103(String prefix) throws Exception
	{

		logger.info("----start ackMt103----");

			String const_date = null;

			Connection conn = null;
			try
			{
				String mt103msg="PSH4OTO180814100009@@@I000******************       ######090814";
				Date date = new Date();
				DateFormat df = new SimpleDateFormat("yyMMdd");
				String dateAsISOString = df.format(date);
				System.out.println(dateAsISOString);
				Random rand = new Random(); 
				  
		        // Generate random integers in range 0 to 999 
		        int rand_int1 = rand.nextInt(1000); 
		        String uniqueid=String.valueOf(rand_int1);
		        mt103msg=mt103msg.replace("@@@", uniqueid);
		        mt103msg=mt103msg.replace("######", dateAsISOString);
		        mt103msg=mt103msg.replace("******************", srcrefnum);
		        System.out.println("mt103msg:"+mt103msg);
		        logger.info("mt103msg:"+mt103msg);
					if (srcrefnum != null)
					{
						String WiresMT103URL = PropertyUtils.readProperty("PSHMT103URL");
						String Final_msg = mt103msg;
						logger.info("Final_msg--->" + Final_msg);
						// Thread.sleep(500000);
						Toolkit toolkit = Toolkit.getDefaultToolkit();
						Clipboard clipboard = toolkit.getSystemClipboard();
						StringSelection strSel = new StringSelection(Final_msg);
						clipboard.setContents(strSel, null);
						Robot robot = new Robot();
						robot.mouseMove(20, 10);
						ChromeOptions options = new ChromeOptions();
						//options.setProxy(null);
						WebDriverManager.chromedriver().setup();
						//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
						options.addArguments("headless");
						  driver = new ChromeDriver(options);
						    
							driver.get(WiresMT103URL);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
						Nfocusonwindow("Testing Async Sanctions");
						driver.findElement(By.xpath("//textarea[@name='txtName']")).sendKeys(Final_msg);

						logger.info("msg done");
						// Thread.sleep(15000);
						driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
						Thread.sleep(5000);
						driver.quit();
						Thread.sleep(15000);
					}
					else
					{
						logger.info("SrcRefnum is Empty");
					}
				
			}
			finally
			{}
			
			logger.info("----end ackMt103----");
		

	}
	
	public static void MT199(String prefix) throws Exception
	{

		logger.info("----start mt199----");


			try
			{
				String mt199msg="IWSL    PSH4L199              TO######775555@@@I1650 27CIBCCATT XXX000010000 00001083318   01179199 02    \n"+    
"{1:F01CIBCCAT0ACMO0000000004}\n"+
"{2:o1991523170531PNBPUS3NAXXX00000000001705311523N}\n"+
"{3:\n"+
"{121:xxxxxxxx-xxxx-4xxx-1234-18121103xxxx}}\n"+
"{4:\n"+
":20:ICN1234567*****\n"+
":21:#var3\n"+
":79:/SWF/IGNORE REF AS CODE CLR SYS IS SWF-}\n"+
//":79:"+prefix+"-}\n"+
"{5:}";
				Date date = new Date();
				DateFormat df = new SimpleDateFormat("yyMMdd");
				String dateAsISOString = df.format(date);
				System.out.println(dateAsISOString);
				Random rand = new Random(); 
				  
		        // Generate random integers in range 0 to 999 
		        int rand_int1 = rand.nextInt(1000); 
		        int rand_int2 = rand.nextInt(100000);
		        String uniqueid=String.valueOf(rand_int1);
		        String uniqueid2=String.valueOf(rand_int2);
		        mt199msg=mt199msg.replace("@@@", uniqueid);
		        mt199msg=mt199msg.replace("*****", uniqueid2);
		        mt199msg=mt199msg.replace("######", dateAsISOString);
		        mt199msg=mt199msg.replace("#var3", wirerefnum);
		        System.out.println("mt199msg:"+mt199msg);
logger.info("mt199msg:"+mt199msg);
					if (wirerefnum != null)
					{
						String WiresMT199URL = PropertyUtils.readProperty("PSHMT900JSP");
						String Final_msg = mt199msg;
						logger.info("Final_msg--->" + Final_msg);
						// Thread.sleep(500000);
						Toolkit toolkit = Toolkit.getDefaultToolkit();
						Clipboard clipboard = toolkit.getSystemClipboard();
						StringSelection strSel = new StringSelection(Final_msg);
						clipboard.setContents(strSel, null);
						Robot robot = new Robot();
						robot.mouseMove(20, 10);
						ChromeOptions options = new ChromeOptions();
						//options.setProxy(null);
						options.addArguments("headless");
						WebDriverManager.chromedriver().setup();
						//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
						  driver = new ChromeDriver(options);
						    
							driver.get(WiresMT199URL);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
							Nfocusonwindow("Testing Async Sanctions");
						driver.findElement(By.xpath("//textarea[@name='txtName']")).sendKeys(Final_msg);

						logger.info("msg done");
						// Thread.sleep(15000);
						driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
						Thread.sleep(5000);
						driver.quit();
						Thread.sleep(15000);
			
					}
					else
					{
						logger.info("WIRERefnum is Empty");
					}
				
			}
			finally
			{
				
			}
			
			logger.info("----end mt199----");
		

	}
	
	public static void MT900(String prefix) throws Exception
	{

		logger.info("----start mt900----");


			try
			{
				String mt900msg="IWSL    PSH4L900              TO######000511@@@I1545 09CIBCCAT0ACMO00002\n"+
			"1545 09PNBPUS3NAXXX04760\n"+
			"900 02\n"+			
"{1:F01CIBCCAT0ACMO0000000002}{2:O9001545190109PNBPUS3NAXXX00000000011901091545N}{4:\n"+
":20:######00035****\n"+
":21:#var3\n"+
":25:1067516\n"+
":32A:190109USD25900,64\n"+
":52A:CIBCCATT\n"+
":72:/BNF/0800/TIME/02.30\n"+
//"///FEDREF/229B1QGC01C010131\n"+
"///"+prefix+"\n"+
"-}{5:}-";

				Date date = new Date();
				DateFormat df = new SimpleDateFormat("yyMMdd");
				String dateAsISOString = df.format(date);
				System.out.println(dateAsISOString);
				Random rand = new Random(); 
				  
		        // Generate random integers in range 0 to 999 
		        int rand_int1 = rand.nextInt(1000); 
		        int rand_int2 = rand.nextInt(100000);
		        String uniqueid=String.valueOf(rand_int1);
		        String uniqueid2=String.valueOf(rand_int2);
		        mt900msg=mt900msg.replace("@@@", uniqueid);
		        mt900msg=mt900msg.replace("****", uniqueid2);
		        mt900msg=mt900msg.replace("######", dateAsISOString);
		        mt900msg=mt900msg.replace("#var3", wirerefnum);
		        System.out.println("mt900msg:"+mt900msg);
		        logger.info("mt900msg:"+mt900msg);
					if (wirerefnum != null)
					{
						String WiresMT199URL = PropertyUtils.readProperty("PSHMT900JSP");
						String Final_msg = mt900msg;
						logger.info("Final_msg--->" + Final_msg);
						// Thread.sleep(500000);
						Toolkit toolkit = Toolkit.getDefaultToolkit();
						Clipboard clipboard = toolkit.getSystemClipboard();
						StringSelection strSel = new StringSelection(Final_msg);
						clipboard.setContents(strSel, null);
						Robot robot = new Robot();
						robot.mouseMove(20, 10);
						ChromeOptions options = new ChromeOptions();
						//options.setProxy(null);
						options.addArguments("headless");
						WebDriverManager.chromedriver().setup();
					//	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
						  driver = new ChromeDriver(options);
						    
							driver.get(WiresMT199URL);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
							Nfocusonwindow("Testing Async Sanctions");
						driver.findElement(By.xpath("//textarea[@name='txtName']")).sendKeys(Final_msg);

						logger.info("msg done");
						// Thread.sleep(15000);
						driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
						Thread.sleep(5000);
						driver.quit();
			
					}
					else
					{
						logger.info("WIRERefnum is Empty");
					}
				
			}
			finally
			{
				
			}
			
			logger.info("----end mt900----");
		

	}
	
	public static boolean Nfocusonwindow(String title)
	{
		boolean flg = true;
		try
		{

			logger.info("----Entering Nfocusonwindow----");

			logger.info("current window:-->" + driver.getTitle());
			if (!title.equalsIgnoreCase(driver.getTitle()))
			{
				logger.info("searching for :-->" + title);
				Set<String> window = driver.getWindowHandles();
				logger.info("total no of windows:-->" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{
					logger.info("I am here 1");
					driver.switchTo().window(itr.next());
					logger.info("I am here 2  " + driver.getTitle());
					if (title.equalsIgnoreCase(driver.getTitle()))
					{
						logger.info("I am here 3");
						break;
					}

				}

			}
			else
			{
				logger.info("You are on right window");
			}
			if ("Internet Explorer cannot display the webpage".equalsIgnoreCase(driver.getTitle()))
			{
				driver.close();
				Set<String> window = driver.getWindowHandles();
				logger.info("total no of windows:-->" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{
					logger.info("I am here 1");
					driver.switchTo().window(itr.next());
					logger.info("I am here 2  " + driver.getTitle());

				}
				flg = false;
			}

		}
		catch (Exception ex)
		{

		}
		return flg;
	}
	
	
	public static void GOW1ACK(String statuscode) throws Exception
	{

		logger.info("----start Gow1ACK---");

			String const_date = null;
			if(statuscode.equalsIgnoreCase("2"))
			{
			Keywordoperations _Storevalue=new Keywordoperations();
			_Storevalue.StoreValue("VAR_FCREQUESTID", fcrequestid);
			}
	
			Connection conn = null;
			try
			{
				String gow1msg="<env:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"+
						"<env:Header>"+
						"<wsa:MessageID env:mustUnderstand=\"1\">uuid:631596</wsa:MessageID>"+
						"<wsa:To env:mustUnderstand=\"1\">http://services.cibc.com/Status/Management/</wsa:To>"+
						"<wsa:From env:mustUnderstand=\"1\">"+
							"<wsa:Address>http://emf.cibc.com/emb/gow/Adapter/</wsa:Address>"+
						"</wsa:From>"+
						"<wsa:ReplyTo env:mustUnderstand=\"1\">"+
							"<wsa:Address>http://emf.cibc.com/emb/gow/Adapter/#Link01/</wsa:Address>"+
						"</wsa:ReplyTo>\n"+
						"<wsa:FaultTo env:mustUnderstand=\"1\">\n"+
							"<wsa:Address>http://emf.cibc.com/emb/gow/Adapter/#Link01/</wsa:Address>\n"+
						"</wsa:FaultTo>\n"+
						"<wsa:Action env:mustUnderstand=\"1\">http://services.cibc.com/Status/Management/#UpdateStatus/Request/</wsa:Action>\n"+
						"<wsu:Timestamp env:mustUnderstand=\"1\">\n"+
							"<wsu:Created>2018-10-09T09:26:05.730-04:00</wsu:Created>\n"+
						"</wsu:Timestamp>\n"+
					"</env:Header>\n"+
					"<env:Body>\n"+
						"<gow:UpdateStatusRequest xmlns:gow=\"http://emf.cibc.com/emb/gow/\" xsi:schemaLocation=\"http://emf.cibc.com/emb/gow/ http://emf.cibc.com/emb/gow/UpdateStatusRequest.xsd\">\n"+
							"<gow:TransactionID>#######</gow:TransactionID>\n"+
							"<gow:OriginatingChannelTypeID>34358</gow:OriginatingChannelTypeID>\n"+
							"<gow:OriginatingOperatorID>@@@@@@</gow:OriginatingOperatorID>\n"+
							"<gow:FulfillmentServiceCode>GOW</gow:FulfillmentServiceCode>\n"+
							"<gow:SR_Channel_Reference_Number>*****************</gow:SR_Channel_Reference_Number>\n"+
							"<gow:SR_Update_Status_Code>$</gow:SR_Update_Status_Code>\n"+
							"<gow:SR_Update_Status_DateTime>2018-10-09T09:26:05.729-04:00</gow:SR_Update_Status_DateTime>\n"+
							"<gow:SR_Update_Status_ExtDescription>Submitted. Submitted-Request</gow:SR_Update_Status_ExtDescription>\n"+
							"<gow:CustomerEmailAddress/>\n"+
						"</gow:UpdateStatusRequest>\n"+
					"</env:Body>\n"+
				"</env:Envelope>";
		
				Date date = new Date();
				DateFormat df = new SimpleDateFormat("yyMMdd");
				String dateAsISOString = df.format(date);
				System.out.println(dateAsISOString);
				Random rand = new Random(); 
				  
		        // Generate random integers in range 0 to 999 
		        int rand_int1 = rand.nextInt(1000); 
		        String uniqueid=String.valueOf(rand_int1);
		        gow1msg=gow1msg.replace("@@@@@@", txngowid);
		        gow1msg=gow1msg.replace("#######",wirerefnum);
		        gow1msg=gow1msg.replace("*****************",fcrequestid);
		        gow1msg=gow1msg.replace("$",statuscode);
		        System.out.println("gow1msg:"+gow1msg);
		        logger.info("gow1msg:"+gow1msg);
					if (wirerefnum!= null)
					{
						String gow1URL = PropertyUtils.readProperty("GOWURL");
						String Final_msg = gow1msg;
						logger.info("Final_msg--->" + Final_msg);
						// Thread.sleep(500000);
						Toolkit toolkit = Toolkit.getDefaultToolkit();
						Clipboard clipboard = toolkit.getSystemClipboard();
						StringSelection strSel = new StringSelection(Final_msg);
						clipboard.setContents(strSel, null);
						Robot robot = new Robot();
						robot.mouseMove(20, 10);
						ChromeOptions options = new ChromeOptions();
						//options.setProxy(null);
					//	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
						WebDriverManager.chromedriver().setup();
						options.addArguments("headless");
						  driver = new ChromeDriver(options);
						    
							driver.get(gow1URL);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
						Nfocusonwindow("Testing Async Sanctions");
						driver.findElement(By.xpath("//textarea[@name='txtName']")).sendKeys(Final_msg);

						logger.info("msg done");
						// Thread.sleep(15000);
						driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
						Thread.sleep(5000);
						driver.quit();
			
					}
					else
					{
						logger.info("SrcRefnum is Empty");
					}
				
			}
			finally
			{}
			
			logger.info("----end gowmsg----");
		

	}

	
	public static void mt101Resp(
	        String sTestCaseID) throws Exception
	{
System.out.println("in here mt101 swift");
		String prefix="IWSL    PSH4L101              TO";
		String mfix="I1318 12CIBCCAT0ACMO00003"+"\n"+
				"1318 12SFBICA06AXXX00003"+"\n"+
				"101 02"+"\n"+
				"{1:F01SFBICA06AXXX0975000073}"+"\n"+
				"{2:O1011408181215CIBCCAT0AXXX00001048981812211408N}"+"\n"+
				"{4:"+"\n"+
				":20:";
		String sufix=":28D:1/1"+"\n"+
":30:";
		String asyncdata=":21:SFE2EWP001\r\n:32B:USD1,00\r\n:50H:/000313000022\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK, AL\r\n12345, CA\r\n:57C://CC/001000998\r\n:59:/200000181241\r\nSHAILESHKUAMR C\r\nNALASOPARA EASR\r\nTHANE, MH\r\n12345, IN\r\n:70:CIBC PAYROLL\r\nEMPLY SALARY\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:71A:BEN\r\n:21:SFE2EAP007\r\n:23E:INTC\r\n:32B:CAD7,00\r\n:50H:/000313000024\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK, AL\r\n12345, CA\r\n:57C://CC/001000031\r\n:59:/3000005\r\nSUSHIL ANANT MORE\r\nBADLAPUR WEST\r\nTHANE, MH\r\n12345, IN\r\n:70:CIBC PAYROLL\r\nEMPLY SALARY\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:71A:BEN\r\n:21:SFE2EAP008\r\n:23E:INTC\r\n:32B:CAD8,00\r\n:50H:/000313000024\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK, AL\r\n12345, CA\r\n:57C://CC/001000031\r\n:59:/3000005\r\nSUSHIL ANANT MORE\r\nBADLAPUR WEST\r\nTHANE, MH\r\n12345, IN\r\n:70:CIBC PAYROLL\r\nEMPLY SALARY\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:71A:BEN\r\n:21:SFE2EAP009\r\n:23E:INTC\r\n:32B:CAD9,00\r\n:50H:/000313000022\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK, AL\r\n12345, CA\r\n:57C://CC/001000031\r\n:59:/3000005\r\nSUSHIL ANANT MORE\r\nBADLAPUR WEST\r\nTHANE, MH\r\n12345, IN\r\n:70:CIBC PAYROLL\r\nEMPLY SALARY\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:71A:BEN\r\n:21:SFE2EAP010\r\n:23E:INTC\r\n:32B:USD10,00\r\n:50H:/000313000025\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK, AL\r\n12345, CA\r\n:57C://CC/001000031\r\n:59:/3000005\r\nSUSHIL ANANT MORE\r\nBADLAPUR WEST\r\nTHANE, MH\r\n12345, IN\r\n:70:CIBC PAYROLL\r\nEMPLY SALARY\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:71A:BEN\r\n:21:SFE2EAP011\r\n:23E:INTC\r\n:32B:CAD11,00\r\n:50H:/000313000025\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK, AL\r\n12345, CA\r\n:57C://CC/001000031\r\n:59:/3000005\r\nSUSHIL ANANT MORE\r\nBADLAPUR WEST\r\nTHANE, MH\r\n12345, IN\r\n:70:CIBC PAYROLL\r\nEMPLY SALARY\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:71A:BEN\r\n:21:SFE2EACHP012\r\n:23E:OTHR/IAT\r\n:32B:USD12,00\r\n:50H:/000313000021\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK*AL\r\nUS*12345\r\n:57C:/011000015\r\n:59:/123456789\r\nZOHAD FAZAL\r\nMG ROAD\r\nMALAD*AL\r\nUS*12345\r\n:70:SAL\r\n22\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:77B:/3000000505\r\nJAMES BOND 007\r\nJAMES BOND 007\r\n:71A:SHA\r\n:21:SFE2EEFTP013\r\n:23E:OTHR/EFT\r\n:32B:USD13,00\r\n:50H:/000313000021\r\nSwift Fin Client5\r\nMANHATTEN\r\nNEWYORK*AL\r\nUS*12345\r\n:57C:/001030800\r\n:59:/0010000859\r\nSUSHIL MORE\r\nBELAVALI\r\nBADLAPUR*AL\r\nUS*12345\r\n:70:460\r\n22\r\nBANKING SOLUTIONS\r\nPAYMENTS DOMAIN\r\n:77B:/3000000500\r\nJAMES BOND 007\r\nJAMES BOND 007\r\n:71A:SHA\r\n-}\r\n{5:{MAC:12AB90EF}{CHK:123456789ABC}}";
		String const_date = null;
		String date = null;
		String wire_ext_ref_num = null;
		String UNIQUE_CHANNEL_REF = null;
	
		Connection conn = null;
		try
		{
		   Date date1= java.util.Calendar.getInstance().getTime();  
			//LocalDateTime now = LocalDateTime.now();  
			 DateFormat df = new SimpleDateFormat("yyMMdd");
			
		    date = df.format(date1);
		    System.out.println(date);
	
			//String uniquegen= DBConnector.GetTestSeq();
			String uniquegen= Keywordoperations.getSaltString("N",5,"VAR");
			wire_ext_ref_num = "E2E" + "00" + date + uniquegen;
			System.out.println("wire_ext_ref_num--->" + wire_ext_ref_num);
			logger.info("wire_ext_ref_num--->" + wire_ext_ref_num);
			const_date = date + "0000" + uniquegen;
			Keywordoperations s=new Keywordoperations();
			
			s.StoreValue("VAR_SOURCEREFNUM", wire_ext_ref_num); 
			UNIQUE_CHANNEL_REF = "TO" + const_date + "I";
			String Final_msg = prefix + const_date + mfix + wire_ext_ref_num + "\n" + sufix + date + "\n" + asyncdata;
		System.out.println("Final_msg--->" + Final_msg);
		logger.info("Final_msg--->" + Final_msg);
			
			// Initialize the Driver
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			WebDriverManager.chromedriver().setup();
		//	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
			//options.addArguments("headless");
			  driver = new ChromeDriver(options);
			    
				driver.get(PropertyUtils.readProperty("PSHMT900JSP"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().window().maximize();
			
			Nfocusonwindow("Insert title here");
			
			WebElement textarea = driver.findElement(By.xpath("//textarea[@name='txtName']"));
			textarea.sendKeys(Final_msg);
			Thread.sleep(5000);
			
			driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
			Thread.sleep(5000);
			driver.quit();
			StringBuffer strQuery = new StringBuffer();

			Connection conn1 = null;

			PreparedStatement pstmt = null;
			ResultSet resultSet = null;
			try
			{
				strQuery.append("UPDATE TEST_TOOL_FILE_NAME ");
				strQuery.append(" set FILENAME  = ");
				strQuery.append("(SELECT PHYSICAL_FILE_NAME FROM PSH_PHYSICAL_FILE_DETAIL where UNIQUE_CHANNEL_REF='"
				        +UNIQUE_CHANNEL_REF+ "')");
				strQuery.append(" where testcaseid = '" + sTestCaseID + "'");

				// file id
				String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
				String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
				String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
				Class.forName("oracle.jdbc.driver.OracleDriver");
				conn1=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
				
				pstmt = conn1.prepareStatement(strQuery.toString());
				resultSet = pstmt.executeQuery();
				try
				{
					conn.commit();
				}
				catch (Exception e)
				{
					// e.printStackTrace();
				}
			Thread.sleep(5000);

		
		}

		catch (Exception ex)
		{
		
		
			ex.printStackTrace();
			logger.debug(ex.getMessage());
		}


		// }
	finally{}
	
	}

	finally{}
	}

	public static void ackMessagePSR(
	        String sTestCaseID) throws Exception
	{

		String prefix="PSH4OTO181128000006000I000";
		String mfix="181128153100";
		String refnum = FileUtilities.GetFilevalue("VAR_SOURCEREFNUM");
	
		Connection conn = null;
		try
		{
			String Final_msg = prefix +refnum+ mfix;
		System.out.println("Final_msg--->" + Final_msg);
		logger.info("Final_msg--->" + Final_msg);
			// Initialize the Driver
			ChromeOptions options = new ChromeOptions();
			//options.setProxy(null);
			WebDriverManager.chromedriver().setup();
		//	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
			//options.addArguments("headless");
			  driver = new ChromeDriver(options);
			    
				driver.get(PropertyUtils.readProperty("MTACKJSP"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().window().maximize();
			
			Nfocusonwindow("Insert title here");
			
			WebElement textarea = driver.findElement(By.xpath("//textarea[@name='txtName']"));
			textarea.sendKeys(Final_msg);
			Thread.sleep(5000);
			
			driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
			Thread.sleep(5000);
			driver.quit();
			
		}
		
catch(Exception e)
		{
	e.printStackTrace();
	logger.debug(e.getMessage());
		}

	}
	
	public static void main(String[] args) throws Exception {
		//dbconnect();
		//ackMt103("f");
		//MT900("FEDREF/229B1QGC01C010131");
		//GOW1ACK("2");
	    //MT199("/FCS/LVTSREF0000001111100000000000000");
	   // MT199("/SWF/IGNORE REF AS CODE CLR SYS IS SWF");
		//AsyncAction("ANSIxSC11xP016|3");
		mt101Resp("SANITY_TC024");
		//Thread.sleep(150000);
		//ackMessagePSR("SANITY_TC024");
	}
}
