package Utilities;
import org.apache.log4j.Logger;
import Logger.LoggerUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import org.apache.commons.net.ftp.FTPClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class FTPExample {
	
	private String host;
	private Integer port;
	private String user;
	private String password;
	
	private JSch jsch;
	private Session session;
	private Channel channel;
	private ChannelSftp sftpChannel;
	static Logger log = LoggerUtils.getLogger();
	public FTPExample(String host, Integer port, String user, String password) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;
	}

	public FTPExample() {
		// TODO Auto-generated constructor stub
	}

	public void connect() throws Exception {
		
		System.out.println("connecting..."+host);
		log.info("connecting..."+host);
		try {
			jsch = new JSch();
			String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
			if(envdetail.contains("CLOUD"))
			{	
			String ppk=	PropertyUtils.readProperty("ENV_PPK");
			//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
			String privateKey = System.getProperty("user.dir")+"\\ppk\\"+ppk;
			jsch.addIdentity(privateKey);
			}
			session = jsch.getSession(user, host,port);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();

			channel = session.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;

		} catch (JSchException e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}

	}
	
	public void disconnect() {
		System.out.println("disconnecting...");
		log.info("disconnecting");
		sftpChannel.disconnect();
		channel.disconnect();
		session.disconnect();
	}
	
	public void upload(String fileName, String remoteDir) throws Exception {

		FileInputStream fis = null;
		connect();
		try {
			// Change to output directory
			if (fileName.contains(".txt"))
					{
				
					}
			else
			{
				//fileName=fileName.concat(".txt");
			}
			
			sftpChannel.cd(remoteDir);
            System.out.println("fileName:"+fileName);
            log.info("fileName:"+fileName);
			// Upload file
			File file = new File(fileName);
			fis = new FileInputStream(file);
			sftpChannel.put(fis, file.getName());

			fis.close();
			System.out.println("File uploaded successfully - "+ file.getAbsolutePath());
			log.info("File uploaded successfully - "+ file.getAbsolutePath());

		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		disconnect();
	}
	
	public void download(String fileName, String localDir) throws Exception {

		byte[] buffer = new byte[1024];
		BufferedInputStream bis;
		connect();
		try {
			// Change to output directory
			String cdDir = fileName.substring(0, fileName.lastIndexOf("/") + 1);
			System.out.println(cdDir);
			boolean b=sftpChannel.isConnected();
			System.out.println("Connection status:"+b);
			sftpChannel.cd(cdDir);

			File file = new File(fileName);
			System.out.println("filename"+sftpChannel.get(file.getName().toString()));
			bis = new BufferedInputStream(sftpChannel.get(file.getName()));

			File newFile = new File(localDir + "/" + file.getName());
			
			// Download file
			OutputStream os = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(os);
			int readCount;
			while ((readCount = bis.read(buffer)) > 0) {
				bos.write(buffer, 0, readCount);
			}
			bis.close();
			bos.close();
			System.out.println("File downloaded successfully - "+ file.getAbsolutePath());
			log.info("File downloaded successfully - "+ file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		disconnect();
	}

	public static void main(String[] args) throws Exception {
		
		
		String localPath = System.getProperty("user.dir")+"/upload/IWS/";
		//String remotePath = "/usr1/SIR18076/GTB_HOME/IPSH_HOME/ipshlogs/ipshlogs";
		String remotePath = "/usr1/SIR17925/GTB_HOME/IPSH_HOME/filearea/incoming/IWS/incoming/";
		//String remotePath = "/usr1/SIR17929/GTB_HOME/IPSH_HOME/filearea/SPLITTING/EMT/response-files/incoming";
		//FTPExample ftp = new FTPExample("10.11.10.12", 22, "SIR18076", "test123");
		FTPExample ftp = new FTPExample("10.11.10.60", 22, "SIR17925", "test123");
		ftp.upload(localPath+"IWSPSH160117096401513_With_SWIFT_UETR_4733.txt", remotePath);
		
		//ftp.download(remotePath+"Inquiry.log", localPath);
		try {
			//SFTPfile_DownloadToLocal("xx","D:/CBX_Automation/Download Log", "Inquiry.log");
	      //  SFTPfile_DownloadToLocal2();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static String SFTPfile_DownloadToLocal(String ServerPath, String LocalPath, String FileName) throws Exception
	{
		 String Result="PASS";
		 String hostname = "10.11.10.12";
		 String login = "SIR18076";
		 String password = "test123";
		//String hostname = Utilities.getProp(CIBC_XPATH_Constants.Env2_HOST_NAME, CIBC_XPATH_Constants.CONFIGPATH);
		//String login = Utilities.getProp(CIBC_XPATH_Constants.Env2_LOGIN, CIBC_XPATH_Constants.CONFIGPATH);
		//String password = Utilities.getProp(CIBC_XPATH_Constants.Env2_PASSWORD, CIBC_XPATH_Constants.CONFIGPATH);

		//String directory ="/usr1/SIR18076/GTB_HOME/IPSH_HOME/ipshlogs";
		String directory = ServerPath;

		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		Session session;
		try
		{
			// GETTING sESSION FROM SERVER
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			if (channel.isConnected())
			{
				System.out.println("Connected!!");
				Channel sftp = channel;
				((ChannelSftp) sftp).cd(directory);
				try
				{
					((ChannelSftp) sftp).cd(directory);
					Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
					for (int i = 0; i < list.size(); i++)
					{
						if (!(list.get(i).getFilename().equalsIgnoreCase("..")
						        || (list.get(i).getFilename().equalsIgnoreCase("."))))
						{
							if (list.get(i).getFilename().equalsIgnoreCase(FileName))
							{
								System.out.println(list.get(i).getFilename());
								System.out.println(directory + "/" + list.get(i).getFilename());
								channel.get(directory + "/" + list.get(i).getFilename(), LocalPath);
							}
						}

					}

				}
				catch (Exception e)
				{	
					Result="FAIL";
					System.out.println(e.toString());
				}
			}
			else
			{
				Result="FAIL";
				System.out.println("Not Connected To Server!!");
			}
			channel.disconnect();
			session.disconnect();
		}
		catch (JSchException e)
		{
			// TODO Auto-generated catch block
			Result="FAIL";
			e.printStackTrace();
			
		}
		return Result;
	}
	

}