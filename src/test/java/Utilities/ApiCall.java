package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import com.google.gson.Gson;

import UIOperations.Keywordoperations;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ApiCall {

	public String Status;
	public static Response response;
	
	public static String POSTToAPI(String Body_Json) throws Exception{
		String status=null;
		try{
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setBody(Body_Json);
			builder.setContentType("application/json; charset=UTF-8");
			RequestSpecification request = builder.build();
			response = RestAssured.given().spec(request).when().post("http://localhost:8080/steve/process");
			Thread.sleep(5000);
			System.out.println(response.getStatusCode());
			String replaceString = response.body().asString().substring(1);
			JSONObject obj = new JSONObject(replaceString);
			status = obj.getString("status");
			System.out.println("Status is: " + status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public String getresponse(String requestcode, String EndtoendId) throws Exception{
		String Result_Status=null;
		try {
			String input1 = "{\"requestCode\" : \"VAR1\",\"inputData\":{\"endToEndIdentification\": \"VAR2\"}}";
			//String RequestCode = "PSH-C sent the Request to CBX";
			//String endtoendid = "67079420663-63469332";
			
			FileUtilities FileUtilities=new FileUtilities();
			String VAR_MESSAGESEQID_SUB = FileUtilities.GetFilevalue("VAR_MESSAGESEQID_SUB");	
			System.out.println(VAR_MESSAGESEQID_SUB);
			
			String input2 = input1.replace("VAR1", requestcode);
			String input = input2.replace("VAR2", VAR_MESSAGESEQID_SUB);
			System.out.println("Final:" + input);
			Result_Status = POSTToAPI(input);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Result_Status;
	}

	public String parseJson2(String jsonString2) throws JSONException {
		JSONObject obj = new JSONObject(jsonString2);
		String status = obj.getString("status");

		System.out.println("Status is: " + status);
		return status;
		/*
		 * JSONObject obj2=obj.getJSONObject("batters");
		 * 
		 * //Here we get json array inside obj2 with key- batter JSONArray
		 * array=obj2.getJSONArray("batter");
		 * 
		 * //Getting json objects inside array for(int
		 * i=0;i<array.length();i++){ JSONObject obj3=array.getJSONObject(i);
		 * //Getting id and type of json objects inside array
		 * System.out.println("Id of obj3 : "+obj3.getString("id")
		 * +" Type of obj3 : "+obj3.getString("type")); }
		 * 
		 * //Here we get json array inside obj1 with key- topping JSONArray
		 * array2=obj.getJSONArray("topping");
		 * 
		 * //Getting json objects inside array for(int
		 * i=0;i<array2.length();i++){ JSONObject obj4=array2.getJSONObject(i);
		 * //Getting id and type of json objects inside array2
		 * System.out.println("Id of obj4 at index "+i+" is : "+obj4.getInt("id"
		 * )+" Type of obj4 at index "+i+" is : "+obj4.getString("type")); }
		 */
	}
	
	public static void main(String[] args) throws Exception{
/*		String input1 = "{\"requestCode\" : \"VAR1\",\"inputData\":{\"endToEndIdentification\": \"VAR2\"}}";
		String RequestCode = "PSH-C sent the Request to CBX";
		String endtoendid = "75379167240-78013429";
		String input2 = input1.replace("VAR1", RequestCode);
		String input = input2.replace("VAR2", endtoendid);
		System.out.println("Final:" + input);
		POSTToAPI(input);*/
		//String TestData="VAR_MESSAGESEQID_SUB|VAR_MessageID";
		//Keywordoperations.GetValueFromElasticDB(TestData);
		//DBConnector DBConnect=new DBConnector();
		//DBConnect.DBConnector("PSH|Select WORKITEMID,MEssage_id from file_proc_opn where MEssage_id|WORKITEMID|VAR_MessageID|VAR_DBWORKITEMID");
		
		String FullFill_RTPAPI = PropertyUtils.readProperty("FullFillRTP_REQ");
		System.out.println(FullFill_RTPAPI);
		
	}

}