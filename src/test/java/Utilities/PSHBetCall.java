/***Class written for PSH-C BET API call
 * written by Aishwarya Nath
 * 
 */

package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;
import Logger.LoggerUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import com.google.gson.Gson;

import UIOperations.Keywordoperations;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PSHBetCall {

	public String Status;
	public static Response response;
	public static String VDate="VAR_VDATE";
	static Logger log = LoggerUtils.getLogger();
	public static String POSTToAPI(String Body_Json) throws Exception{
		String status=null;
		try{
			String BETURL = PropertyUtils.readProperty("BETURL");
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setBody(Body_Json);
			builder.setContentType("application/json; charset=UTF-8");
			builder.addHeader("channelId", "CBX");
			RequestSpecification request = builder.build();
			response = RestAssured.given().spec(request).when().post(BETURL);
			Thread.sleep(5000);
			System.out.println(response.getStatusCode());
			log.info(response.getStatusCode());
			status = response.body().asString();
			System.out.println("Response Body"+status);
			log.info("Response Body"+status);
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return status;
	}
	
	public String getresponse(String Type,String ValueDate,String Dbtraccno,String TransitNo, String PanNo,String SettType) throws Exception{
		String Result_Status=null;
		try {

	        String api=Type;
			String input1=null;
	        System.out.println(api);
	        if(api.equals("BET"))
	        {
	        	System.out.println("IN BET");
	        	log.info("IN BET");
	        	//input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:48:38\",\"Authstn\":{\"Prtry\":\"Y\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"CAD\",\"Nm\":\"ABICHSG\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"RmtInf\":{\"Ustrd\":[\"Shubham1\",\"Shubham2\",\"Shubham3\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"DbAmt\":\"100\",\"Ccy\":\"CAD\",\"securityQuestion\":\"SecurityQuestion\",\"securityAnswer\":\"SecurityAnswer\",\"hashType\":\"SHA256\",\"hashSalt\":\"1W\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"Debt123\",\"PymtExpDt\":\"2019-12-11T13:59:10\",\"ChnlPymtId\":\"#VAR9\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"10.198.12.11\",\"CustAuthMtd\":\"PVQ\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
	        	//input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:48:38\",\"Authstn\":{\"Prtry\":\"Y\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"CAD\",\"Nm\":\"ABICHSG\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"RmtInf\":{\"Ustrd\":[\"Shubham1\",\"Shubham2\",\"Shubham3\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"DbAmt\":\"100\",\"Ccy\":\"CAD\",\"securityQuestion\":\"SecurityQuestion\",\"securityAnswer\":\"SecurityAnswer\",\"hashType\":\"SHA256\",\"hashSalt\":\"1W\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"Debt123\",\"PymtExpDt\":\"2020-04-25T13:59:10.536Z\",\"ChnlPymtId\":\"#VAR9\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"10.198.12.11\",\"CustAuthMtd\":\"PVQ\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
	        	//input1="{\r\n   \"Document\":{\r\n      \"CstmrCdtTrfInitn\":{\r\n         \"GrpHdr\":{\r\n            \"MsgId\":\"#VAR1\",\r\n            \"CreDtTm\":\"#VAR2T00:10:24\",\r\n            \"NbOfTxs\":\"1\",\r\n            \"CtrlSum\":\"91.91\",\r\n            \"InitgPty\":{\r\n               \"Nm\":\"CBX Client 02\",\r\n               \"Id\":{\r\n                  \"OrgId\":{\r\n                     \"Othr\":{\r\n                        \"Id\":\"99000021\"\r\n                     }\r\n                  }\r\n               }\r\n            }\r\n         },\r\n         \"PmtInf\":[\r\n            {\r\n               \"PmtInfId\":\"CBXBATCH001\",\r\n               \"PmtMtd\":\"TRF\",\r\n               \"NbOfTxs\":\"1\",\r\n               \"CtrlSum\":\"91.91\",\r\n               \"PmtTpInf\":{\r\n                  \"SvcLvl\":{\r\n                     \"Prtry\":\"ACCOUNT_ALIAS_PAYMENT\"\r\n                  },\r\n                  \"LclInstrm\":{\r\n                     \"Prtry\":\"BTR\"\r\n                  }\r\n               },\r\n               \"ReqdExctnDt\":\"#VAR3\",\r\n               \"Dbtr\":{\r\n                  \"Nm\":\"CBX Client 02\",\r\n                  \"PstlAdr\":{\r\n                     \"Ctry\":\"CA\"\r\n                  },\r\n                  \"Id\":{\r\n                     \"OrgId\":{\r\n                        \"Othr\":{\r\n                           \"Id\":\"#VAR4\"\r\n                        }\r\n                     }\r\n                  }\r\n               },\r\n               \"DbtrAcct\":{\r\n                  \"Ccy\":\"CAD\",\r\n                  \"Nm\":\"CUST1K001\",\r\n                  \"Id\":{\r\n                     \"Othr\":{\r\n                        \"Id\":\"7272727\"\r\n                     }\r\n                  }\r\n               },\r\n               \"DbtrAgt\":{\r\n                  \"FinInstnId\":{\r\n                     \"Othr\":{\r\n                        \"Id\":\"0010\"\r\n                     }\r\n                  },\r\n                  \"BrnchId\":{\r\n                     \"Id\":\"#VAR5\"\r\n                  }\r\n               },\r\n               \"CdtTrfTxInf\":[\r\n                  {\r\n                     \"PmtId\":{\r\n                        \"EndToEndId\":\"#VAR6\"\r\n                     },\r\n                     \"Amt\":{\r\n                        \"InstdAmt\":{\r\n                           \"Ccy\":\"CAD\",\r\n                           \"Amt\":\"91.91\"\r\n                        }\r\n                     },\r\n                     \"Cdtr\":{\r\n                        \"Nm\":\"CONTACT LEGAL NM\",\r\n                        \"PstlAdr\":{\r\n                           \"Ctry\":\"CA\"\r\n                        },\r\n                        \"CtctDtls\":{\r\n                           \"EmailAdr\":\"test@email.com\",\r\n                           \"Othr\":\"en\"\r\n                        }\r\n                     },\r\n                     \"CdtrAcct\":{\r\n                        \"Id\":{\r\n                           \"Othr\":{\r\n                              \"Id\":\"20090919874710032520127\"\r\n                           }\r\n                        }\r\n                     },\r\n                     \"RmtInf\":{\r\n                        \"Ustrd\":[\r\n                           \"sdfsdf\"\r\n                        ]\r\n                     },\r\n                     \"SplmtryData\":{\r\n                        \"Envlp\":{\r\n                           \"Cnts\":{\r\n                              \"StlmMth\":\"#VAR8\",\r\n                              \"ChnlPymtId\":\"#VAR9\",\r\n                              \"DbAmt\":\"91.91\",\r\n                              \"Ccy\":\"CAD\",\r\n                              \"ProductCd\":\"DOMESTIC\",\r\n                              \"DbtrPuId\":\"99000021\",\r\n                              \"PymtExpDt\":\"2020-10-09T12:10:24.957Z\",\r\n                              \"AliasHandleType\":\"EMAIL\",\r\n                              \"AliasHandleId\":\"test@email.com\",\r\n                              \"AcctCretDt\":\"2020-01-29\"\r\n                           }\r\n                        }\r\n                     }\r\n                  }\r\n               ]\r\n            }\r\n         ],\r\n         \"SplmtryData\":{\r\n            \"Envlp\":{\r\n               \"Cnts\":{\r\n                  \"PANID\":\"#VAR7\",\r\n                  \"ChnlIndctr\":\"ONLINE\",\r\n                  \"CustIpAddr\":\"000.000.000.000\",\r\n                  \"CustAuthMtd\":\"PASSWORD\",\r\n                  \"AuthTp\":\"NOT_REQUIRED\"\r\n               }\r\n            }\r\n         }\r\n      }\r\n   }\r\n}";
	        	input1="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T00:10:24\",\"CtrlSum\":\"91.9100\",\"NbOfTxs\":\"1\",\"InitgPty\":{\"Nm\":\"CBX Client 02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000021\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"91.91\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"ACCOUNT_ALIAS_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX Client 02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000021\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"CUST1K001\",\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"91.91\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"EmailAdr\":\"test@email.com\",\"Othr\":\"EN\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"20090919874710032520127\"}}},\"RmtInf\":{\"Ustrd\":[\"sdfsdf\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"ChnlPymtId\":\"#VAR9\",\"DbAmt\":\"91.91\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000021\",\"PymtExpDt\":\"2020-10-09T12:10:24.957Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"test@email.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
	        }
	        if(api.equals("RTP"))
	        {
	        	System.out.println("IN RTP");
	        	log.info("IN RTP");
	       //	input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T13:59:10\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"FULFILL_REQUEST_FOR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"1480000002\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"CAD\",\"Nm\":\"ABICHSG\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\",\"InstrId\":\"INTERAC123RREFNUM\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"CdtrAgt\":{\"FinInstnId\":{\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"CACPA\"}}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"Ctry\":\"CA\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":[\"Shubham1\",\"Shubham2\",\"Shubham3\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"DbAmt\":\"100\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"Debt123\",\"PymtExpDt\":\"2019-12-11T13:59:10\",\"ChnlPymtId\":\"#VAR9\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"10.198.12.11\",\"CustAuthMtd\":\"PVQ\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
	       	//input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T13:59:10\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"FULFILL_REQUEST_FOR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"1480000002\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"CAD\",\"Nm\":\"ABICHSG\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\",\"InstrId\":\"INTERAC123RREFNUM\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"CdtrAgt\":{\"FinInstnId\":{\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"CACPA\"}}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"Ctry\":\"CA\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":[\"Shubham1\",\"Shubham2\",\"Shubham3\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"DbAmt\":\"100\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"Debt123\",\"PymtExpDt\":\"2020-04-25T13:59:10.536Z\",\"ChnlPymtId\":\"#VAR9\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"10.198.12.11\",\"CustAuthMtd\":\"PVQ\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";

	       	input1="{\r\n  \"Document\": {\r\n    \"CstmrCdtTrfInitn\": {\r\n      \"GrpHdr\": {\r\n        \"MsgId\": \"#VAR1\",\r\n        \"CreDtTm\": \"#VAR2T18:12:05\",\r\n        \"Authstn\": {\r\n          \"Prtry\": \"Y\"\r\n        },\r\n        \"NbOfTxs\": \"1\",\r\n        \"CtrlSum\": \"19.0\",\r\n        \"InitgPty\": {\r\n          \"Nm\": \"CBX TEST FF\",\r\n          \"Id\": {\r\n            \"OrgId\": {\r\n              \"Othr\": {\r\n                \"Id\": \"99000003\"\r\n              }\r\n            }\r\n          }\r\n        }\r\n      },\r\n      \"PmtInf\": [\r\n        {\r\n          \"PmtInfId\": \"CBXBATCH001\",\r\n          \"PmtMtd\": \"TRF\",\r\n          \"NbOfTxs\": \"1\",\r\n          \"CtrlSum\": \"19.0\",\r\n          \"PmtTpInf\": {\r\n            \"SvcLvl\": {\r\n              \"Prtry\": \"FULFILL_REQUEST_FOR_PAYMENT\"\r\n            },\r\n            \"LclInstrm\": {\r\n              \"Prtry\": \"BTR\"\r\n            }\r\n          },\r\n          \"ReqdExctnDt\": \"#VAR3\",\r\n          \"Dbtr\": {\r\n            \"Nm\": \"FF ACC 2\",\r\n            \"PstlAdr\": {\r\n              \"Ctry\": \"CA\"\r\n            },\r\n            \"Id\": {\r\n              \"OrgId\": {\r\n                \"Othr\": {\r\n                  \"Id\": \"99000003\"\r\n                }\r\n              }\r\n            }\r\n          },\r\n          \"DbtrAcct\": {\r\n            \"Ccy\": \"CAD\",\r\n            \"Nm\": \"FF ACC 2\",\r\n            \"Id\": {\r\n              \"Othr\": {\r\n                \"Id\": \"#VAR4\"\r\n              }\r\n            }\r\n          },\r\n          \"DbtrAgt\": {\r\n            \"FinInstnId\": {\r\n              \"Othr\": {\r\n                \"Id\": \"0010\"\r\n              }\r\n            },\r\n            \"BrnchId\": {\r\n              \"Id\": \"#VAR5\"\r\n            }\r\n          },\r\n          \"CdtTrfTxInf\": [\r\n            {\r\n              \"PmtId\": {\r\n                \"EndToEndId\": \"#VAR6\",\r\n                \"InstrId\": \"INT2930482030184839298361556\"\r\n              },\r\n              \"Amt\": {\r\n                \"InstdAmt\": {\r\n                  \"Ccy\": \"CAD\",\r\n                  \"Amt\": \"19.0\"\r\n                }\r\n              },\r\n              \"CdtrAgt\": {\r\n                \"FinInstnId\": {\r\n                  \"ClrSysMmbId\": {\r\n                    \"MmbId\": \"00100002\",\r\n                    \"ClrSysId\": {\r\n                      \"Cd\": \"CACPA\"\r\n                    }\r\n                  }\r\n                }\r\n              },\r\n              \"Cdtr\": {\r\n                \"Nm\": \"Aka_lostGirl\",\r\n                \"PstlAdr\": {\r\n                  \"Ctry\": \"IN\"\r\n                }\r\n              },\r\n              \"CdtrAcct\": {\r\n                \"Id\": {\r\n                  \"Othr\": {\r\n                    \"Id\": \"123456789\"\r\n                  }\r\n                }\r\n              },\r\n              \"SplmtryData\": {\r\n                \"Envlp\": {\r\n                  \"Cnts\": {\r\n                    \"StlmMth\": \"#VAR8\",\r\n                    \"ChnlPymtId\": \"#VAR9\",\r\n                    \"DbAmt\": \"19.0\",\r\n                    \"Ccy\": \"CAD\",\r\n                    \"ProductCd\": \"DOMESTIC\",\r\n                    \"DbtrPuId\": \"99000003\",\r\n                    \"PymtExpDt\": \"2020-06-07T06:12:05.210Z\",\r\n                    \"AcctCretDt\": \"2020-07-05\"\r\n                  }\r\n                }\r\n              }\r\n            }\r\n          ]\r\n        }\r\n      ],\r\n      \"SplmtryData\": {\r\n        \"Envlp\": {\r\n          \"Cnts\": {\r\n            \"PANID\": \"#VAR7\",\r\n            \"ChnlIndctr\": \"ONLINE\",\r\n            \"CustIpAddr\": \"000.000.000.000\",\r\n            \"CustAuthMtd\": \"PASSWORD\",\r\n            \"AuthTp\": \"NOT_REQUIRED\"\r\n          }\r\n        }\r\n      }\r\n    }\r\n  }\r\n}";
	        }
			
			Keywordoperations key=new Keywordoperations();
			String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
			System.out.println(VAR_messageid);
			log.info(VAR_messageid);
			String VAR_E2Eid = key.getSaltString("Y", 10,"BETE2E");
			System.out.println(VAR_E2Eid);
			log.info(VAR_E2Eid);
			key.StoreValue("VAR_PSHBETWORKITEMID",VAR_messageid);
			key.StoreValue("VAR_CUSTREFNUM",VAR_E2Eid);
			String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
			System.out.println(VAR_chnlid);
			log.info(VAR_chnlid);
			String CurrentDate=key._DateFormat();
			System.out.println(CurrentDate);
			String ExecutionDate=ValueDate;
			String ExecDate=null;
			if(ExecutionDate.equals("CBD"))
			{
				ExecDate=CurrentDate;
			}
			else
			{
				   String NoOfDays = ExecutionDate.substring(4);
				   int Days=Integer.parseInt(NoOfDays); 
				   ExecDate=AddDate(CurrentDate,Days);
			}
			
			String input2 = input1.replace("#VAR1",VAR_messageid);
			input2= input2.replace("#VAR2",CurrentDate);
			input2= input2.replace("#VAR3",ExecDate);
			input2= input2.replace("#VAR4",Dbtraccno);
			input2= input2.replace("#VAR5",TransitNo);
			input2= input2.replace("#VAR6",VAR_E2Eid);
			input2= input2.replace("#VAR7",PanNo);
			input2= input2.replace("#VAR8",SettType);
			input2= input2.replace("#VAR9", VAR_chnlid);
			System.out.println("Final:" + input2);
log.info("Final:" + input2);
			String Finalstatus=null;
		
			Finalstatus=POSTToAPI(input2);
			if(Finalstatus.contains("\"TxSts\":\"TXVA\""))
			{
				Result_Status="PASS";
			}
			else if(Finalstatus.contains("\"TxSts\":\"ACSP\""))
			{
				Result_Status="PASS";
			}


			
		}catch (Exception e) {
			e.printStackTrace();
			Result_Status="FAIL";
			log.debug(e.getMessage());
		}
		return Result_Status;
	}

	public String parseJson2(String jsonString2) throws JSONException {
		JSONObject obj = new JSONObject(jsonString2);
		String status = obj.getString("status");

		System.out.println("Status is: " + status);
		log.info("Status is: " + status);
		return status;
		
	}
	
	public static void main(String[] args) throws Exception{
		String Result_Status=null;

	//	String input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:48:38\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"900451\",\"Authstn\":{\"Prtry\":\"Y\"},\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"900451\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"StrtNm\":\"opp to Jai Coach\",\"BldgNb\":\"12\",\"PstCd\":\"4000065\",\"TwnNm\":\"Opp Lan No 2\",\"CtrySubDvsn\":\"200008\",\"Ctry\":\"CA\",\"AdrLine\":[\"181-University\",\"South Intersection\",\"ON M4A 3W7, CA\"]},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"1480000002\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"INR\",\"Nm\":\"Abi\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"CIBC\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"InstrId\":\"ARRRRR123\",\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"900451\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"213887431\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"BUKBGB22\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"NWBKGB21\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"USABA\"},\"MmbId\":\"011000015\"},\"Nm\":\"FEDERAL RESERVE BANK\",\"PstlAdr\":{\"StrtNm\":\"Opp to Jai Coach\",\"BldgNb\":\"43\",\"PstCd\":\"400065\",\"TwnNm\":\"Western Exp \",\"CtrySubDvsn\":\"1212\",\"Ctry\":\"US\",\"AdrLine\":[\"CgtrAgt Addr Line01\",\"CgtrAgt Addr Line02\",\"CgtrAgt Addr Line03\"]}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"StrtNm\":\"Creditor Address Line03 CA\",\"Ctry\":\"US\",\"AdrLine\":[\"Creditor Address Line01\",\"Creditor Address Line02\",\"Creditor Address Line03 CA\"]},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01\"},\"SplmtryData\":[{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"PymtAmt\":\"6963.51\",\"AutoDep\":\"Yes\",\"RTPFlflmntInd\":\"No\",\"securityQuestion\":\"abcd testing\",\"securityAnswer\":\"allowed\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\"}}}]}]}],\"SplmtryData\":[{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"L\",\"channelSeqId\":\"6963.51\"}}}]}}}";
	//String input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:48:38\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"#VAR4\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"2211065\"}},\"Ccy\":\"CAD\",\"Nm\":\"ABICHSG\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"CdtrAgt\":{\"FinInstnId\":{\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"CACPA\"}}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":[\"Shubham1\",\"Shubham2\",\"Shubham3\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"DbAmt\":\"100\",\"Ccy\":\"CAD\",\"securityQuestion\":\"SecurityQuestion\",\"securityAnswer\":\"SecurityAnswer\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ProductCd\":\"Prod123\",\"DbtrPuId\":\"Debt123\",\"PymtExpDt\":\"2019-12-11T13:59:10\",\"ChnlPymtId\":\"#VAR9\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"10.198.12.11\",\"CustAuthMtd\":\"PVQ\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
	//	String input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:59:10\",\"Authstn\":{\"Prtry\":\"Y\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"StrtNm\":\"Times Square\",\"BldgNb\":\"22\",\"PstCd\":\"R1R1R1\",\"TwnNm\":\"Toronto\",\"CtrySubDvsn\":\"ON\",\"Ctry\":\"CA\",\"AdrLine\":[\"dsdsad\",\"sadasdsad\",\"sad\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"1480000002\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR5\"}},\"Ccy\":\"INR\",\"Nm\":\"Abi\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"CIBC\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR6\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"InstrId\":\"ACHREMIT\",\"EndToEndId\":\"#VAR4\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"213887431\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"BUKBGB22\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"NWBKGB21\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"USABA\"},\"MmbId\":\"011000015\"},\"Nm\":\"FEDERAL RESERVE BANK\",\"PstlAdr\":{\"StrtNm\":\"ELM ST\",\"BldgNb\":\"420\",\"PstCd\":\"1R1R1R\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"sasadaead\",\"dsfgs\",\"dsfsdf\"]}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"StrtNm\":\"PISCATWAY\",\"BldgNb\":\"201\",\"PstCd\":\"12345\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"dsd\",\"sadlkl\",\"kllklk\"]},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01\"},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"PymtAmt\":\"6963.51\",\"AutoDep\":\"Yes\",\"RTPFlflmntInd\":\"Yes\",\"originatorFIRTPRefNum\":\"1231\",\"interacRTPRefNum\":\"12321\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"09612 CMO\",\"channelSeqId\":\"23451 09877\"}}}}}}";
		//String input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:48:38\",\"Authstn\":{\"Prtry\":\"Y\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"CAD\",\"Nm\":\"ABICHSG\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"RmtInf\":{\"Ustrd\":[\"Shubham1\",\"Shubham2\",\"Shubham3\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"DbAmt\":\"100\",\"Ccy\":\"CAD\",\"securityQuestion\":\"SecurityQuestion\",\"securityAnswer\":\"SecurityAnswer\",\"hashType\":\"SHA256\",\"hashSalt\":\"1W\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"Debt123\",\"PymtExpDt\":\"2019-12-11T13:59:10\",\"ChnlPymtId\":\"#VAR9\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"10.198.12.11\",\"CustAuthMtd\":\"PVQ\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
		String input1="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T00:10:24\",\"CtrlSum\":\"91.9100\",\"NbOfTxs\":\"1\",\"InitgPty\":{\"Nm\":\"CBX Client 02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000021\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"91.91\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"ACCOUNT_ALIAS_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX Client 02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"#VAR4\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"CUST1K001\",\"Id\":{\"Othr\":{\"Id\":\"#VAR5\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"00021\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"91.91\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"EmailAdr\":\"test@email.com\",\"Othr\":\"EN\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"20090919874710032520127\"}}},\"RmtInf\":{\"Ustrd\":[\"sdfsdf\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VAR8\",\"ChnlPymtId\":\"#VAR9\",\"DbAmt\":\"91.91\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000021\",\"PymtExpDt\":\"2020-10-09T12:10:24.957Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"test@email.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
		Keywordoperations key=new Keywordoperations();
		String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
		System.out.println(VAR_messageid);
		String VAR_E2Eid = key.getSaltString("Y", 10,"BETE2E");
		System.out.println(VAR_E2Eid);
		key.StoreValue("VAR_PSHBETWORKITEMID",VAR_messageid);
		String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
		System.out.println(VAR_chnlid);
		String CurrentDate=key._DateFormat();
		System.out.println(CurrentDate);
		String ExecutionDate="CBD+3";
		String ExecDate=null;
		if(ExecutionDate.equals("CBD"))
		{
			ExecDate=CurrentDate;
		}
		else
		{
			   String NoOfDays = ExecutionDate.substring(4);
			   int Days=Integer.parseInt(NoOfDays); 
			   ExecDate=AddDate(CurrentDate,Days);
		}
		
		String input2 = input1.replace("#VAR1",VAR_messageid);
		input2= input2.replace("#VAR2",CurrentDate);
		input2= input2.replace("#VAR3",ExecDate);
		input2= input2.replace("#VAR4","9000021");
		input2= input2.replace("#VAR5","00091");
		input2= input2.replace("#VAR6",VAR_E2Eid);
		input2= input2.replace("#VAR7","9000000000000000");
		input2=input2.replace("#VAR8", "L");
		input2= input2.replace("#VAR9", VAR_chnlid);
		System.out.println("Final:" + input2);
		//Result_Status = POSTToAPI(input2);
		String Finalstatus=POSTToAPI(input2);
		if(Finalstatus.contains("\"TxSts\":\"TXVA\""))
		{
			Result_Status="PASS";
			System.out.println("PASS");
		}

		}
	public static String AddDate(String CurrentDate,Integer NoOfDays){
		//Given Date in String format
		String CurrDate = CurrentDate;  
		System.out.println("Date before Addition: "+CurrDate);
		//Specifying date format that matches the given date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try{
		   //Setting the date to the given date
		   c.setTime(sdf.parse(CurrDate));
		}catch(ParseException e){
			e.printStackTrace();
		 }
		   
		//Number of Days to add
		c.add(Calendar.DAY_OF_MONTH,NoOfDays);  
		//Date after adding the days to the given date
		String newDate = sdf.format(c.getTime());  
		//Displaying the new Date after addition of Days
		System.out.println("Date after Addition: "+newDate);
		log.info("Date after Addition: "+newDate);
		Keywordoperations _Storevalue=new Keywordoperations();
		_Storevalue.StoreValue(VDate, newDate);
		return newDate;
	   }

	public static String POSTToAPI2(String Body_Json, String TestData) throws Exception{
		String status=null;
		String[] Data = TestData.split("\\|");
		String channelID=  Data[0];
		String Flow_ID = Data[1];
				try{
			String BETURL = PropertyUtils.readProperty("BETURL");
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setBody(Body_Json);
			builder.setContentType("application/json; charset=UTF-8");
			builder.addHeader("channelId", channelID);
			builder.addHeader("Flow_ID",Flow_ID);
			RequestSpecification request = builder.build();
			response = RestAssured.given().spec(request).when().post(BETURL);
			Thread.sleep(5000);
			System.out.println(response.getStatusCode());
			status = response.body().asString();
			System.out.println("Response Body"+status);
			log.info("Response Body"+status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	}

