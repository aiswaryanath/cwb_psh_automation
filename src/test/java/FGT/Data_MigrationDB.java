package FGT;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import com.codoid.products.fillo.*;

import com.ibatis.common.jdbc.ScriptRunner;

import Utilities.PropertyUtils;
import Constants.UrlConstants;
import org.apache.log4j.Logger;
import Logger.LoggerUtils;
public class Data_MigrationDB {
static ArrayList<String> Source_sql =new ArrayList<String>();

static Logger log = LoggerUtils.getLogger();
	public static void Delete_ExistingDataEnv1(String Suite_Name) throws Exception {
//		String classname = "oracle.jdbc.driver.OracleDriver";
//		String URL = "jdbc:oracle:thin:@10.10.9.26:1521:SIR13199";
//		String user = "CIBC_IR_162_BASE";
//		String pwd = "CIBC_IR_162_BASE";

		String classname ="oracle.jdbc.driver.OracleDriver";
		String URL = PropertyUtils.readProperty("PSH_FGTDBURL");
		String user = PropertyUtils.readProperty("PSH_FGTDBUserName");
		String pwd = PropertyUtils.readProperty("PSH_FGTDBUserPass");
	try{
		Class.forName(classname);
		Connection connection=DriverManager.getConnection(URL,user,pwd);
		Conditional_DeleteOLDRECORDS(connection,Suite_Name);
	}catch (Exception e) {
		e.printStackTrace();
	}
}

public static void Source_DestinationDBMigration(String Suite_Name) throws Exception {
 ArrayList<String> Source_sql1 =new ArrayList<String>();
	String classname = PropertyUtils.readProperty("PSH_CLASS");
	String URL = PropertyUtils.readProperty("PSH_FGTDBURL");
	String user = PropertyUtils.readProperty("PSH_FGTDBUserName");
	String pwd = PropertyUtils.readProperty("PSH_FGTDBUserPass");
try{
	Class.forName(classname);
	Connection connection=DriverManager.getConnection(URL,user,pwd);
	ResultSet Source_Records=getResultSet(connection, "TEST_TOOL_FILE_NAME",Suite_Name);
	File dir4 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\PRE QUERIES\\test_tool_file_name.sql");
	

	PrintWriter p=new PrintWriter(dir4);
	try {
       long rows=0;
	       do {
	    	  if(Source_Records.next()) {
	    		  String Source_TESTCASEID = Source_Records.getString("TESTCASEID");
	    		  String Source_FORMATNAME = Source_Records.getString("FORMATNAME");
	    		  String Source_CHANNEL = Source_Records.getString("CHANNEL");
	    		  String Source_LSI = Source_Records.getString("LSI");
	    		  String Source_CLIENT_FILE_REF = Source_Records.getString("CLIENT_FILE_REF");
	    		  String Source_BIC_CODE = Source_Records.getString("BIC_CODE");
	    		  String Source_BBDID = Source_Records.getString("BBDID");
	    		  String Source_CUST_REF = Source_Records.getString("CUST_REF");
	    		  String Source_UNIQUE_ID = Source_Records.getString("UNIQUE_ID");
	    		  String Source_FILENAME = Source_Records.getString("FILENAME");
	    		  String Source_TIMESTAMP = Source_Records.getString("TIMESTAMP");
	    		  String Source_APPLIED_RULES = Source_Records.getString("APPLIED_RULES");
	    		  String Source_ERROR_CODES = Source_Records.getString("ERROR_CODES");
	    		  String Source_MODIFIED_FIELD = Source_Records.getString("MODIFIED_FIELD");
//	    		  String Source_FOLDER_NAME = Source_Records.getString("FOLDER_NAME");
	    		  String Source_FOLDER_NAME = "Sanity";
	    		  String Source_SUITE_NAME = Source_Records.getString("SUITE_NAME");

	    		System.out.println("Insert into test_tool_file_name (TESTCASEID,FORMATNAME,CHANNEL,LSI,CLIENT_FILE_REF,BIC_CODE,BBDID,CUST_REF,UNIQUE_ID,FILENAME,TIMESTAMP,APPLIED_RULES,ERROR_CODES,MODIFIED_FIELD,FOLDER_NAME,SUITE_NAME) values ("
				+ "'"+Source_TESTCASEID+"',"
				+ "'"+Source_FORMATNAME+"',"
				+ "'"+Source_CHANNEL+"',"
				+ "'"+Source_LSI+"',"
				+ "'"+Source_CLIENT_FILE_REF+"',"
				+ "'"+Source_BIC_CODE+"',"
				+ "'"+Source_BBDID+"',"
				+ "'"+Source_CUST_REF+"',"
				+ "'"+Source_UNIQUE_ID+"',"
				+ "'"+Source_FILENAME+"',"
				+ "null,"
				+ "'"+Source_APPLIED_RULES+"',"
				+ "'"+Source_ERROR_CODES+"',"
				+ "'"+Source_MODIFIED_FIELD+"',"
				+ "'"+Source_FOLDER_NAME+"',"
				+ "'"+Source_SUITE_NAME+"')");
	    		 log.info("Insert into test_tool_file_name (TESTCASEID,FORMATNAME,CHANNEL,LSI,CLIENT_FILE_REF,BIC_CODE,BBDID,CUST_REF,UNIQUE_ID,FILENAME,TIMESTAMP,APPLIED_RULES,ERROR_CODES,MODIFIED_FIELD,FOLDER_NAME,SUITE_NAME) values ("
	    					+ "'"+Source_TESTCASEID+"',"
	    					+ "'"+Source_FORMATNAME+"',"
	    					+ "'"+Source_CHANNEL+"',"
	    					+ "'"+Source_LSI+"',"
	    					+ "'"+Source_CLIENT_FILE_REF+"',"
	    					+ "'"+Source_BIC_CODE+"',"
	    					+ "'"+Source_BBDID+"',"
	    					+ "'"+Source_CUST_REF+"',"
	    					+ "'"+Source_UNIQUE_ID+"',"
	    					+ "'"+Source_FILENAME+"',"
	    					+ "null,"
	    					+ "'"+Source_APPLIED_RULES+"',"
	    					+ "'"+Source_ERROR_CODES+"',"
	    					+ "'"+Source_MODIFIED_FIELD+"',"
	    					+ "'"+Source_FOLDER_NAME+"',"
	    					+ "'"+Source_SUITE_NAME+"')");
	    		  String _SQL_source = "Insert into test_tool_file_name (TESTCASEID,FORMATNAME,CHANNEL,LSI,CLIENT_FILE_REF,BIC_CODE,BBDID,CUST_REF,UNIQUE_ID,FILENAME,TIMESTAMP,APPLIED_RULES,ERROR_CODES,MODIFIED_FIELD,FOLDER_NAME,SUITE_NAME) values ("
	    					+ "'"+Source_TESTCASEID+"',"
	    					+ "'"+Source_FORMATNAME+"',"
	    					+ "'"+Source_CHANNEL+"',"
	    					+ "'"+Source_LSI+"',"
	    					+ "'"+Source_CLIENT_FILE_REF+"',"
	    					+ "'"+Source_BIC_CODE+"',"
	    					+ "'"+Source_BBDID+"',"
	    					+ "'"+Source_CUST_REF+"',"
	    					+ "'"+Source_UNIQUE_ID+"',"
	    					+ "'"+Source_FILENAME+"',"
	    					+ "null,"
	    					+ "'"+Source_APPLIED_RULES+"',"
	    					+ "'"+Source_ERROR_CODES+"',"
	    					+ "'"+Source_MODIFIED_FIELD+"',"
	    					+ "'"+Source_FOLDER_NAME+"',"
	    					+ "'"+Source_SUITE_NAME+"')";
	    		  //System.out.println("insert query:"+_SQL_source);
	    		  Source_sql1.add(_SQL_source);
	    		  p.write(_SQL_source+";");
	    	
	    		  p.write("\n");
	    	  }
	    	 // p.write(Source_sql1.toString());
	    	// p.close();
	    	   if (rows++ % 10000 == 1000) {
	                break;
	            }
	    	  
			} while (true);
			}catch (Exception e) {
					e.printStackTrace();
					log.debug(e.getMessage());
			}finally {
	
				     p.write("commit;");
			    	 p.close();
				connection.close();
				Source_Records.close();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
	}

	private void close(final Statement statement) {
	    if (statement == null) {
	        return;
	    }
	    try {
	        statement.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    	log.debug(e.getMessage());
	    }
	}

	private static Object[] getRowValues(final ResultSet resultSet, final ResultSetMetaData resultSetMetaData) throws SQLException {
	    List<Object> rowValues = new ArrayList<Object>();
	    for (int i = 2; i < resultSetMetaData.getColumnCount(); i++) {
	        rowValues.add(resultSet.getObject(i));
	    }
	    return rowValues.toArray(new Object[rowValues.size()]);
	}

	private static final Long hash(final Object... objects) {
	    StringBuilder builder = new StringBuilder();
	    for (Object object : objects) {
	        builder.append(object);
	    }
	    return hash(builder.toString());
	}

	public static ResultSet getResultSet(final Connection connection, final String tableName,String Suite_Name) {
		String query = "Select TESTCASEID ,FORMATNAME ,CHANNEL ,LSI ,CLIENT_FILE_REF ,BIC_CODE ,BBDID ,CUST_REF ,UNIQUE_ID ,FILENAME ,TIMESTAMP ,APPLIED_RULES ,ERROR_CODES ,MODIFIED_FIELD ,FOLDER_NAME ,SUITE_NAME from test_tool_file_name where testcaseid like '"+Suite_Name+"'";
	    System.out.println("queryLHS"+query);
	    return executeQuery(connection, query);
	}
	
	public static ResultSet ExecuteDelete_oldrecords(final Connection connection) {
	    String query = "Delete from test_tool_file_name ";
	    System.out.println("Delete from TEST_TOOL_FILE_NAME:"+query);
		log.info("Delete from TEST_TOOL_FILE_NAME:"+query);;
	    return executeQuery(connection, query);
	}
	
	public static ResultSet Conditional_DeleteOLDRECORDS(final Connection connection,String Suite_Name) {
	    String query = "Delete from test_tool_file_name where testcaseid like '"+Suite_Name+"'";
	    System.out.println("Delete from TEST_TOOL_FILE_NAME:"+query);
	    log.info("Delete from TEST_TOOL_FILE_NAME:"+query);
	    return executeQuery(connection, query);
	}
	
	public static ResultSet getResultSet_WORKITEMID(final Connection connection, final String tableName,String WORKITEMID,String REC_KEY) {
		String query = "Select REC_KEY,workitemid,CUST_REF_NUM,ACC_CCY,CR_NAME,DEBIT_AMOUNT,DR_ACCT_NO,LIMIT_BAL_CHK_FLAG,SETTL_METHOD,TXN_CLGSYSREF,TXN_STATUS,CHQ_DLVY_MTD,LIMIT_CHK_EQUI_AMT,VALUE_DATE,DR_EXEC_DATE,TRN_STATUS_CODE, TXNS_OTI09, CHQ_NUM, CHQ_TYP, CLIENT_ID, IPS_TRN_REF, LCL_REC_TYPEID, DR_STREET, DR_BLDG_NO, DR_CITY, DR_STATE, DR_PSTL_CODE, INIT_PRTY_ID, BPOI_DR_NM, ACNO_RETURN, TXNS_OTI08, TXN_CUST_SM, DR_AGT_STREET, DR_AGT_BLDG_NO, DR_AGT_STATE, DR_AGT_CTRY, DR_AGT_PSTL_CD, TFRI_PUR_FT, INSTR_AMOUNT, INSTR_CURRENCY, BAL_CHK_EQUI_AMT, INSTR_CODE1, EXCHANGE_RATE, DEAL_REF_NUM, FX_QUOTE_ID, FX_CONTRACT_ID, FX_RATE, FX_RATE_TYPE, PARAM_FX_CONT_CHK, NOTIONAL_EXCH_RATE, CR_STREET, CR_BLDG_NO, CR_AGT_NAME, CR_AGT_STREET, CR_AGT_BLDG_NO, CR_AGT_STATE, CR_AGT_CTRY, CR_AGT_PSTL_CD, CR_AGT_ADDR1, CR_AGT_ADDR2, CR_AGT_ADDR3, INTER_AGT_BIC, INTER_AGT_ADDR1, INTER_AGT_ADDR2, INTER_AGT_ADDR3, INTER_AGT_CTRY, INTER_AGT_CLRING_SYS, INTER_AGT_CLRING_SYS_CD, FEE_CCY, CORR_FEE_ACC_NO, CORR_TRANSIT, CHQ_FRM_NM, CHQ_FRM_STRT, CHQ_FRM_BLDG, CHQ_FRM_TW, CHQ_FRM_CTRY, CHQ_FRM_PSTL, CHQ_TO_NM, CHQ_TO_STRT, CHQ_TO_BLDG, CHQ_TO_TW, CHQ_TO_CTRY, CHQ_TO_PSTL, REMT_STATUS, REMT_INDICATOR, REMT_LANG, REMT_STATUS_DESC, INSTR_CODE_SL, INTER_AGT_CLRING_SYS_CD, SAR_LOC_MTD, REMT_CR_NM_TXN, REMT_CR_STRT_TXN, REMT_CR_BLDG_TXN, REMT_CR_CTRY_TXN, REMT_CR_PSTL_TXN, REMT_CR_LCFX, REMT_CR_LCEM, CLR_SYSM_CODE, NOC_FLAG, NOC_CORR_INFO, NOC_CHG_CODE from "+tableName+" where REC_KEY = '"+REC_KEY+"' AND CUST_REF_NUM IN('"+WORKITEMID+"')";
	    System.out.println("queryRHS:"+query);
	    log.info("queryRHS:"+query);
	    return executeQuery(connection, query);
	}

	private static final ResultSet executeQuery(final Connection connection, final String query) {
	    try {
	        return connection.createStatement().executeQuery(query);
	    } catch (SQLException e) {
	        throw new RuntimeException(e);
	    }
	}

	public static void GetList_SQL() {
		for (int i = 0; i < Source_sql.size(); i++) {
			System.out.println(Source_sql.get(i));
			log.info(Source_sql.get(i));
		}
	}

//	public static void Insert_Excetions(String TESTCASEID,String FILE_ID,String ROW_NUMBER,String workitemid_LHS,String workitemid_RHS,String DIFFERENCE_LHS,String DIFFERENCE_RHS,String Type_Test) throws ClassNotFoundException, SQLException {
//		Statement stmt=null;
//		Connection connection=null;
//	try {	
//				Class.forName("oracle.jdbc.driver.OracleDriver");
//				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16864","CIBC_PAYMENTS_WEB12C3","CIBC_PAYMENTS_WEBPASS");
//				stmt = connection.createStatement();
//				String DIFF_LHS_RHS_EXCEP_GRID_DETAIL_query="Insert into DIFF_LHS_RHS_EXCEP_GRID_DETAIL(TESTCASEID,FILE_ID,ROW_NUMBER,WORKITEMID_LHS,WORKITEMID_RHS,DIFFERENCE_LHS,DIFFERENCE_RHS,Type_Test) VALUES("
//						+ "'"+TESTCASEID.substring( 0, TESTCASEID.indexOf(","))+"',"
//						+ "'"+FILE_ID+"',"
//						+ "'"+ROW_NUMBER+"',"
//						+ "'"+workitemid_LHS+"',"
//						+ "'"+workitemid_RHS+"',"
//						+ "'"+DIFFERENCE_LHS+"',"
//						+ "'"+DIFFERENCE_RHS+"',"
//						+ "'"+Type_Test+"')";
//				stmt.executeUpdate(DIFF_LHS_RHS_EXCEP_GRID_DETAIL_query);
//		}catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			 stmt.close();
//			 connection.close();
//		}	
//	}

public static void Execute_QueriesDestination(String suiteName) throws Exception {

	
	String classname =PropertyUtils.readProperty("PSH_CLASS");
	String URL = PropertyUtils.readProperty("PSH_DBURLC");
	String user = PropertyUtils.readProperty("PSH_DBUserName");
	String pwd = PropertyUtils.readProperty("PSH_DBUserPass");
	
	Connection connection=null;
	Statement stmt=null;
	
	try {	
				Class.forName(classname);
				connection=DriverManager.getConnection(URL,user,pwd);
				//Delete Old Records In destination
				Conditional_DeleteOLDRECORDS(connection,suiteName);
		
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}finally {
			
			 connection.close();
		}	
	}

public static void DeleteTest_Tool_File_Name_ENV1() throws Exception {
//	String classname = "oracle.jdbc.driver.OracleDriver";
//	String URL = "jdbc:oracle:thin:@10.10.9.55:1521:SIR16864";
//	String user = "CIBC_PAYMENTS_WEB12C3";
//	String pass = "CIBC_PAYMENTS_WEBSWIFT";

	String classname =PropertyUtils.readProperty("PSH_CLASS");
	String URL = PropertyUtils.readProperty("PSH_FGTDBURL");
	String user = PropertyUtils.readProperty("PSH_FGTDBUserName");
	String pass = PropertyUtils.readProperty("PSH_FGTDBUserPass");
	
	Connection connection=null;
//	Statement stmt=null;
	
	try {	
				Class.forName(classname);
				connection=DriverManager.getConnection(URL,user,pass);
				ExecuteDelete_oldrecords(connection);
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}finally {
//			 stmt.close();
			 connection.close();
		}	
	}

public static void DeleteTest_Tool_File_Name_ENV2() throws Exception {
//	String classname = "oracle.jdbc.driver.OracleDriver";
//	String URL = "jdbc:oracle:thin:@10.10.9.55:1521:SIR16864";
//	String user = "CIBC_PAYMENTS_WEB12C3";
//	String pass = "CIBC_PAYMENTS_WEBSWIFT";

	String classname =PropertyUtils.readProperty("PSH_CLASS");
	String URL = PropertyUtils.readProperty("PSH_DBURL");
	String user = PropertyUtils.readProperty("PSH_DBUserName");
	String pwd = PropertyUtils.readProperty("PSH_DBUserPass");
	
	Connection connection=null;
//	Statement stmt=null;
	
	try {	
				Class.forName(classname);
				connection=DriverManager.getConnection(URL,user,pwd);
				ExecuteDelete_oldrecords(connection);
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}finally {
//			 stmt.close();
			 connection.close();
		}	
	}



public static void PREExecute_Queries() throws Exception {
	Connection connection=null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				stmt.executeUpdate("update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('10006722')");
				stmt.executeUpdate("update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('12000048')");
				stmt.executeUpdate("update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='12000056'");
				stmt.executeUpdate("update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='20200821'");
				stmt.executeUpdate("update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='10006687'");
				stmt.executeUpdate("update stub_psh_envoy set balancesavailableamt=1000000000 where accounttransitnumber='2012008'");
				stmt.executeUpdate("update stub_wire_pmt  set statuscode='000', resp_type='S',APPROVALCODE='APPROV' where pan='4591237894251369'");
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}finally {
			 stmt.close();
			 
			 connection.close();
		}	
	}

public static void PREExecute_Queries_SANITY() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/SANITY/STUB Update Query/Stub Update Query for Sanity.sql";
	Reader reader = null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
				log.info("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}


/*public static void ExecutePreQueries() throws Exception{
	try{
	Fillo fill2=new Fillo();
	com.codoid.products.fillo.Connection conn2=fill2.getConnection("D:\\PSH_Automation\\Excel_Files\\Test_Data.xlsx");			
	Recordset rs2=conn2.executeQuery("Select * from MainController where RUNSTATUS='Yes'");
	while(rs2.next()) {
		String TEST_SCENARIO = rs2.getField("TEST_SCENARIO");
		System.out.println(TEST_SCENARIO);
		
		switch (TEST_SCENARIO) {
		case "SANITY":
			String SANITY_SQL = UrlConstants._SANITY_SQL;
				PREExecute_QueriesCC2(SANITY_SQL);
			break;
		case "OnUs E2E":
			String OnUs_E2E = UrlConstants._OnUs_E2E;
				PREExecute_QueriesCC2(OnUs_E2E);
			break;
		case "PH3 E2E":
			String PH3_E2E = UrlConstants._PH3_E2E;
			System.out.println(PH3_E2E);
				PREExecute_QueriesCC2(PH3_E2E);
			break;
		case "PH2 E2E":
			String PH2_E2E = UrlConstants._PH2_E2E;
				PREExecute_QueriesCC2(PH2_E2E);
			break;
		case "REGRESSION":
			String REGRESSION = UrlConstants._REGRESSION;
				PREExecute_QueriesCC2(REGRESSION);
			break;
		case "CC1 Reg":
			String CC1_Reg = UrlConstants._CC1_Reg;
				PREExecute_QueriesCC2(CC1_Reg);
			break;
		case "QUICK_SANITY":
			String QUICK_SANITY = "D:/PSH_Automation/JAR/Customer Update Queries/Quick_SANITY/QUICK_SANITY.sql";
				PREExecute_QueriesCC2(QUICK_SANITY);
			break;
		case "Business Validation":
			String Business_Validation = "D:/PSH_Automation/JAR/Customer Update Queries/Business Validation/BVM Update scripts.sql";
				PREExecute_QueriesCC2(Business_Validation);
		case "Manual Action":
			String Manual_Action = "D:/PSH_Automation/JAR/Customer Update Queries/Manual Action/Manual action prequeries.sql";
				PREExecute_QueriesCC2(Manual_Action);
			
			break;
	
		default:
			break;
		}
	}
  }catch (Exception e) {
		// TODO: handle exception
  }
}
*/

public static void CustomerUpdateQueries() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/SANITY/STUB Update Query/Stub Update Query for Sanity.sql";
	Reader reader = null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
				log.info("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}



public static void PREExecute_Queries_ONUS() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/ONUS/STUB Update Query/Stub Update Query for Onus E2E.sql";
	Reader reader = null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}


public static void PREExecute_Queries_SETUP() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/SETUP_Query/Customer_Query.sql";
	Reader reader = null;
	Statement stmt=null;
	
	String classname =PropertyUtils.readProperty("PSH_CLASS");
	String URL = PropertyUtils.readProperty("PSH_FGTDBURL");
	String user = PropertyUtils.readProperty("PSH_FGTDBUserName");
	String pass = PropertyUtils.readProperty("PSH_FGTDBUserPass");
	
	try {	
				Class.forName(classname);
				connection=DriverManager.getConnection(URL,user,pass);
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}



public static void PREExecute_Queries_Sanity() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/SANITY/STUB Update Query/Stub Update Query for Sanity.sql";
	Reader reader = null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}

public static void PREExecute_Queries_PH3_E2E() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/REG PH3/Stub_Queries/Pre-Stub Queries_Phase III Regression.sql";
	Reader reader = null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}

public static void PREExecute_Queries_Regression() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:\\PSH_Automation\\JAR\\Customer Update Queries\\REGRESSION\\Regression Update SQL.sql";
	Reader reader = null;
	Statement stmt=null;
	try {	
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}



public static void PREExecute_Queries_PH2_E2E() throws Exception {
	Connection connection=null;
	String scriptFilePath = "D:/PSH_Automation/JAR/Customer Update Queries/REG PH2/Stub_Queries/Pre_Stub Queries_Phase II Regression_Updated_25052018.sql";
	Reader reader = null;
	Statement stmt=null;
	try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16866","CIBC_PSH_CORE3","CIBC_PSH_CORE3");
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}

public static void PREExecute_QueriesCC2(String SQL_Path) throws Exception {
	Connection connection=null;
	String scriptFilePath = SQL_Path;
	Reader reader = null;
	Statement stmt=null;
	
	String classname ="oracle.jdbc.driver.OracleDriver";
	String URL = PropertyUtils.readProperty("PSH_FGTDBURL");
	String user = PropertyUtils.readProperty("PSH_FGTDBUserName");
	String pwd = PropertyUtils.readProperty("PSH_FGTDBUserPass");
	
	try {	
				Class.forName(classname);
				connection=DriverManager.getConnection(URL,user,pwd);
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
				System.out.println("Executed SQL Migration Successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 stmt.close();
			 connection.close();
		}	
	}

	public static void main(String[] args) throws Exception{
		//ExecutePreQueries();
		Data_MigrationDB.Source_DestinationDBMigration("SANITY_TC%");
//		Data_MigrationDB.Execute_QueriesDestination();
//		PREExecute_Queries_ONUS();
//		PREExecute_Queries_PH2_E2E();
//		PREExecute_QueriesCC2();
		
//		String classname = Read_Config.getENV2_OracleDriver();
//		String URL = Read_Config.getENV2_ConnName();
//		String user = Read_Config.getENV2_Uname();
//		String pass = Read_Config.getENV2_Pass();
//		
//		System.out.println(classname);
//		System.out.println(URL);
//		System.out.println(user);
//		System.out.println(pass);
//		Data_MigrationDB.DeleteTest_Tool_File_Name_ENV1();
//		Data_MigrationDB.DeleteTest_Tool_File_Name_ENV2();
	}
}
