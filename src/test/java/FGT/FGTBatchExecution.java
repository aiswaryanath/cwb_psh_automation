package FGT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

public class FGTBatchExecution {

	public static void main(String[] args) throws Exception {
	
	
	   fgtexecute("CWB_FGT_Positive_Santosh.xlsx","20000","CWB_BVM_Santosh");
	//	H2HB_FileUpload.File_Operation_H2HB();
	//	DIGI_FileUpload.File_Operation_DIGI();
	//	H2HC_FileUpload.File_Operation_H2HC();
	//	INBOUND.File_Operation_INBOUND();
	
	}
	
	public static String fgtexecute(String sheetname,String millis,String Suite) throws InterruptedException, IOException {
		String result="PASS";

		try
		
		{
			File f=new File(System.getProperty("user.dir")+"\\output.txt");
			
			FileUtils.deleteQuietly(f);
			ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "Start", "FGT.bat",sheetname);
			
			File dir = new File(System.getProperty("user.dir"));
			
			
		pb.directory(dir);
	Process p = pb.start(); 
			//keep this dynamic
			Thread.sleep(Long.parseLong(millis));
			/*
			boolean flag=false;
			File f1=new File("output.txt");
			BufferedReader reader=new BufferedReader(new FileReader(f1));
			String currentLine=reader.readLine();
		
		while(currentLine!=null && !flag)
			
		{
			
			if(currentLine.contains("FGT Executed successfully"))
			{
				
				System.out.println("FGT Execution complete");
				flag=true;//14th seconds
				break;
			}
			
			 currentLine=reader.readLine();
			 //Thread.sleep(1000);
		}
		reader.close();
		if(flag==true)
		{
		//	Runtime.getRuntime().exec("taskkill /F /IM cmd.exe /T");
		}
		*/
		FGTDataRead m=new FGTDataRead();
		try
		{
		m.FGTfolderdata(Suite);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{
		H2HB_FileUpload.File_Operation_H2HB();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		try
		{
		H2HC_FileUpload.File_Operation_H2HC();
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
		try
		{
		DIGI_FileUpload.File_Operation_DIGI();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{
		INBOUND.File_Operation_INBOUND();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		}
	
	
	
	catch(Exception e)
	{
		 result="FAIL";
	}
		return result;
	
	
	
}
}
