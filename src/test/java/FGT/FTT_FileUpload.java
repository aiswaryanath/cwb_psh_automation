/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena aLtd. All rights reserved */
/* */
/************************************************************************/
/* Application : PSH Integration Automation	*/
/* Module Name : SCA_FileUpload.java */
/* */
/* File Name : SCA_FileUpload.java */
/* */
/* Description : Used to Upload SCA files*/
/* */
/* Author : Varun Paranganath */
/****************************************************************************************************/

package FGT;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import Utilities.PropertyUtils;

//import Read_Configuration.SFTP_Config;

public class FTT_FileUpload {
	
	public static void File_Operation_FTT() throws Exception{
		String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FTT";
		//File directory =new File("D:/CBX_Automation/LOCAL_PAYMENT_FILE/FTT");
		File directory =new File(filepath);
		File[] file_list=directory.listFiles();
		for (File files: file_list) {
			String P_TString = files.getAbsolutePath().toString().substring(files.getAbsolutePath().length()-10);
			if(P_TString.substring(0, 3).equalsIgnoreCase(".T.")){
				System.out.println(files.getAbsolutePath()+""+files.getName());
				UPLoad_FilesServer_FTT_Testing(files.getAbsolutePath(),files.getName());
			}
			if(P_TString.substring(0, 3).equalsIgnoreCase(".P.")){
				System.out.println(files.getAbsolutePath()+""+files.getName());
				UPLoad_FilesServer_FTT_Production(files.getAbsolutePath(),files.getName());
			}
			
			if (!(P_TString.substring(0, 3).equalsIgnoreCase(".T.") || (P_TString.substring(0, 3).equalsIgnoreCase(".P.")))) {
				System.out.println(files.getAbsolutePath()+""+files.getName());
				UPLoad_FilesServer_FTT_Production(files.getAbsolutePath(),files.getName());
			}
		}
	}
	
	public static void UPLoad_FilesServer_FTT_Production(String FILE_PATH,String FILE_NAME) throws Exception{
//		String hostname = "10.10.8.70";
//		String login = "SIR16738";
//		String password = "intellect@123";
//		String directory = "/usr1/SIR16738/GTB_HOME/IPSH_HOME/filearea/SPLITTING/SCA/incoming";
		
		String hostname =  PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password =  PropertyUtils.readProperty("PSHAPP_Password");
		String directory = PropertyUtils.readProperty("PSH_FTT");
		
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		try {
			String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
					if(envdetail.contains("CLOUD"))
					{	
					String ppk=	PropertyUtils.readProperty("ENV_PPK");
					String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
					//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
				
						ssh.addIdentity(privateKey);
					}
			}		catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		Session session;
		
		try{
			//Getting Session Of Server LINUX
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications","publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			//CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			
			Channel sftp = (ChannelSftp) channel;
			((ChannelSftp) sftp).cd(directory);
			
				File file =new File(FILE_PATH);
				FileInputStream fileinputstream=new FileInputStream(file);
				channel.put(fileinputstream,file.getName());
			
				channel.chmod(0777, directory+"/"+FILE_NAME);
				channel.rename(directory+"/"+FILE_NAME,directory+"/"+FILE_NAME.substring(0, FILE_NAME.length() - 4));
				Thread.sleep(20000);
				
			channel.disconnect();
			session.disconnect();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
public static void UPLoad_FilesServer_FTT_Testing(String SCA_FILE_PATH,String FILE_NAME) throws Exception{
//		String hostname = "10.10.8.70";
//		String login = "SIR16738";
//		String password = "intellect@123";
//		String directory = "/usr1/SIR16738/GTB_HOME/IPSH_HOME/filearea/SPLITTING/SCA_TEST/incoming";
		
	String hostname =  PropertyUtils.readProperty("PSHAPP");
	String login = PropertyUtils.readProperty("PSHAPP_username");
	String password =  PropertyUtils.readProperty("PSHAPP_Password");
	String directory = PropertyUtils.readProperty("PSH_FTT_TEST");
		
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		try {
			String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
					if(envdetail.contains("CLOUD"))
					{	
					String ppk=	PropertyUtils.readProperty("ENV_PPK");
					String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
					//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
				
						ssh.addIdentity(privateKey);
					}
			}		catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
		Session session;
		
		try{
			//Getting Session Of Server LINUX
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications","publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			//CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			
			Channel sftp = (ChannelSftp) channel;
			((ChannelSftp) sftp).cd(directory);
			
				File file =new File(SCA_FILE_PATH);
				FileInputStream fileinputstream=new FileInputStream(file);
				channel.put(fileinputstream,file.getName());
			
				channel.chmod(0777, directory+"/"+FILE_NAME);
				channel.rename(directory+"/"+FILE_NAME,directory+"/"+FILE_NAME.substring(0, FILE_NAME.length() - 4));
				Thread.sleep(20000);
				
			channel.disconnect();
			session.disconnect();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception{
          File_Operation_FTT();
	}
}
