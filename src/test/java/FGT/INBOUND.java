package FGT;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import Utilities.PropertyUtils;

public class INBOUND {
	public static ArrayList<String> INBOUND_array=null; 
	public static ArrayList<String> INBOUND_Filename =null;
		
		public static void File_Operation_INBOUND() throws Exception{
			String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\INBOUND";
		
			File directory =new File(filepath);
			File[] file_list=directory.listFiles();
			for (File files: file_list) {
				String P_TString = files.getAbsolutePath().toString().substring(files.getAbsolutePath().length()-10);
				System.out.println("Absolute path:"+P_TString);
				if(P_TString.contains(".T.")){
				System.out.println("testing");
					System.out.println(files.getName());
					UPLoad_FilesServer_INBOUND_Testing(files.getAbsolutePath(),files.getName());
				}
				if(P_TString.contains(".P.")){
					System.out.println("production");
					System.out.println(files.getName());
					UPLoad_FilesServer_INBOUND_Production(files.getAbsolutePath(),files.getName());
				}
				
			
			}
		}

		public static void UPLoad_FilesServer_INBOUND_Production(String FILE_PATH,String FILE_NAME) throws FilloException, IOException{
//			String hostname = "10.10.8.70";
//			String login = "SIR16738";
//			String password = "intellect@123";
//			String directory = "/usr1/SIR16738/GTB_HOME/IPSH_HOME/filearea/SPLITTING/INBOUND/incoming";
			
			String hostname =  PropertyUtils.readProperty("PSHAPP");
			String login = PropertyUtils.readProperty("PSHAPP_username");
			String password =  PropertyUtils.readProperty("PSHAPP_Password");
			String directory = PropertyUtils.readProperty("PSH_INBOUND");
			
			System.out.println(directory);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch ssh = new JSch();
			String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
			if(envdetail.contains("CLOUD"))
			{	
			String ppk=	PropertyUtils.readProperty("ENV_PPK");
			String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
			//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
			//System.out.println(privateKey);
			try {
				ssh.addIdentity(privateKey);
			} catch (JSchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			Session session;
			
			try{
				//Getting Session Of Server LINUX
				session = ssh.getSession(login, hostname, 22);
				session.setConfig(config);
				session.setConfig("PreferredAuthentications","publickey,keyboard-interactive,password");
				session.setPassword(password);
				session.connect();

				//CONNECTED TO CHANNEL
				ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
				channel.connect();
			//	System.out.println(channel.isConnected());
				Channel sftp = (ChannelSftp) channel;
				((ChannelSftp) sftp).cd(directory);
			//  System.out.println(channel.get(directory));
					File file =new File(FILE_PATH);
					FileInputStream fileinputstream=new FileInputStream(file);
					channel.put(fileinputstream,file.getName());	
				System.out.println("reached here:"+file.getName());
			channel.chmod(0777, directory+"/"+FILE_NAME);
		 channel.rename(directory+"/"+FILE_NAME,directory+"/"+FILE_NAME.substring(0, FILE_NAME.length() - 6));
					Thread.sleep(4000);
					
				channel.disconnect();
				session.disconnect();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		public static void UPLoad_FilesServer_INBOUND_Testing(String INBOUND_FILE_PATH,String FILE_NAME) throws Exception{
//			String hostname = "10.10.8.70";
//			String login = "SIR16738";
//			String password = "intellect@123";
//			String directory = "/usr1/SIR16738/GTB_HOME/IPSH_HOME/filearea/SPLITTING/INBOUND_TEST/incoming";
			
			String hostname =  PropertyUtils.readProperty("PSHAPP");
			String login = PropertyUtils.readProperty("PSHAPP_username");
			String password =  PropertyUtils.readProperty("PSHAPP_Password");
			String directory = PropertyUtils.readProperty("PSH_INBOUND_TEST");
			System.out.println(directory);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch ssh = new JSch();
			try {
				String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
						if(envdetail.contains("CLOUD"))
						{	
						String ppk=	PropertyUtils.readProperty("ENV_PPK");
						//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
						String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
							ssh.addIdentity(privateKey);
						}
				}		catch (JSchException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			Session session;
			
			try{
				//Getting Session Of Server LINUX
				session = ssh.getSession(login, hostname, 22);
				session.setConfig(config);
				session.setConfig("PreferredAuthentications","publickey,keyboard-interactive,password");
				session.setPassword(password);
				session.connect();

				//CONNECTED TO CHANNEL
				ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
				channel.connect();
				
				Channel sftp = (ChannelSftp) channel;
				((ChannelSftp) sftp).cd(directory);
				
					File file =new File(INBOUND_FILE_PATH);
					FileInputStream fileinputstream=new FileInputStream(file);
					channel.put(fileinputstream,file.getName());
				
					channel.chmod(0777, directory+"/"+FILE_NAME);
					channel.rename(directory+"/"+FILE_NAME,directory+"/"+FILE_NAME.substring(0, FILE_NAME.length() - 6));
					Thread.sleep(4000);
					
				channel.disconnect();
				session.disconnect();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		public static void main(String[] args) throws Exception{
		File_Operation_INBOUND();
	}
}
