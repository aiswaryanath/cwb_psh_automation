package FGT;

public class FGTPOJO {
@Override
	public String toString() {
		return "FGTPOJO [Testcaseid=" + Testcaseid + ", filename=" + filename + ", folder=" + folder + "]";
	}
String Testcaseid;
String filename;
String format;
String folder;
public String getTestcaseid() {
	return Testcaseid;
}
public void setTestcaseid(String testcaseid) {
	Testcaseid = testcaseid;
}
public String getFilename() {
	return filename;
}
public void setFilename(String filename) {
	this.filename = filename;
}
public String getFormat() {
	return format;
}
public void setFormat(String format) {
	this.format = format;
}
public String getFolder() {
	return folder;
}
public void setFolder(String folder) {
	this.folder = folder;
}
}
