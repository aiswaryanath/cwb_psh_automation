package FGT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebElement;

import Utilities.DBConnector;

public class FGTDataRead {

	public static void main(String[] args) throws Exception {
		FGTfolderdata("CWB_SUITE");	
	}
	 public static   void FGTfolderdata(String suite) throws Exception{

			// TODO Auto-generated method stub
			
			List<String> po=new ArrayList<String>();
			List<String> folder1=new ArrayList<String>();
			List<FGTPOJO> pojo=new ArrayList<FGTPOJO>();
			
				FileReader fir=new FileReader(new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\mapping.txt"));
				   BufferedReader br=new BufferedReader(fir); 
		
				   while(br.ready())
			
				   {
			
					
					   po=br.lines().collect(Collectors.toList());
				 
				   }
				   System.out.println(po.size());
				   po.stream().forEach( System.out::println );
					//po.stream().forEach(System.out::println);
				   DBConnector db=new DBConnector();
				  Connection c= db.getConnection("");
				   String SQL_INSERT = "INSERT INTO TEST_TOOL_FILE_NAME (TESTCASEID,CHANNEL,FILENAME,SUITE_NAME,FORMATNAME) VALUES (?,?,?,?,?)";
delete_entries(suite);
					for(String p:po)
					{
					//	p=p.replaceAll("\\w=","");
						FGTPOJO f=new FGTPOJO();
						String[] fgt=p.split(",");
						String tcid[]=(fgt[0].split("="));
						String filename[]=(fgt[1].split("="));
						String folder[]=(fgt[2].split("="));
						String format[]=(fgt[3].split("="));
						f.setTestcaseid(tcid[1]);				
							f.setFilename(filename[1]);			
						f.setFolder(folder[1]);
						f.setFormat(format[1]);
						pojo.add(f);
						PreparedStatement preparedStatement = c.prepareStatement(SQL_INSERT);
				
				            preparedStatement.setString(1,f.getTestcaseid());
				            preparedStatement.setString(2,f.getFolder());
				            preparedStatement.setString(3,f.getFilename());
				            preparedStatement.setString(4,suite);
				            preparedStatement.setString(5,f.getFormat());
				           // System.out.println( preparedStatement.get);
				         preparedStatement.execute();

				            // rows affected
				            System.out.println("No of rows inserted:"+f.getFilename()); //1
						//System.out.println(f.toString());
					}
					//c.commit();
					c.close();
					Set<String> th=new TreeSet<String>();
			
					for(int i=0;i<pojo.size();i++){
						FGTPOJO ol=pojo.get(i)		;
						//System.out.println(ol.getFolder()+" "+ol.getTestcaseid());
						th.add(ol.getFolder());
						
					}
					
					try
					{
						File dir2 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\"+"DIGI");
						FileUtils.deleteDirectory(dir2);
						dir2.mkdir();
					}
					catch(Exception e)
					{
						
					}
					try
					{
						File dir2 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\"+"INBOUND");
						FileUtils.deleteDirectory(dir2);
						dir2.mkdir();
					}
					catch(Exception e)
					{
						
					}
					try
					{
						File dir2 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\"+"H2HC");
						FileUtils.deleteDirectory(dir2);
						dir2.mkdir();
					}
					catch(Exception e)
					{
						
					}
					try
					{
						File dir2 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\"+"H2HB");
						FileUtils.deleteDirectory(dir2);
						dir2.mkdir();
					}
					catch(Exception e)
					{
						
					}
					
					
					for(int i=0;i<pojo.size();i++){
						FGTPOJO ol=pojo.get(i)		;
						System.out.println("Should reach here:"+ol.getFolder());
						File src=new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\Files\\"+ol.getFilename());
						File dest=new File(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\"+ol.getFolder());
						File in=new File(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\INBOUND");
						FileUtils.copyFileToDirectory(src,dest);
						if(ol.getFilename().startsWith("DATAELUS")||ol.getFilename().startsWith("DATAELEC"))
								{

							FileUtils.copyFileToDirectory(src,in);
								}
						
					
					}
					
						
				
		
						
	 }
	 
	public static void delete_entries(String query) throws Exception
	 {
		 DBConnector db=new DBConnector();
		  Connection c= db.getConnection("");
		  int n=0;
		   String sql_delete = "DELETE FROM TEST_TOOL_FILE_NAME WHERE SUITE_NAME="+"'"+query+"'";
		   Statement ps = c.createStatement();
		   try
		   {
		 n= ps.executeUpdate(sql_delete);
		 System.out.println("No. of records deleted:"+n);
		   }
		   catch(Exception e)
		   {
			   System.out.println(e.getLocalizedMessage()) ;
		   }
		   finally
		   {
			 
			   ps.close();
			   c.close();
		   }
		  
		   
	 }
}
