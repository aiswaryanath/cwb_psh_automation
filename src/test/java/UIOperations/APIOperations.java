package UIOperations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.json.JSONObject;
import org.testng.Reporter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import UIOperations.Keywordoperations;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIOperations {
	
	public static Response response;
	FileUtilities Fileutil = new FileUtilities();
	
	public static String ReplaceValueJSON(String VAR_PAYMENTREFNO,String VAR_PARTICIPANTPAYMENTREFNO,String VAR_MESSAGEID) throws Exception {
		String NotificationReq_API = PropertyUtils.readProperty("NotificationReq_API");
		String finalstring = NotificationReq_API
				.replace("VAR_PAYMENTREFNO", VAR_PAYMENTREFNO)
				.replace("VAR_PARTICIPANTPAYMENTREFNO", VAR_PARTICIPANTPAYMENTREFNO);
		return finalstring;
	}
	
	//Used Here
	public static String ReplaceValueURI(String VAR_CUSTOMERID,String VAR_ERRORCODE,String VAR_TEMPLATEID,String VAR_SEARCHPARAM,String VAR_SETHEADERS) throws Exception{
		String _URI = PropertyUtils.readProperty("STATELESS_API");
		String finalstring = _URI
				.replace("VAR_CUSTOMERID", VAR_CUSTOMERID)
				.replace("VAR_ERRORCODE", VAR_ERRORCODE)
				.replace("VAR_TEMPLATEID", VAR_TEMPLATEID)
				.replace("VAR_SEARCHPARAM", VAR_SEARCHPARAM)
				.replace("VAR_SETHEADERS", VAR_SETHEADERS);
		return finalstring;
	}

	public static String INTERAC_PARAM_OVERIDE_URI(String VAR_CUSTOMERID,String VAR_PARAM,String VAR_PARAMVALUE,String VAR_TEMPLATEID,String VAR_SEARCHPARAM,String VAR_SETHEADERS) throws Exception{
		String _URI = PropertyUtils.readProperty("INTERAC_PARAM_OVERIDE_API");
		String finalstring = _URI
				.replace("VAR_CUSTOMERID", VAR_CUSTOMERID)
				.replace("VAR_PARAM", VAR_PARAM)
				.replace("PARAMVALUE", VAR_PARAMVALUE)
				.replace("VAR_TEMPLATEID", VAR_TEMPLATEID)
				.replace("VAR_SEARCHPARAM", VAR_SEARCHPARAM)
				.replace("VAR_SETHEADERS", VAR_SETHEADERS);
		return finalstring;
	}

	
	public static String getSaltString(String Flag, int _NumLength,String VAR_MESSAGEID)  throws Exception {
		String SALTCHARS = "1234567890";
		String Result = null;
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < _NumLength) {
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		if (Flag.equals("Y")) {
			Result = VAR_MESSAGEID + saltStr;
		} else {
			Result = saltStr;
		}
		return Result;
	}

	public static String getSimpleDateFormat() throws Exception {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'20:53:23.234'Z'");
		String dateAsISOString = df.format(date);
		System.out.println(dateAsISOString);
		return dateAsISOString;
	}
	
public static String UpdateSTATELESS_API(String TestData) throws Exception {		
		boolean flag_res = true;
		String prettyJsonString=null;
	   try{
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setContentType("application/json; charset=UTF-8");
			RequestSpecification request = builder.build();
			
			String VAR_TEMPLATEID = Keywordoperations.get_DataValue(TestData).get(0);
			String VAR_CUSTOMERID = Keywordoperations.get_DataValue(TestData).get(1);
			String VAR_ERRORCODE = Keywordoperations.get_DataValue(TestData).get(2);
			String VAR_SEARCHPARAM = Keywordoperations.get_DataValue(TestData).get(3);
			String VAR_SETHEADERS = Keywordoperations.get_DataValue(TestData).get(4);
			
			System.out.println(PropertyUtils.readProperty("HOST_NAME") +""+ReplaceValueURI(VAR_TEMPLATEID, VAR_CUSTOMERID, VAR_ERRORCODE,VAR_SEARCHPARAM,VAR_SETHEADERS));
			Thread.sleep(1000); 
			    response = RestAssured.given().spec(request).when().post(PropertyUtils.readProperty("HOST_NAME") +""+ReplaceValueURI(VAR_TEMPLATEID, VAR_CUSTOMERID, VAR_ERRORCODE,VAR_SEARCHPARAM,VAR_SETHEADERS));
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(response.body().asString());
				prettyJsonString = gson.toJson(je);
				System.out.println(prettyJsonString);
	   }catch (Exception e) {
		   e.printStackTrace();
	}
   return "Status_Code:"+response.getStatusCode()+"|Response:"+prettyJsonString; 
}

public static String UpdateSTATELESSinteracParamOveride_API(String TestData) throws Exception {		
	boolean flag_res = true;
	String prettyJsonString=null;
   try{
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setContentType("application/json; charset=UTF-8");
		RequestSpecification request = builder.build();

		String VAR_TEMPLATEID = Keywordoperations.get_DataValue(TestData).get(0);
		String VAR_CUSTOMERID = Keywordoperations.get_DataValue(TestData).get(1);
		String VAR_PARAM = Keywordoperations.get_DataValue(TestData).get(2);
		String VAR_PARAMVALUE = Keywordoperations.get_DataValue(TestData).get(3);
		String VAR_SEARCHPARAM = Keywordoperations.get_DataValue(TestData).get(4);
		String VAR_SETHEADERS = Keywordoperations.get_DataValue(TestData).get(5);

		Thread.sleep(1000); 
		    response = RestAssured.given().spec(request).when().post(PropertyUtils.readProperty("HOST_NAME") +""+INTERAC_PARAM_OVERIDE_URI(VAR_CUSTOMERID, VAR_PARAM, VAR_PARAMVALUE,VAR_TEMPLATEID,VAR_SEARCHPARAM,VAR_SETHEADERS));
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(response.body().asString());
			prettyJsonString = gson.toJson(je);
			System.out.println(prettyJsonString);
   }catch (Exception e) {
	e.printStackTrace();
 }
return "Status_Code:"+response.getStatusCode()+"|Response:"+prettyJsonString; 
}

//----------------------------------------NOTIFICATION API
public static String ReplaceValueJSON_NOTIFICATION(String VAR_PARTICIPANTPAYMENTREFNO) throws Exception {
	String NotificationReq_API = PropertyUtils.readProperty("NotificationReq_API");
	String finalstring = NotificationReq_API
			.replace("VAR_PARTICIPANTPAYMENTREFNO", VAR_PARTICIPANTPAYMENTREFNO);
	return finalstring;
}

public static String INTERAC_NOTIFICATION_URI(String CUST_ID) throws Exception{
	String _URI = PropertyUtils.readProperty("POST_NOTIF_SNDMONEY_API");
	String finalstring = _URI
			.replace("CUST_ID", CUST_ID);
	return finalstring;
}


public static String POST_SENDMoneyNotificationAPI(String TestData) throws Exception {
	String Result_Resp=null;
	String VAR_PARTICIPANTPAYMENTREFNO=null;
	FileUtilities FileUtilities = new FileUtilities();
	Keywordoperations Keywordoperations=new Keywordoperations();
	try {
		String Check_var=null;
		try{
			Check_var = Keywordoperations.get_DataValue(TestData).get(1);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if (Check_var.equals(" ")) {
			String _MSG_SEQID = FileUtilities.GetFilevalue("VAR_MESSAGESEQID_SUB");
			VAR_PARTICIPANTPAYMENTREFNO = _MSG_SEQID.replaceAll("[^a-zA-Z0-9]", "");
		}else{
			VAR_PARTICIPANTPAYMENTREFNO = Keywordoperations.get_DataValue(TestData).get(1);
		}
		String participant_userid=Keywordoperations.get_DataValue(TestData).get(2);
		String MESSAGE_ID = Keywordoperations.get_DataValue(TestData).get(3);
		String CUST_ID="123";
		
		String StoreRequest_id = "AUT_" + getSaltString("N", 5,MESSAGE_ID);
		Keywordoperations.StoreValue(Keywordoperations.get_DataValue(TestData).get(4), StoreRequest_id);
		
		URL url = new URL(PropertyUtils.readProperty("HOST_NAME") +""+INTERAC_NOTIFICATION_URI(CUST_ID));
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("x-et-api-signature", "CA000003");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("x-et-channel-indicator", "ONLINE");
		conn.setRequestProperty("x-et-participant-id", "CA000010");
		conn.setRequestProperty("x-et-request-id",StoreRequest_id);
		conn.setRequestProperty("x-et-retry-indicator", "TRUE");
		conn.setRequestProperty("x-et-transaction-time",getSimpleDateFormat());
		conn.setRequestProperty("x-et-participant-user-id", participant_userid);
		conn.setRequestProperty("X-et-api-signature-type","PAYLOAD_DIGEST_SHA256");

		String input = ReplaceValueJSON_NOTIFICATION(VAR_PARTICIPANTPAYMENTREFNO);
		System.out.println(input);
		
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
		System.out.println(conn.getResponseCode());
		Result_Resp = Integer.toString(conn.getResponseCode());
		conn.disconnect();
	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	}
	return Result_Resp;
}

public void StoreValue(String key, String Value) {
	Value = Value.replace("\n", " ");
	if (key.contains("VAR")) {
		Fileutil.writeUsingFileWriter(key + "=" + Value);
	}
}




//*******************************************API

public static String GetTRANSFER_OPTIONS_API(String TestData) throws Exception {
	FileUtilities FileUtilities = new FileUtilities();
	Keywordoperations _StoreMessageID = new Keywordoperations();
	String _TestData = FileUtilities.GetFilevalue(Keywordoperations.get_DataValue(TestData).get(0));
	String API_Url = PropertyUtils.readProperty("HOST_NAME") +""+APIOperations.ReplaceValueGETTransferOptions("DOMESTIC", "ACCOUNT_DEPOSIT", "6013000201");
	System.out.println("Elastic url" + API_Url);
	String ReferrenceID = _TestData;
	String getVal = PropertyUtils.readProperty("MsgId");
	System.out.println("Elastic msgid" + getVal);
	String _messageID = null;
	try {
		RestAssured.baseURI = API_Url;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = GetResponse(ReferrenceID, httpRequest);
		httpRequest.headers("x-et-api-signature","12345");
		httpRequest.headers("x-et-api-signature-type","PAYLOAD_DIGEST_SHA256");
		httpRequest.headers("x-et-channel-indicator","ONLINE");
		httpRequest.headers("x-et-participant-id","12357");
		httpRequest.headers("x-et-participant-user-id","P00012");
		httpRequest.headers("x-et-request-id","Senorita98");
		httpRequest.headers("x-et-transaction-time","5-15-2018");
		httpRequest.headers("Content-Type","application/json");
		
		JsonPath jsonPathEvaluator = GetJSONPathvalue(response);
		_messageID = GetMessageID(getVal, jsonPathEvaluator);
		System.out.println("Elastic resposne" + _messageID);
		_StoreMessageID.StoreValue(Keywordoperations.get_DataValue(TestData).get(1), _messageID);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return _messageID;
}

public static String getSaltString(int _NumLength) {
    String SALTCHARS = "1234567890";
    StringBuilder salt = new StringBuilder();
    Random rnd = new Random();
    while (salt.length() < _NumLength) { // length of the random string.
        int index = (int) (rnd.nextFloat() * SALTCHARS.length());
        salt.append(SALTCHARS.charAt(index));
    }
    String saltStr = salt.toString();
    return saltStr;
}

public static String _GetTRANSFER_OPTIONS_API(String TestData) throws Exception {
	String responseBody=null;
	try{
		RestAssured.baseURI = PropertyUtils.readProperty("HOST_NAME")+"/payments/";
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.headers("x-et-api-signature","12345");
		httpRequest.headers("x-et-api-signature-type","PAYLOAD_DIGEST_SHA256");
		httpRequest.headers("x-et-channel-indicator","ONLINE");
		httpRequest.headers("x-et-participant-id","12357");
		httpRequest.headers("x-et-participant-user-id",Keywordoperations.get_DataValue(TestData).get(4));
		httpRequest.headers("x-et-request-id","REQID"+getSaltString(5));
		httpRequest.headers("x-et-transaction-time","5-15-2018");
		httpRequest.headers("Content-Type","application/json");
		Response response = RestAssured.given().spec(httpRequest).when().request(io.restassured.http.Method.GET, APIOperations.ReplaceValueGETTransferOptions(Keywordoperations.get_DataValue(TestData).get(0), Keywordoperations.get_DataValue(TestData).get(1), Keywordoperations.get_DataValue(TestData).get(2)));
		responseBody = response.getBody().asString();
	}catch (Exception e) {
		e.printStackTrace();
	}
	return responseBody;
}


public static String GetCustomerAmount_API(String DOMESTIC_URL,String CURRENCY_TYPE) throws Exception{
	String _URI = PropertyUtils.readProperty("GetCustomerAmounts_API");
	String finalstring = _URI
			.replace("DOMESTIC_URL", DOMESTIC_URL)
			.replace("CURRENCY_TYPE", CURRENCY_TYPE);
	return finalstring;
}

public static String _getCustomerAmounts_API(String TestData) throws Exception {
	String responseBody=null;
	try{
		RestAssured.baseURI = PropertyUtils.readProperty("HOST_NAME")+"/customers/P00012/";
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.headers("x-et-api-signature","12345");
		httpRequest.headers("x-et-api-signature-type","PAYLOAD_DIGEST_SHA256");
		httpRequest.headers("x-et-channel-indicator","ONLINE");
		httpRequest.headers("x-et-participant-id","12357");
		httpRequest.headers("x-et-participant-user-id",Keywordoperations.get_DataValue(TestData).get(3));
		httpRequest.headers("x-et-request-id","REQID"+getSaltString(5));
		httpRequest.headers("x-et-transaction-time","5-15-2018");
		httpRequest.headers("Content-Type","application/json");
		Response response = RestAssured.given().spec(httpRequest).when().request(io.restassured.http.Method.GET, GetCustomerAmount_API(Keywordoperations.get_DataValue(TestData).get(1), Keywordoperations.get_DataValue(TestData).get(2)));
		responseBody = response.getBody().asString();
	}catch (Exception e) {
		e.printStackTrace();
	}
	return responseBody;
}

public static String _getCustomeRegistration_API(String TestData) throws Exception {
	String responseBody=null;
	String ResponseCode=null;
	try{
		RestAssured.baseURI = PropertyUtils.readProperty("HOST_NAME")+"/customers/P00012/";
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.headers("x-et-api-signature","12345");
		httpRequest.headers("x-et-api-signature-type","PAYLOAD_DIGEST_SHA256");
		httpRequest.headers("x-et-channel-indicator","ONLINE");
		httpRequest.headers("x-et-participant-id","CA000001");
		httpRequest.headers("x-et-participant-user-id",Keywordoperations.get_DataValue(TestData).get(1));
		httpRequest.headers("x-et-request-id","REQID"+getSaltString(5));
		httpRequest.headers("x-et-transaction-time","2014-09-12T20:53:23.234Z");
		httpRequest.headers("x-et-extsystem","CBX");
		httpRequest.headers("Content-Type","application/json");
		Response response = RestAssured.given().spec(httpRequest).when().request(io.restassured.http.Method.GET,"");
		System.out.println(response.getStatusCode());
		ResponseCode=Integer.toString(response.getStatusCode());
		responseBody = response.getBody().asString();
	}catch (Exception e) {
		e.printStackTrace();
	}
	return responseBody+":ResponseCode:"+ResponseCode;
}


public static Response GetResponse(String ReferrenceID, RequestSpecification httpRequest) {
	Response response = RestAssured.given().spec(httpRequest).when().request(io.restassured.http.Method.GET,
			getReferrenceID(ReferrenceID));
	return response;
}

public static String getReferrenceID(String ReferrenceID) {
	return ReferrenceID + "?pretty";
}

public static JsonPath GetJSONPathvalue(Response response) {
	JsonPath jsonPathEvaluator = response.jsonPath();
	return jsonPathEvaluator;
}

public static String GetMessageID(String getVal, JsonPath jsonPathEvaluator) {
	String _messageID;
	_messageID = jsonPathEvaluator.get(getVal);
	return _messageID;
}


public static String ReplaceValueGETTransferOptions(String DOMESTIC_URL,String ACCOUNT_DEPOSIT_URL,String deposit_handle_URL) throws Exception{
	String _URI = PropertyUtils.readProperty("GET_TRANSFER_OPTIONS_API_URL");
	String finalstring = _URI
			.replace("DOMESTIC_URL", DOMESTIC_URL)
			.replace("ACCOUNT_DEPOSIT_URL", ACCOUNT_DEPOSIT_URL)
			.replace("deposit_handle_URL", deposit_handle_URL);
	return finalstring;
}

/*public static String UPDATE_DEFAULT_VALUE_API(String TestData) throws Exception {		
	boolean flag_res = true;
	String prettyJsonString=null;
   try{
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setContentType("application/json; charset=UTF-8");
		RequestSpecification request = builder.build();
		
		String VAR_TEMPLATEID = Keywordoperations.get_DataValue(TestData).get(0);
		String VAR_CUSTOMERID = Keywordoperations.get_DataValue(TestData).get(1);
		String VAR_ERRORCODE = Keywordoperations.get_DataValue(TestData).get(2);

		Thread.sleep(1000); 
		    response = RestAssured.given().spec(request).when().post(PropertyUtils.readProperty("HOST_NAME") +""+ReplaceValueURI(VAR_TEMPLATEID, VAR_CUSTOMERID, VAR_ERRORCODE));
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(response.body().asString());
			prettyJsonString = gson.toJson(je);
			System.out.println(prettyJsonString);
   }catch (Exception e) {
	   e.printStackTrace();
}
return "Status_Code:"+response.getStatusCode()+"|Response:"+prettyJsonString; 
}*/


public static String _DateFormat() throws Exception {
	Date date = new Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	String dateAsISOString = df.format(date);
	System.out.println(dateAsISOString);
	return dateAsISOString;
}

//____________________________________________Send_Money___NotificationsAPI_________________________________________________________________________________


public static void TriggerSendMoneyNotifications(String TestData) throws Exception {
	FileUtilities FileUtilities=new  FileUtilities();
	String VAR_DATE=null;
	String PARTIAL_URL = Keywordoperations.get_DataValue(TestData).get(5);
	String PUID = Keywordoperations.get_DataValue(TestData).get(6);
	
	String FullFill_RTPAPI = PropertyUtils.readProperty("SendNotificationAPIURL");
	try {
		String VAR_AMOUNT = Keywordoperations.get_DataValue(TestData).get(0);
		
		VAR_DATE = Keywordoperations.get_DataValue(TestData).get(1);
		if (VAR_DATE.equalsIgnoreCase("date")) {
			VAR_DATE = _DateFormat();
		}else{
			VAR_DATE = Keywordoperations.get_DataValue(TestData).get(1);
		}
		System.out.println(FileUtilities.GetFilevalue("VAR_MESSAGESEQID_SUB"));
		
		String MESSAGE_ID = Keywordoperations.get_DataValue(TestData).get(2);
		String VAR_EVENT = Keywordoperations.get_DataValue(TestData).get(3);
		String VAR_STATUS = Keywordoperations.get_DataValue(TestData).get(4);
//		+PARTIAL_URL
		String REQUEST_ID = "GSPR_" + getSaltString("N", 5,MESSAGE_ID);
		System.out.println(REQUEST_ID);
		URL url = new URL(FullFill_RTPAPI);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("x-et-api-signature", "CA000003");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("x-et-channel-indicator", "ONLINE");
		conn.setRequestProperty("x-et-participant-id", "CA000010");
		conn.setRequestProperty("x-et-request-id",REQUEST_ID);
		conn.setRequestProperty("x-et-retry-indicator", "TRUE");
		conn.setRequestProperty("x-et-transaction-time",getSimpleDateFormat());
		conn.setRequestProperty("x-et-participant-user-id", PUID);
		conn.setRequestProperty("X-et-api-signature-type","PAYLOAD_DIGEST_SHA256");
		String input = ReplaceSendMoneyValueNotificationsJSON(VAR_AMOUNT,VAR_DATE,VAR_EVENT,VAR_STATUS);
		
		System.out.println(input);
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		System.out.println(conn.getResponseCode());
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		conn.disconnect();
	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}

public static String ReplaceSendMoneyValueNotificationsJSON(String VAR_AMOUNT,String VAR_DATE,String VAR_EVENT,String VAR_STATUS) throws Exception {
	String ReqPayload_Create_Domain = PropertyUtils.readProperty("SEND_MONEY_API");
	FileUtilities FileUtilities=new FileUtilities();
	String MessageID = 	FileUtilities.GetFilevalue("VAR_MESSAGESEQID_SUB");
	String finalstring = ReqPayload_Create_Domain
			.replace("VAR_AMOUNT", VAR_AMOUNT)
			.replace("VAR_DATE", VAR_DATE)
			.replace("VAR_EVENT", VAR_EVENT)
			.replace("VAR_STATUS", VAR_STATUS)
			.replace("VAR_MESSAGESEQID_SUB", MessageID);
	return finalstring;
}

public static void TriggerSendRTPNotifications(String TestData) throws Exception {
	FileUtilities FileUtilities=new  FileUtilities();
	String VAR_DATE=null;
	String PARTIAL_URL = Keywordoperations.get_DataValue(TestData).get(5);
	String PUID = Keywordoperations.get_DataValue(TestData).get(6);
	
	String FullFill_RTPAPI = PropertyUtils.readProperty("SendNotificationAPIURL");
	try {
		String VAR_AMOUNT = Keywordoperations.get_DataValue(TestData).get(0);
		
		VAR_DATE = Keywordoperations.get_DataValue(TestData).get(1);
		if (VAR_DATE.equalsIgnoreCase("date")) {
			VAR_DATE = _DateFormat();
		}else{
			VAR_DATE = Keywordoperations.get_DataValue(TestData).get(1);
		}
		System.out.println(FileUtilities.GetFilevalue("VAR_MESSAGESEQID_SUB"));
		
		String MESSAGE_ID = Keywordoperations.get_DataValue(TestData).get(2);
		String VAR_EVENT = Keywordoperations.get_DataValue(TestData).get(3);
		String VAR_STATUS = Keywordoperations.get_DataValue(TestData).get(4);
//		+PARTIAL_URL
		URL url = new URL(FullFill_RTPAPI);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("x-et-api-signature", "CA000003");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("x-et-channel-indicator", "ONLINE");
		conn.setRequestProperty("x-et-participant-id", "CA000010");
		conn.setRequestProperty("x-et-request-id","GSPR_" + getSaltString("N", 5,MESSAGE_ID));
		conn.setRequestProperty("x-et-retry-indicator", "TRUE");
		conn.setRequestProperty("x-et-transaction-time",getSimpleDateFormat());
		conn.setRequestProperty("x-et-participant-user-id", PUID);
		conn.setRequestProperty("X-et-api-signature-type","PAYLOAD_DIGEST_SHA256");
		String input = ReplaceValueNotificationsJSON(VAR_AMOUNT,VAR_DATE,VAR_EVENT,VAR_STATUS);
		
		System.out.println(input);
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		System.out.println(conn.getResponseCode());
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		conn.disconnect();
	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}	

public static String ReplaceValueNotificationsJSON(String VAR_AMOUNT,String VAR_DATE,String VAR_EVENT,String VAR_STATUS) throws Exception {
	String ReqPayload_Create_Domain = PropertyUtils.readProperty("SendNotificationAPI");
	FileUtilities FileUtilities=new FileUtilities();
	String MessageID = 	FileUtilities.GetFilevalue("VAR_MESSAGESEQID_SUB");
	String finalstring = ReqPayload_Create_Domain
			.replace("VAR_AMOUNT", VAR_AMOUNT)
			.replace("VAR_DATE", VAR_DATE)
			.replace("VAR_EVENT", VAR_EVENT)
			.replace("VAR_STATUS", VAR_STATUS)
			.replace("VAR_MESSAGESEQID_SUB", MessageID);
	return finalstring;
}

public static void main(String[] args) throws Exception {
//		POST_SENDMoneyNotificationAPI("NotificationAPI|7596282750399488371|00000222|ASH|VAR_REQUESTID");
//		GetTRANSFER_OPTIONS_API(PropertyUtils.readProperty("GET_TRANSFER_OPTIONS_API_URL"));
//		System.out.println(_GetTRANSFER_OPTIONS_API("GetTRANSFER_OPTIONS_API|DOMESTIC|ACCOUNT_DEPOSIT|6013000201|P00012"));
//		System.out.println(_getCustomerAmounts_API(""));
//		System.out.println(_getCustomerAmounts_API("GetCustomerAmounts|DOMESTIC|CAD|P00012"));
//		System.out.println(_getCustomeRegistration_API("getCustomerRegistration|P00021"));
//		System.out.println(UpdateSTATELESS_API("P00016|404|getTransferOptions| |P00016"));
//	String VAR_TEMPLATEID = Keywordoperations.get_DataValue(TestData).get(0);
//	String VAR_CUSTOMERID = Keywordoperations.get_DataValue(TestData).get(1);
//	String VAR_PARAM = Keywordoperations.get_DataValue(TestData).get(2);
//	String VAR_PARAMVALUE = Keywordoperations.get_DataValue(TestData).get(3);
//	String VAR_SEARCHPARAM = Keywordoperations.get_DataValue(TestData).get(4);
//	String VAR_SETHEADERS = Keywordoperations.get_DataValue(TestData).get(5);

//		UpdateSTATELESSinteracParamOveride_API("getCustomerAmounts1|PU0001|PARAM01|404|333|XYZ");
//	System.out.println(PropertyUtils.readProperty("HOST_NAME")+"/customers/P00012/");
	}
}
