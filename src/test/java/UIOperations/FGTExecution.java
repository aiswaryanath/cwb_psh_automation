package UIOperations;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;


import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import FGT.Data_MigrationDB;
import FGT.FGTDataRead;
import FGT.DIGI_FileUpload;
import FGT.H2HB_FileUpload;
import FGT.H2HC_FileUpload;
import FGT.SCA_FileUpload;
import Logger.LoggerUtils;
import ReportGeneration.kiosk;
import Utilities.DBConnector;
import Utilities.FileUtilities;
import Utilities.NoFilewORKITEMIDException;
import Utilities.PropertyUtils;
import Utilities.XmlParserDOTRDFTR2;
import file_Driver.SFTP_PSH_exe;

public class FGTExecution {
	static Logger logger = LoggerUtils.getLogger();
	public static WebDriver driver=null;

     
	public static void executeFGT(String sufix,String Modular,String SHEETNAME,String Format,String sqlmatch) throws Exception
	{
		try
		{
			/*
			
				*/
				System.out.println("SFTP Files are processing..UPLOADING TO SERVER...please wait!!");
		
				FGTDataRead.FGTfolderdata("CWB_BVM");
			
				System.out.println("files uploading from H2HC folder");
				logger.info("files uploading from H2HC folder");
				H2HC_FileUpload.File_Operation_H2HC();
	
			
				System.out.println("files uploading from H2HB folder");
				logger.info("files uploading from H2HB folder");
				H2HB_FileUpload.File_Operation_H2HB();
				
				System.out.println("files uploading from DIGI folder");
				logger.info("files uploading from DIGI folder");
				DIGI_FileUpload.File_Operation_DIGI();
				System.out.println("files uploaded");
				logger.info("files uploaded");
				Thread.sleep(9000);
				
				
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
	
	}
	
	public static String GetFilename(String Testcaseid) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String file_name = null;
		String finalfielname=null;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String testcaseid= FileUtilities.GetFilevalue("VAR_TCNO");
		String formatname=null;
		//Connection connection=null;
		Statement stmt=null;
		System.out.println("testcaseid:"+testcaseid);
		logger.info("testcaseid:"+testcaseid);
		try{
			
			Properties systemProperties = System.getProperties();
			kiosk k=new kiosk();
			String suite=FileUtilities.GetFilevalueglobal("db_suite");
			String query = "select * from test_tool_file_name where testcaseid ='"+testcaseid+"'"+" and suite_name like '"+suite+"'";
			
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			stmt = connection.createStatement();
			
			stmt.executeUpdate(query);
			ResultSet _ResultSet=connection.createStatement().executeQuery(query);
		
	
		  while (_ResultSet.next()) {

			  file_name = _ResultSet.getString("FILENAME");
			  formatname=_ResultSet.getString("FORMATNAME");
				System.out.println("File name from test_tool_file_name:"+file_name);
				Result1.put("FILE_NAME",file_name);
				finalfielname=replace( file_name,formatname);
				System.out.println("File name after parsing:"+ finalfielname);
				_Storevalue.StoreValue("VAR_FILENAME",finalfielname);

			}
		  String fileworkitemid = "Select WORKITEMID from file_proc_opn where file_name like '"+finalfielname+"'";
		  String workitemid;
		  stmt.executeUpdate(fileworkitemid);
			ResultSet _ResultSet1=connection.createStatement().executeQuery(fileworkitemid);
			  System.out.println("TILL HERE");
			while (_ResultSet1.next()) {
				  System.out.println("TILL HERE2");
				   workitemid = _ResultSet1.getString("WORKITEMID");
					_Storevalue.StoreValue("VAR_PSHFILEWORKITEMID",workitemid);
					  System.out.println("Fileworkitemid:"+workitemid);
					  logger.info("Fileworkitemid:"+workitemid);
				}
		

		}catch (Exception e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		}
		
		return file_name;
	}
	
	public static HashMap<String, String> GetFilenamethroughTestData(String Testcaseid) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		HashMap<String,String> Result1=new HashMap<String,String>();   
		Keywordoperations _Storevalue=new Keywordoperations();
		String file_name;
		String finalfielname=null;
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		//String testcaseid= FileUtilities.GetFilevalue("VAR_TCNO");
		String testcaseid = Testcaseid;
		String formatname=null;
		//Connection connection=null;
		Statement stmt=null;
		System.out.println("testcaseid:"+testcaseid);
		try{
			String query = "select * from test_tool_file_name where testcaseid ='"+testcaseid+"'";
			
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			stmt = connection.createStatement();
			
			stmt.executeUpdate(query);
			ResultSet _ResultSet=connection.createStatement().executeQuery(query);
		
	
		  while (_ResultSet.next()) {

			  file_name = _ResultSet.getString("FILENAME");
			  formatname=_ResultSet.getString("FORMATNAME");
			  
				Result1.put("FILE_NAME",file_name);
				finalfielname=replace( file_name,formatname);
				System.out.println("Select query executed Successfully with FILE_NAME:"+ finalfielname);
				_Storevalue.StoreValue("VAR_FILENAME",finalfielname);

			}
		  String fileworkitemid = "Select WORKITEMID from file_proc_opn where file_name like '"+finalfielname+"'";
		  String workitemid;
		  stmt.executeUpdate(fileworkitemid);
			ResultSet _ResultSet1=connection.createStatement().executeQuery(fileworkitemid);
			  System.out.println("TILL HERE");
			while (_ResultSet1.next()) {
				  System.out.println("TILL HERE2");
				   workitemid = _ResultSet1.getString("WORKITEMID");
					_Storevalue.StoreValue("VAR_PSHFILEWORKITEMID",workitemid);
					  System.out.println("Fileworkitemid:"+workitemid);
				}
		

		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Result1;
	}
	
	public static String GetFileWorkitemid() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		
		Keywordoperations _Storevalue=new Keywordoperations();
	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String filename= FileUtilities.GetFilevalue("VAR_FILENAME");
		 String workitemid;
		 String filestatus;
		//Connection connection=null;
		Statement stmt=null;

		try{
			String query= "Select WORKITEMID,FILE_STATUS from file_proc_opn where file_name like '"+filename+'%'+"' order by WORKITEMID desc";
			System.out.println(query);
			logger.info(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			stmt = connection.createStatement();

			  stmt.executeUpdate(query);
				ResultSet _ResultSet1=connection.createStatement().executeQuery(query);
				//  System.out.println("resultset value:"+_ResultSet1);
				  int count=0;
				   StringBuilder fw = null;
				   StringBuilder ki = null;
			
					  System.out.println("resultset value:true");
						logger.info("resultset value:true");
				if (_ResultSet1.next()) {
					  System.out.println("TILL HERE2");
					   workitemid = _ResultSet1.getString("WORKITEMID");
					   filestatus=_ResultSet1.getString("FILE_STATUS");
					   if(count==0)
					   {  
						fw=new StringBuilder(workitemid);
						ki=new StringBuilder(filestatus);
						//_Storevalue.StoreValue("VAR_PSHFILEWORKITEMID",workitemid);
					   }
					   else
					   {
						   fw=fw.append(',').append(workitemid);	
						   ki=ki.append(',').append(filestatus);
						   
						 //  _Storevalue.StoreValue("VAR_PSHFILEWORKITEMID"+count,workitemid);   
					   }
						  System.out.println("Fileworkitemid:"+workitemid);
						  logger.info("Fileworkitemid:"+workitemid);
						  count=count+1;
					}
		  
				_Storevalue.StoreValue("VAR_PSHFILEWORKITEMID",fw.toString());
				_Storevalue.StoreValue("VAR_FILE_STATUS",ki.toString());
				_Storevalue.StoreValue("VAR_PSHFILEWORKITEMIDLIST",fw.toString());
				return "PASS";
				   
			
			

		}catch (Exception e) {
			 throw new NoFilewORKITEMIDException("Fileworkitemid not available in system ");  
		
		}

	}
	
	public static void GetallFileWorkitemids() throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		
		Keywordoperations _Storevalue=new Keywordoperations();
	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String filename= FileUtilities.GetFilevalue("VAR_FILENAME");
		 String workitemids = "";
		//Connection connection=null;
		Statement stmt=null;

		try{
			String query= "Select WORKITEMID from file_proc_opn where file_name like '"+filename+'%'+"'";
			System.out.println(query);
			logger.info(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			stmt = connection.createStatement();

			  stmt.executeUpdate(query);
				ResultSet _ResultSet1=connection.createStatement().executeQuery(query);
				  System.out.println("TILL HERE");
				while (_ResultSet1.next()) {
					  System.out.println("TILL HERE2");
					   workitemids = workitemids+ " "+_ResultSet1.getString("WORKITEMID");
						_Storevalue.StoreValue("VAR_PSHFILEWORKITEMID",workitemids);
						  System.out.println("Fileworkitemid:"+workitemids);
						  logger.info("Fileworkitemid:"+workitemids);
					}
		  
		

		}catch (Exception e) {
			e.printStackTrace();
		}

	}
	void deleteDir(File file) {
	    File[] contents = file.listFiles();
	    if (contents != null) {
	        for (File f : contents) {
	            deleteDir(f);
	        }
	    }
	    file.delete();
	}
	public static void main(String[] args) throws Exception {
		//deletefromserver();
		//Data_MigrationDB.Delete_ExistingDataEnv1("Bulke%");
		//executeFGT("Sanity Suite","No","Sanity_FGT_2.0.xls","PAIN 001_Remit_check","SANITY_TC%");
	//	runDataMigration("SANITY_TC%");
		//GetCurrentFileWorkitemid("ANSIxSC11xP003");
		//Data_MigrationDB.Source_DestinationDBMigration("Bulke%");
		//Data_MigrationDB.Execute_QueriesDestination();
        GetFilename("POSITIVE_FV_VAL-13022-01_TC0001");
        //GetFileWorkitemid();
		//String filename=h.get("FILE_NAME");
		//String result = filename.substring(0, filename.lastIndexOf("."));
		//System.out.println(result);
	}
	
	public void runFGT(String Testdata) throws Exception {

        String suite_name="Sanity Suite";
        String modular="No";
        String Excelsheet="BulkE2E_test_FGT_4.0.xls";
        String Formatname="ALL";
        String testcaseid="Bulk";
     
		if (Testdata != null) {
			String[] Data = Testdata.split("\\|");
		
			suite_name =  Data[0];
			modular = Data[1];
			Excelsheet = Data[2];
			Formatname = Data[3];
			testcaseid = Data[4];
		}
		deletefromserver();
		Data_MigrationDB.Delete_ExistingDataEnv1(testcaseid);
		executeFGT(suite_name,modular,Excelsheet,Formatname,testcaseid);
        }
      

	
	public static String runDataMigration(String Testdata) throws Exception {
	
		//Data_MigrationDB.Source_DestinationDBMigration(Testdata);
		Data_MigrationDB.Execute_QueriesDestination(Testdata);
		
		DBConnector fe= new DBConnector();
        fe.PREExecute_QueriesCC2("test_tool_file_name.sql");
	

		return Testdata;
	
	}
	public static void deletefromserver() throws IOException, JSchException
	{
		String hostname = PropertyUtils.readProperty("FGT_HOSTNAME");
		String login = PropertyUtils.readProperty("FGT_LOGIN");
		String password = PropertyUtils.readProperty("FGT_PWD");

		String directory = "/usr1/SIR13310/GTB_HOME/IPSH_HOME/filearea/Output_Files/";
		// String directory = ServerPath;

		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		Session session;
		session = ssh.getSession(login, hostname, 22);
		session.setConfig(config);
		session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
		session.setPassword(password);
		try
		{
			// GETTING sESSION FROM SERVER

			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			if (channel.isConnected())
			{
				System.out.println("Connected!!");

				recursiveFolderDelete(channel, directory);

				/*
				 * File f = new File(directory);
				 * System.out.println(f.getPath()); File[] files =
				 * f.listFiles((FileFilter) FileFileFilter.FILE); if (files ==
				 * null) { System.out.println("bad luck"); }
				 * 
				 * // System.out.println(files.toString());
				 * System.out.println("in here 0"); for (int i = 0; i <=
				 * f.length(); i++) { System.out.println(files[i].getName());
				 * files[i].delete(); }
				 */
				session.disconnect();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			session.disconnect();
		}

	}

	private static void recursiveFolderDelete(ChannelSftp channelSftp, String path) throws SftpException
	{

		// List source directory structure.
		Collection<ChannelSftp.LsEntry> fileAndFolderList = channelSftp.ls(path);

		// Iterate objects in the list to get file/folder names.
		for (ChannelSftp.LsEntry item : fileAndFolderList)
		{
			if (!item.getAttrs().isDir())
			{
				channelSftp.rm(path + "/" + item.getFilename()); // Remove file.
			}
			else if (!(".".equals(item.getFilename()) || "..".equals(item.getFilename())))
			{ // If it is a subdir.
				try
				{
					// removing sub directory.
					channelSftp.rmdir(path + "/" + item.getFilename());
				}
				catch (Exception e)
				{ // If subdir is not empty and error occurs.
				  // Do lsFolderRemove on this subdir to enter it and clear
				  // its contents.
					recursiveFolderDelete(channelSftp, path + "/" + item.getFilename());
				}
			}
		}
		// channelSftp.rmdir(path); // delete the parent directory after empty
	
	}
	
	public static String replace(String filename, String formatName)
			throws InterruptedException {
		logger.info("----Enter in replace----");
		String fname = null;
		if (filename != null) {

			try {
				String file_name = filename.replace("tmp", "");

				String str[] = file_name.split("\\.");
				String finalname = str[0];
				for (int i = 0; i < str.length; i++) {
					String tmp = str[i];
					if (i > 0) {
						finalname = finalname + "." + tmp;
						// START
						if (filename.contains(".P.dat")
								|| filename.contains(".P.xml")
								|| filename.contains(".T.dat")
								|| filename.contains(".T.xml")) {

							if ("dat".equalsIgnoreCase(str[i])
									|| "xml".equalsIgnoreCase(str[i])) {

								break;

							}
						} else {

							if ("1464".equalsIgnoreCase(str[i])
									|| "C1464".equalsIgnoreCase(str[i])
									|| "E1464".equalsIgnoreCase(str[i])
									|| "0094".equalsIgnoreCase(str[i])
									|| "I0094".equalsIgnoreCase(str[i])
									|| "E0094".equalsIgnoreCase(str[i])
									|| "0080".equalsIgnoreCase(str[i])) {

								break;

							}
						}

					}
				}
				// END
				if (formatName.contains("CPA")) {

					fname = finalname.replace(".1464", ".E1464");

				} else if (filename.contains(".1464")
						&& !formatName.contains(".CPA")) {
					fname = finalname.replace(".1464", ".C1464");

				}
				
		

				else {
					fname = finalname;

				}
				if(filename.startsWith("DATAELEC")||filename.startsWith("DATAELUS"))
				{	
					System.out.println("entering inbound");
					String dataname[]=filename.split("\\.");
					fname=dataname[0]+".C"+dataname[1];
				}
			} catch (Exception e) {
				logger.fatal("Exception in replace", e);
				e.printStackTrace();
			}
		}

		logger.info("fname---" + fname);
		logger.info("----exit from replace----");
		return fname;
	}

	public static void GetCurrentFileWorkitemid(String cust_ref_num) throws Exception {
FileUtilities FileUtilities=new FileUtilities();
		
		Keywordoperations _Storevalue=new Keywordoperations();
	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String filename= FileUtilities.GetFilevalue("VAR_FILENAME");
		 String workitemid= FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMIDLIST");
		//Connection connection=null;
		Statement stmt=null;

		try{
			String query= "Select file_workitem_id from txn_proc_opn where file_workitem_id in"+"("+workitemid+")"+" and cust_ref_num="+"'"+cust_ref_num+"'";
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			stmt = connection.createStatement();

			  stmt.executeUpdate(query);
				ResultSet _ResultSet1=connection.createStatement().executeQuery(query);
				  System.out.println("TILL HERE");
				while (_ResultSet1.next()) {
					  System.out.println("TILL HERE2");
					   workitemid = _ResultSet1.getString("file_workitem_id");
		
						_Storevalue.StoreValue("VAR_PSHFILEWORKITEMID",workitemid);
					
	
						  System.out.println("Fileworkitemid:"+workitemid);
						
					}
		  

		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	
}
