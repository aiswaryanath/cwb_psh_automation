/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena aLtd. All rights reserved */
/* */
/************************************************************************/
/* Application : PSH Integration Automation	*/
/* Module Name : FTS_FileUpload.java */
/* */
/* File Name : FTS_FileUpload.java */
/* */
/* Description : Used to Upload FTS files*/
/* */
/* Author : Varun Paranganath */
/****************************************************************************************************/

package file_Driver;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

//import org.CIBC_Automation.driver.CIBC_XPATH_Constants;
//import org.CIBC_Automation.functionlib.Utilities;
import org.apache.log4j.Logger;
import Logger.LoggerUtils;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import Utilities.PropertyUtils;

public class SFTP_FileUpload
{

	public static ArrayList<String>	FTS_array		= null;
	public static ArrayList<String>	FTS_Filename	= null;
	static Logger log = LoggerUtils.getLogger();
	public static void File_Operation_SFTPUpload() throws Exception
	{
	
		String filepath=System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\ACH";
		//File directory = new File("D:/PSH_Automation/LOCAL_PAYMENT_FILE/ACH/");
		File directory = new File(filepath);
		File[] file_list = directory.listFiles();
		for (File files : file_list)
		{
			System.out.println(files.getAbsolutePath() + "   " + files.getName());
			log.info(files.getAbsolutePath() + "   " + files.getName());
			UPLoad_ToFilesServer(files.getAbsolutePath(), files.getName());
		}
	}

	public static void File_Operation_SFTPUploadEFT() throws Exception
	{
		String filepath=System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\EFT";
		//File directory = new File("D:/PSH_Automation/LOCAL_PAYMENT_FILE/EFT/");
		File directory = new File(filepath);
		File[] file_list = directory.listFiles();
		for (File files : file_list)
		{
			System.out.println(files.getAbsolutePath() + "" + files.getName());
			log.info(files.getAbsolutePath() + "" + files.getName());
			UPLoad_ToFilesServer(files.getAbsolutePath(), files.getName());
		}
	}

	public static void UtilitySFTPUpload(String Origin, String Destination) throws Exception
	{
		// File directory = new
		// File("D:/PSH_Automation/LOCAL_PAYMENT_FILE/ACH/");
		File directory = new File(Origin);
		File[] file_list = directory.listFiles();
		for (File files : file_list)
		{
			System.out.println(files.getAbsolutePath() + "" + files.getName());
			log.info(files.getAbsolutePath() + "" + files.getName());
			SFTPUPLoad_ToFilesServer(files.getAbsolutePath(), files.getName(), Destination);
		}
	}

	public static void SFTPUPLoad_ToFilesServer(String FILE_PATH, String FILE_NAME, String Destination) throws Exception
	{
		// String hostname = "10.10.8.62";
		// String login = "SIR18360";
		// String password = "PSHRetro@Joyo123";
		//String hostname = Utilities.getProp(CIBC_XPATH_Constants.Env2_HOST_NAME, CIBC_XPATH_Constants.CONFIGPATH);
		//String login = Utilities.getProp(CIBC_XPATH_Constants.Env2_LOGIN, CIBC_XPATH_Constants.CONFIGPATH);
		//String password = Utilities.getProp(CIBC_XPATH_Constants.Env2_PASSWORD, CIBC_XPATH_Constants.CONFIGPATH);
		String hostname =  PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password =  PropertyUtils.readProperty("PSHAPP_Password");
		
		// String directory =
		// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/incomingfiles/";
		String directory = Destination;
	

		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
		if(envdetail.contains("CLOUD"))
		{	
		String ppk=	PropertyUtils.readProperty("ENV_PPK");
		//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
		String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
		ssh.addIdentity(privateKey);
		}
		Session session;

		try
		{
			// Getting Session Of Server LINUX
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			Channel sftp = channel;
			((ChannelSftp) sftp).cd(directory);

			File file = new File(FILE_PATH);
			FileInputStream fileinputstream = new FileInputStream(file);
			channel.put(fileinputstream, file.getName());

			// channel.chmod(0777, directory + "/" + FILE_NAME);
			// channel.rename(directory + "/" + FILE_NAME,directory + "/" +
			// FILE_NAME.substring(0, FILE_NAME.length() - 4));
			Thread.sleep(20000);

			channel.disconnect();
			session.disconnect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.debug(e.getMessage());
		}
	}

	public static void UPLoad_ToFilesServer(String FILE_PATH, String FILE_NAME) throws Exception
	{
		//String hostname = "10.10.8.62";
		//String login = "SIR18360";
		//String password = "PSHRetro@Joyo123";
		//String directory = "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/incomingfiles/";
		String hostname =  PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password =  PropertyUtils.readProperty("PSHAPP_Password");
		String directory = PropertyUtils.readProperty("strRebulkPathTOSTUB");
		
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
		if(envdetail.contains("CLOUD"))
		{	
		String ppk=	PropertyUtils.readProperty("ENV_PPK");
		
		//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
		String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
		ssh.addIdentity(privateKey);
		}
		Session session;
		

		try
		{
			// Getting Session Of Server LINUX
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			Channel sftp = channel;
			((ChannelSftp) sftp).cd(directory);

			File file = new File(FILE_PATH);
			FileInputStream fileinputstream = new FileInputStream(file);
			channel.put(fileinputstream, file.getName());

			// channel.chmod(0777, directory + "/" + FILE_NAME);
			// channel.rename(directory + "/" + FILE_NAME,directory + "/" +
			// FILE_NAME.substring(0, FILE_NAME.length() - 4));
			Thread.sleep(20000);

			channel.disconnect();
			session.disconnect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.debug(e.getMessage());
		}
	}


	public static void main(String[] args) throws Exception
	{
	//	SFTP_FileUpload.UtilitySFTPUpload("D:/PSH_Automation/LOCAL_PAYMENT_FILE/CNR/ISO_REMIT",
	//	        "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/incomingfiles");
		
		UtilitySFTPUpload("D:\\workspace\\CWB_IntegratedAutomation\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\DIGI",
		        "/usr1/SIR22680/GTB_HOME/IPSH_HOME/filearea/SPLITTING/DIGITAL/incoming");

	}
}
