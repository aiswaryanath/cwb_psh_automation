package AutoIt;


import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AutoIt_Test {
 WebDriver driver;
 
 @BeforeTest
 public void setup() throws Exception {
	 ChromeOptions options = new ChromeOptions();
		options.setProxy(null);
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
	    driver = new ChromeDriver(options);
			driver.get("http://www.ox.ac.uk/admissions/postgraduate_courses/apply/application_forms.html");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();

 }
 
 @Test
 public void testCaseOne_Test_One() throws IOException, InterruptedException {
	 String href=driver.findElement(By.xpath(".//*[@id='aapplications_for_recognised_student_status']/div/ul/li[1]/a")).getAttribute("href");
	   
	   //Framing the command string which includes two parameters
	   //Parameter 1-  Location where the Download.exe file is located
	   //Parameter 2 - file location which we have stored in "href" variable 
	   String command="Location where Download.exe file is saved"+" "+href;
	   //If you happened to save Download.exe file  at "Public downloads" folder then command statement will be like 
	      //String command="\"C:/Users/Public/Documents/Download.exe\""+" "+href;
	   
	      System.out.println("command is "+command);
	      ArrayList argList = new ArrayList();
	      argList.add(href);
	      
	      //Running the windows command from Java
	      Runtime.getRuntime().exec(command);     
	  }
}