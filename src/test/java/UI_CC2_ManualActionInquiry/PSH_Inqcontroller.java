package UI_CC2_ManualActionInquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.IAssert;

import com.codoid.products.exception.FilloException;

import Logger.LoggerUtils;
import Utilities.DBConnector;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;

public class PSH_Inqcontroller {

	static String BPS_window;

	static FileUtilities Fileutil = new FileUtilities();
	static PropertyUtils Prop = new PropertyUtils();
	static Logger log = LoggerUtils.getLogger();

	public static void SearchWorkitemID(WebDriver driver, String TestData, String xPathNav) throws Exception {
		try {

			String Workitemid = Fileutil.GetFilevalue(TestData);
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

			/*
			 * WebElement Search_WitemId = driver .findElement(By.
			 * xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-23]"
			 * ));
			 */
			WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
//ASH 25
			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			// WebElement ele =
			// driver.findElement(By.xpath("//*[@class='x-menu-item
			// x-menu-item-arrow']"));
			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(5000);
			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));

			Thread.sleep(3000);
			/*
			 * driver.findElement(By.xpath(
			 * "(//*[@class='x-menu-list-item'])[last()-0]")).sendKeys(Keys.
			 * ENTER); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("//*[@class='x-menu-item x-menu-item-arrow']")));
			 * Thread.sleep(3000); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));
			 * Thread.sleep(3000);
			 */
			driver.findElement(By.xpath("(//*[contains(text(),'" + Workitemid + "')])[last()-1]")).click();
			driver.switchTo().defaultContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void SearchWorkitemIDEFT(WebDriver driver, String TestData, String xPathNav) throws Exception {
		try {

			String Workitemid = Fileutil.GetFilevalue(TestData);
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

			/*
			 * WebElement Search_WitemId = driver .findElement(By.
			 * xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-23]"
			 * ));
			 */
			WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-27]")));
			
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				//	driver.findElement(By.xpath("(//*[@id='ext-gen181']")));
			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			// WebElement ele =
			// driver.findElement(By.xpath("//*[@class='x-menu-item
			// x-menu-item-arrow']"));
			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(5000);
			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));

			Thread.sleep(3000);
			/*
			 * driver.findElement(By.xpath(
			 * "(//*[@class='x-menu-list-item'])[last()-0]")).sendKeys(Keys.
			 * ENTER); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("//*[@class='x-menu-item x-menu-item-arrow']")));
			 * Thread.sleep(3000); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));
			 * Thread.sleep(3000);
			 */
			driver.findElement(By.xpath("(//*[contains(text(),'" + Workitemid + "')])[last()-1]")).click();
			driver.switchTo().defaultContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void SearchWorkitemIDActionClick(WebDriver driver, String TestData, String XpathNav)
			throws Exception {
		try {

			String Workitemid = Fileutil.GetFilevalue(TestData);
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

			WebElement Search_WitemId = driver.findElement(By.xpath(XpathNav));
			/*
			 * WebElement Search_WitemId = driver .findElement(By.
			 * xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-23]"
			 * ));
			 */
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));

			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();

			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();

			Thread.sleep(3000);
			// driver.findElement(By.xpath("(//*[contains(text(),'"+Workitemid+"')])[last()-1]")).click();
			Actions actions = new Actions(driver);
			actions.moveToElement(
					driver.findElement(By.xpath("//*[@class='x-grid3-cell-inner x-grid3-col-viewWorkItemID']//img[1]")))
					.click().perform();

			driver.switchTo().defaultContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void ExecutePSH_InquirySystem(WebDriver driver, String Credentials) throws FilloException, Exception {
		

		PSH_Login.Login_Execute(driver, Credentials);

		if (Credentials.equalsIgnoreCase("BCCMKR1")) {
			System.out.println("Maker Script Begins");
			
		}
	}

	

	public static void SearchClientFileID(WebDriver driver, String TestData, String xPathNav) throws Exception {
		String Action = null;
		String Remarks = "dnd";
		HashMap<String, String> h = new HashMap();
		try {
			int length;
			if (TestData != null) {
				String[] Data = TestData.split("\\|");
				length = Data.length;

				Action = Data[0];
				Remarks = Data[1];
				for (int i = 2; i < length; i++) {
					h.put(Data[i], Data[i]);
				}
			}
			DBConnector db = new DBConnector();
			db.GetpshFileId();
			Thread.sleep(5000);
			String Workitemid = Fileutil.GetFilevalue("VAR_PSHFILEID");
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

		
			WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));

			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));

			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(5000);
			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-6]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();

			Thread.sleep(3000);

			List<WebElement> we = driver.findElements(By.className("x-grid3-row-checker"));
			Actions actions = new Actions(driver);
			for (WebElement w : we)

			{

				System.out.println(w.toString());

				actions.moveToElement(w);
				Thread.sleep(1000);

				actions.click().perform();

			}
			driver.findElement(By.className("x-grid3-hd-checker")).click();
			driver.findElement(By.xpath("//button[contains(text(), 'Bulk Action')]")).click();
			//boolean b = isDialogPresent(driver);

			Nfocusonwindow(driver, "IntellectFlow");

			Thread.sleep(15000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("IwMiddleFrame");
			WebElement elem = driver.findElement(By.id("P162943174-525099944"));
			elem.click();

			((JavascriptExecutor) driver).executeScript("arguments[0].value='" + Remarks + "';",
					driver.findElement(By.id("P162943174-525099944")));

			driver.switchTo().defaultContent();
			Thread.sleep(6000);

			driver.switchTo().frame("IwBottomFrame");
			
			if (!Action.equals("Authorize")) {
				WebElement butons = driver.findElement(By.xpath("//td/a/span[text()='" + Action + "']"));
				butons.click();

			} else 
			{
				WebElement authorize = driver.findElement(By.id("P162943174-542545414"));
				authorize.click();
			}
			Thread.sleep(5500);
			String message = driver.switchTo().alert().getText();
			driver.switchTo().alert().accept();
			System.out.println(message);
			driver.switchTo().defaultContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean Nfocusonwindow(WebDriver driver, String title) {
		boolean flg = true;
		try
		{

			System.out.println("----Entering Nfocusonwindow----");

			System.out.println("current window:-->" + driver.getTitle());
			if (!title.equalsIgnoreCase(driver.getTitle())) 
			{
				System.out.println("searching for :-->" + title);
				Set<String> window = driver.getWindowHandles();
				System.out.println("total no of windows:-->" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{

					driver.switchTo().window(itr.next());
					System.out.println("Current window:  " + driver.getTitle());
					if (title.equalsIgnoreCase(driver.getTitle())) 
					{
						System.out.println("Correct Window");
						break;
					}

				}

			} 
			else 
			{
				System.out.println("You are on right window");
			}
			if ("Internet Explorer cannot display the webpage".equalsIgnoreCase(driver.getTitle())) 
			{
				driver.close();
				Set<String> window = driver.getWindowHandles();
				System.out.println("total no of windows:-->" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{
					System.out.println("I am here 1");
					driver.switchTo().window(itr.next());
				}
				flg = false;
			}

		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		return flg;
	}

	static boolean isDialogPresent(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (wait.until(ExpectedConditions.alertIsPresent()) == null)

		{
			System.out.println("alert was not present");

			return true;
		} else 
		{
			System.out.println("alert was present:" + driver.switchTo().alert().getText());
			driver.switchTo().alert().accept();
			return false;
		}
	}
}
