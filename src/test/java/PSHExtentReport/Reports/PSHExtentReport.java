package PSHExtentReport.Reports;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Category;
import org.apache.log4j.Logger; 
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
 
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;


/**
 * Unit test for simple App.
 */
public class PSHExtentReport 

{

		private static ExtentReports extent;
		static ExtentTest logger;
		public static synchronized ExtentReports GetExtent(String suite,String config) throws IOException {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
			String dateAsISOString = df.format(date);
			File dir1 = new File(System.getProperty("user.dir") +"\\src\\test\\resources\\Reports\\"+"STMExtentReport.html");
			File dir2 = new File(System.getProperty("user.dir") +"\\src\\test\\resources\\Reports\\backup\\"+dateAsISOString);
		
			dir2.mkdir();
		
			FileUtils.copyFileToDirectory(dir1, dir2);
		    if (extent != null) return extent;

		    extent = new ExtentReports (System.getProperty("user.dir") +"\\src\\test\\resources\\Reports\\STMExtentReport.html", true);
			//extent.addSystemInfo("Environment","Environment Name")
			extent
	                .addSystemInfo("Suite Name", suite)
	                .addSystemInfo("Environment", config)
	                .addSystemInfo("User Name", "Aishwarya Nath");
			extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		    return extent;
		    
		}
		 @BeforeClass
		    public void setup(){
		     
		    }
	@Test
	@Parameters ({"SUITE", "CONFIG"})
		public static void startPSH(String suite,String config) throws Exception{


		    System.out.println("Current selection suite and env:"+suite+"  "+config);
			Connection conn = null;
		    extent = GetExtent(suite,config);
		    try
			{
			Fillo fill=new Fillo();
		
		conn=fill.getConnection(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls");
			Recordset rs=conn.executeQuery("Select * from Main_Controller where RunStatus ='Yes'");
			while (rs.next()) {
					String _TestCases=rs.getField("TestCases");
					
			
					System.out.println("Testcases started:"+_TestCases);
					
					logger = extent.startTest(_TestCases);
					try
					{
					Recordset rs1=conn.executeQuery("Select * from PSH where TestCase ='"+_TestCases+"'");
				
					while (rs1.next())
					{
						String Resultflag=rs1.getField("RESULT");
						String KeywordOperation=rs1.getField("KeywordOperation");
						String StackStraceflag=rs1.getField("STACKTRACE");
						if(Resultflag.equalsIgnoreCase("PASS"))
						{
							logger.log(LogStatus.PASS,KeywordOperation, StackStraceflag);
						}
						if(Resultflag.equalsIgnoreCase("FAIL"))
						{
							logger.log(LogStatus.FAIL, KeywordOperation, StackStraceflag);
						}
						if(Resultflag.equalsIgnoreCase("SKIPPED"))
						{
							logger.log(LogStatus.SKIP,KeywordOperation,StackStraceflag);
						}
						extent.endTest(logger);
					}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					}
		
		
		
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally{
				  extent.flush();
				    extent.close();
					
		}
		    conn.close();
	}
	
	 public static void main (String args[]) throws Exception 
	   { 
		 startPSH("SUITE-CWB-BVM","AUTOMATION");
	   
	   }

}
