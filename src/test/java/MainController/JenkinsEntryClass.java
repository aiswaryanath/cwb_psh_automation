package MainController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Category;
import org.apache.log4j.Logger; 
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Logger.LoggerUtils;
import Modules.Modules;
import UIOperations.Keywordoperations;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;

public class JenkinsEntryClass {
	/*List of SUITE VALUE(USE ONLY ANY 1)=SUITE-BBD,SUITE-BTRBVM,SUITE-BTRE2E,SUITE-BULKBVM,SUITE-BULKE2E,SUITE-CC1AW,SUITE-CC1CTR,
			SUITE-CC1E2E-INT,SUITE-CC1GOW,SUITE-CC2BVM,SUITE-CC2E2E,SUITE-FILEBATCHBVM,SUITE-PHASE2,SUITE-SANITY
			
		17925,17929,18360,18713,18750,18763,DIT1,DIT2_DEV7,PTE_CLOUD,17952	
*/			
	public static String suite="";
	public static String config="";
	public static void main(String[] args) throws Exception{
				Logger log = LoggerUtils.getLogger();
	    log.info("Start of Main Controller");
	    log.debug("debug message from logger");
	     suite=args[0];
	 	System.out.println("Final suite:"+suite);
	     config=args[1];
	
		System.out.println("Final environment:"+config);
	    FileUtilities.setFoldervalue(suite+"\\");
	    PropertyUtils.setFoldervalue(config);
	    //Logger logger = Logger.getLogger("Main_Controller.class");
	       String log4jConfigFile = System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+ File.separator + "log4j.properties";
	        PropertyConfigurator.configure(log4jConfigFile);

//	*/
		Fillo fill=new Fillo();
		
		Connection conn=fill.getConnection(System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
		Recordset rs=conn.executeQuery("Select * from Main_Controller where RunStatus ='Yes'");
		while (rs.next()) {
				String _TestCases=rs.getField("TestCases");
			
				log.info("Testacases started:"+_TestCases);
				System.out.println("Testcases started:"+_TestCases);
				//String Process_1 = rs.getField("Process_1");
				String Process_2=rs.getField("Process_2");
				//String Process_3 = rs.getField("Process_3");
			//	if (Process_1.equalsIgnoreCase("BO_CIM")) {
				//	System.out.println("CIM BO Will Execute");
				//	log.info("In Process1");
				//}
				if (Process_2.equalsIgnoreCase("PSH")) {
					Modules mod=new Modules();
					Keywordoperations _Storevalue=new Keywordoperations();
					_Storevalue.StoreValue("VAR_TCNO",_TestCases);
				   System.out.println(_TestCases);
					//mod.BO_CIM(_TestCases,suite);
					
			
				}
				//if (Process_3.equalsIgnoreCase("PSH")) {
				//	System.out.println("PSH Will Execute");
				//}
		}
		conn.close();
		try
		{
			  File excel = new File(System.getProperty("user.dir")+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
			    FileInputStream fis = new FileInputStream(excel);
			    HSSFWorkbook book = new HSSFWorkbook(fis);
			    FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir")+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
			    book.setForceFormulaRecalculation(true);
			    book.write(fos);
			    fis.close();
			    fos.flush();
			    fos.close();
		}
		catch(Exception e)
		{
			
		}
	}
}
