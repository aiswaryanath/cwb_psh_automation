package MainController;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.log4j.Category;
import org.apache.log4j.Logger; 
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
 
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;


import Logger.LoggerUtils;
import Modules.Modules;
import UIOperations.Keywordoperations;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;

public class TestNGEntryClass {
	//public static Logger					logger			= Logger.getLogger("CBX");
	private static ExtentReports extent;
	static ExtentTest logger;
	public static synchronized ExtentReports GetExtent(String suite,String config) {
	    if (extent != null) return extent;

	    extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
		//extent.addSystemInfo("Environment","Environment Name")
		extent
                .addSystemInfo("Suite Name", suite)
                .addSystemInfo("Environment", config)
                .addSystemInfo("User Name", "Aishwarya Nath");
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	    return extent;
	    
	}
	 @BeforeClass
	    public void setup(){
	     
	    }
@Test
@Parameters ({"SUITE", "CONFIG"})
	public static void startPSH(String suite,String config) throws Exception{
    String log4jConfigFile = System.getProperty("user.dir")+ "\\src\\test\\resources\\" + "log4j.properties";
    PropertyConfigurator.configure(log4jConfigFile);
	Logger log = LoggerUtils.getLogger();
    log.info("Start of Main Controller");

	    System.out.println("Current selection suite and env:"+suite+"  "+config);
	    log.info("Current selection suite and env:"+suite+"  "+config);
	    FileUtilities.setFoldervalue(suite+"\\");
	    PropertyUtils.setFoldervalue(config);
	    extent = GetExtent(suite,config);
	    System.out.println("FileUtilities value:"+FileUtilities.foldervalue);
	    System.out.println("Property value:"+PropertyUtils.getFoldervalue());
	    

//	*/
		Fillo fill=new Fillo();
	
		Connection conn=fill.getConnection(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls");
		Recordset rs=conn.executeQuery("Select * from Main_Controller where RunStatus ='Yes'");
		while (rs.next()) {
				String _TestCases=rs.getField("TestCases");
			
		
				System.out.println("Testcases started:"+_TestCases);
				log.info("Testcases started:"+_TestCases);
				logger = extent.startTest(_TestCases);
				//String Process_1 = rs.getField("Process_1");
				String Process_2=rs.getField("Process_2");
				//String Process_3 = rs.getField("Process_3");
			//	if (Process_1.equalsIgnoreCase("BO_CIM")) {
				//	System.out.println("CIM BO Will Execute");
			
				//}
				if (Process_2.equalsIgnoreCase("PSH")) {
					Modules mod=new Modules();
					Keywordoperations _Storevalue=new Keywordoperations();
					_Storevalue.StoreValue("VAR_TCNO",_TestCases);
				   System.out.println(_TestCases);
				   log.info(_TestCases);
				    System.out.println("suite value:"+suite);
				extent=	Modules.BO_CIM(_TestCases,suite,logger,extent);
				//	logger.
			
				}
				//if (Process_3.equalsIgnoreCase("PSH")) {
				//	System.out.println("PSH Will Execute");
				//}
		}
		conn.close();
		try
		{
		   File excel = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls");
		    FileInputStream fis = new FileInputStream(excel);
		    HSSFWorkbook book = new HSSFWorkbook(fis);
		    FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir")+"\\\\src\\\\test\\\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls");
		    book.setForceFormulaRecalculation(true);
		    book.write(fos);
		    fis.close();
		    fos.flush();
		    fos.close();
		    extent.flush();
		    extent.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
