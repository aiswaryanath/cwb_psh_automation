package MainController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import PSHExtentReport.Reports.*;
import org.apache.log4j.Category;
import org.apache.log4j.Logger; 
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
 
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Logger.LoggerUtils;
import Modules.Modules;
import PSHExtentReport.Reports.runTestNGTest;
import ReportGeneration.reportTest;
import UIOperations.Keywordoperations;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;

public class Main_Executable {
	//public static Logger					logger			= Logger.getLogger("CBX");
	public static String suite="SUITE-BBD";
	public static String config="17925";
	public static ExtentReports extent;
	static ExtentTest logger;
	
	public static synchronized ExtentReports GetExtent(String Suite,String config) {
	    if (extent != null) return extent;

	    extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
		//extent.addSystemInfo("Environment","Environment Name")
		extent
                .addSystemInfo("Suite Name", Suite)
                .addSystemInfo("Environment", config)
                .addSystemInfo("User Name", "Aishwarya Nath");
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	    return extent;
	}
	public static void main(String[] args) throws Exception{
				Logger log = LoggerUtils.getLogger();
	    log.info("Start of Main Controller");
	    log.debug("debug message from logger");
	 
	    System.out.println("");
		System.out.println("");
		System.out.println("******************Intellect Design Arena...<<Design For Digital>>*********************");
		System.out.println("");
		System.out.println("*****************Welcome To Integrated PSH Automation Tool*****************************-V 5.0");
		System.out.println("");
		System.out.println("Select environment for execution");
		System.out.println("");
		System.out.println("(1) - CWB");
		System.out.println("");
		System.out.println("(2) - In case of new env");
	
		System.out.println("Please enter the above keys for suite selection.");
		BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
		String _Op = reader.readLine(); 
		switch (_Op) {
		case "2":
			System.out.println("If new environment enter the created folder name in the project");
			BufferedReader reader1 =new BufferedReader(new InputStreamReader(System.in));
			String newf = reader1.readLine(); 
			config=newf;
			break;
		case "1":
			config="CWB";
			break;
		
		default:
			System.out.println("It seems you have entered invalid input...Please Try Again!!");
			Thread.sleep(10000);
			System.exit(0);
			break;
			
		
			
		}
		System.out.println("Select suite for execution");
		System.out.println("");
		System.out.println("(1) - SUITE- Negative BV");
		System.out.println("");
		System.out.println("(2) - SUITE- Positive BV");
		System.out.println("");
		System.out.println("(3) - SUITE- Negative FV");
		System.out.println("");
		System.out.println("(4) - SUITE- Positive FV");
		System.out.println("");
		System.out.println("(5) - SUITE- Negative INBOUND");
		System.out.println("");
		System.out.println("(6) - SUITE- Positive INBOUND");
	
		System.out.println("Please enter the keys above keys for selection of suite.");
		BufferedReader reader3 =new BufferedReader(new InputStreamReader(System.in));
		String _Op1 = reader.readLine(); 
		switch (_Op1) {

		case "1":
			suite="SUITE-CWB-N-BV";
			break;
		
		case "2":
			suite="SUITE-CWB-P-BV";
			break;
		case "3":
			suite="SUITE-CWB-N-FV";
			break;
		case "4":
			suite="SUITE-CWB-P-FV";
			break;
		case "5":
			suite="SUITE-CWB-INBOUND-NEGATIVE";
			break;
		case "6":
			suite="SUITE-CWB-INBOUND-POSITIVE";
			break;
		default:
			System.out.println("It seems you have entered invalid input...Please Try Again!!");
			Thread.sleep(10000);
			System.exit(0);
			break;
			
		
			
		}
		System.out.println("Final suite:"+suite);
		System.out.println("Final environment:"+config);
	    FileUtilities.setFoldervalue(suite+"\\");
	    PropertyUtils.setFoldervalue(config);
	    
	    extent = GetExtent(suite,config);
	    //Logger logger = Logger.getLogger("Main_Controller.class");
	       String log4jConfigFile = System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+ File.separator + "log4j.properties";
	        PropertyConfigurator.configure(log4jConfigFile);

//	*/
		Fillo fill=new Fillo();
		
		Connection conn=fill.getConnection(System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
		Recordset rs=conn.executeQuery("Select * from Main_Controller where RunStatus ='Yes'");
		   reportTest r7=new reportTest();
		 r7.deletevalueentries(suite);
		while (rs.next()) {
				String _TestCases=rs.getField("TestCases");
			
				log.info("Testacases started:"+_TestCases);
				System.out.println("Testcases started:"+_TestCases);
				logger = extent.startTest(_TestCases);
        		
				String Process_2=rs.getField("Process_2");
		
				if (Process_2.equalsIgnoreCase("PSH")) {
					Modules mod=new Modules();
					r7.deletevalueentries(suite);
					Keywordoperations _Storevalue=new Keywordoperations();
					_Storevalue.StoreValue("VAR_TCNO",_TestCases);
				   System.out.println(_TestCases);
				
					extent=mod.BO_CIM(_TestCases,suite,logger,extent);
					
			
				}
			
		}
		conn.close();
		
		try
		{
			runTestNGTest r=new runTestNGTest();
			r.createtestng(suite,config);
			  File excel = new File(System.getProperty("user.dir")+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
			    FileInputStream fis = new FileInputStream(excel);
			    HSSFWorkbook book = new HSSFWorkbook(fis);
			    FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir")+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
			    book.setForceFormulaRecalculation(true);
			    book.write(fos);
			    fis.close();
			    fos.flush();
			    fos.close();
			    extent.flush();
			    extent.close();
		}
		catch(Exception e)
		{
			
		}
	}
}
