package Modules;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import FGT.FGTBatchExecution;

//import com.gargoylesoftware.htmlunit.BrowserVersion;

import Logger.LoggerUtils;
import UIOperations.APIOperations;
import UIOperations.FGTExecution;
import UIOperations.Keywordoperations;
import UI_CC2_ManualActionInquiry.PSH_Inqcontroller;
import Utilities.ApiCall;
import Utilities.DBConnect;
import Utilities.DBConnector;
import Utilities.DataComparision;
import Utilities.FileUtilities;
import Utilities.PSHWiresInteracUpdate;
import Utilities.PropertyUtils;
import Utilities.TakeScreenShot_evidence;
import Utilities.WiresAckMT103MT199;
import Utilities.XmlParserDOTRDFTR2;
import io.github.bonigarcia.wdm.WebDriverManager;
import rebulkandResponse.Action_Keywords;
import MainController.Main_Executable;
import ReportGeneration.kiosk;


public class Modules {
	public static WebDriver driver=null;
	public static Connection connection=null;
	static Logger log = LoggerUtils.getLogger();
	ApiCall apisteve=new ApiCall();
	static FileUtilities Fileutil = new FileUtilities();
	static String KeywordOperation;
	static kiosk k5;
	public static ExtentReports BO_CIM(String TestCases,String suite,ExtentTest logger,ExtentReports extent)throws Exception{
		try{
		String filename=System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls";		
		File file =    new File(filename);
		System.out.println("line1:"+filename);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook _Workbook = null;
		_Workbook = new HSSFWorkbook(inputStream);
		System.out.println("line2");
		ChromeOptions options = new ChromeOptions();
		//options.setProxy(null);
		System.out.println("line3");
		Keywordoperations Storevalue1=new Keywordoperations();
		String E2EDB_URL=PropertyUtils.readProperty("E2EDB_URL");
		String E2EDB_UserName=PropertyUtils.readProperty("E2EDB_UserName");
		String E2EDB_UserPass=PropertyUtils.readProperty("E2EDB_UserPass");
	    log.info( E2EDB_URL+"   "+E2EDB_UserName+"  "+E2EDB_UserPass);
		
	    log.info("Modules class from getlogger");
	    log.debug("debug message from getlogger");
	
		try
		
		{
			log.info("before connnection");
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection=DriverManager.getConnection("jdbc:oracle:thin:@"+E2EDB_URL,E2EDB_UserName,E2EDB_UserPass);
			System.out.println("CONNECTION SUCESSFUL");
		    log.info("CONNECTION SUCESSFUL TO DB");}
		
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("connection unsuccessful to db");
			log.info("connection unsuccessful to db");
			
			System. exit(1);
		}
		String pathdriver=System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe";
		//System.setProperty("webdriver.chrome.driver", pathdriver);
		WebDriverManager.chromedriver().setup();
		Sheet sheet = _Workbook.getSheet("PSH");
		int rowcount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		log.info(rowcount);
		String Resultflag="NA";
		String StackStraceflag="NA";
		ArrayList <Integer>arrli= new ArrayList<Integer>();
		
		LinkedHashMap<String, String> Result = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> Result_StackStrace = new LinkedHashMap<String, String>();
			for (int i = 1; i < rowcount+1; i++) {	
			
			Row row1 = sheet.getRow(i);
			String  Test_Cases= row1.getCell(0).toString();

			String Flag = row1.getCell(3).toString();
			//String tcNO = row1.getCell(3).toString();
			if (Test_Cases.equalsIgnoreCase(TestCases)&&Flag.equalsIgnoreCase("YES") )
			{	
				//ystem.out.println("HERE");
				arrli.add(i);
				//Storevalue1.StoreValue("TESTCASENO", );
				
			}
			}
			for (int k :arrli)
			{ Resultflag="NA";
			StackStraceflag="NA";
			Row row = sheet.getRow(k);

			try{
			KeywordOperation = row.getCell(4).toString();
			String TestData = row.getCell(5).toString();
			String TestCaseNo = row.getCell(0).toString();
			String Skipflag= row.getCell(6).toString();
			System.out.println("Testdata:"+TestData);
			System.out.println("Testcaseno:"+TestCaseNo);
			Keywordoperations Keywordoperations=new Keywordoperations();
			APIOperations APIOperations=new APIOperations(); 
			FileUtilities fileutil=new FileUtilities(); 
			
			ArrayList<String> _OBVal= new ArrayList<String>();
			_OBVal= DBConnect.GetValues(KeywordOperation,connection);

			String XPATH_NAV = _OBVal.get(0);
			String ElementName = _OBVal.get(1);
			String ElementType = _OBVal.get(2);

			System.out.println(XPATH_NAV+":"+ElementType+":"+ElementName);
			log.info("element type:"+ElementType+":"+ElementName);
			String checkflag=fileutil.GetFilevalue("VAR_CHECKFLAG");
			if(Skipflag.equalsIgnoreCase("Y")&& !(checkflag.equalsIgnoreCase("PASS")))
			{
				Resultflag="SKIPPED";
				StackStraceflag="PRE CHECK CONDITION FAIL";	
			}
			if(Skipflag.equalsIgnoreCase("N") || (Skipflag.equalsIgnoreCase("Y")&& checkflag.equalsIgnoreCase("PASS")))
			{	
			switch (ElementType) {
			case "NavigateUI":
			try{
				   log.info("NavigateUI");
				    //driver = new ChromeDriver();
				    //options.addArguments("headless");
				    driver = new ChromeDriver(options);
				    String PSHURL = PropertyUtils.readProperty(TestData);
				    driver.get(PSHURL);
				    
				    //driver.get(PSHURLSCHEDULER);
					//driver.get(TestData);
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					driver.manage().window().maximize();
				
					//fileutil.emptyFilecontents();
					Resultflag="PASS";
				}
			catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "NavigatePSH":
				try{
		
						//fileutil.emptyFilecontents();
						//String Driverpath=System.getProperty("user.dir")+"\\Driver\\IEDriverServer.exe";
						System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\IEDriverServer.exe");
						DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
						capability.setCapability("pageLoadStrategy", "eager");
						
						//commented
						//capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true); 
						capability.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,false);
						capability.setCapability("nativeEvents", false); 
						//capability.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
						capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
						driver = new InternetExplorerDriver(capability);
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
						//driver.get("http://10.10.8.62:10062/BPS/");
						String PSHURL = PropertyUtils.readProperty("PSH_URL");
						driver.get(PSHURL);
						

						driver.manage().window().maximize();

						
						
						Resultflag="PASS";
}
				catch (Exception e) {
					e.printStackTrace();
					log.debug(e.toString());
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;
			/*		
			case "NavigateHeadLess":
				try{
				
					
						System.setProperty("webdriver.ie.driver", "D:/CBX_Automation/Driver/IEDriverServer.exe");
						DesiredCapabilities capability = DesiredCapabilities.htmlUnit();
						capability.setCapability("pageLoadStrategy", "eager");
						capability.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,false);
						capability.setCapability("nativeEvents", false); 
						capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
						 HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.INTERNET_EXPLORER_11);
						 driver.setJavascriptEnabled(true);
						 driver.getCapabilities();
						 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
					
						String PSHURL = PropertyUtils.readProperty("PSH_URL");
						driver.get(PSHURL);
						

						driver.manage().window().maximize();

						
						
						Resultflag="PASS";
					}
				catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;		
				*/	  
				
			case "switchToWindow":
				
				try{
					boolean flag=false;
					String parentWinHandle=driver.getWindowHandle();
				System.out.println(driver.getTitle());
			        Set<String> winHandles = driver.getWindowHandles();
			        for(String handle: winHandles){
			        	
			            if(!handle.equals(parentWinHandle)){
			            driver.switchTo().window(handle);
			            
			            if(driver.getTitle().equals(TestData))
			            break;
			        	System.out.println(driver.getTitle());
			         	log.info(driver.getTitle());
			            }
			            else
			            {
			            	System.out.println("Window not found");
			            	log.info("Window not found");
			            	
			            }}
			        
			        driver.switchTo().defaultContent();
			           
					Resultflag="PASS";
					
				}
				catch (Exception e) {
					Resultflag="FAIL";
					log.debug(e.toString());
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
case "SwitchToFrame":
				/*
				try{
					JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
					String currentFrame = (String) jsExecutor.executeScript("return self.name");
					System.out.println("CurrentFrame:"+currentFrame); 
					
					Thread.sleep(5000);
					driver.switchTo().defaultContent();
					System.out.println(driver.getTitle());
					driver.switchTo().frame(TestData);
					Thread.sleep(5000);
					Resultflag="PASS";
					
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				*/


	try{
		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		String currentFrame = (String) jsExecutor.executeScript("return self.name");
		System.out.println("CurrentFrame:"+currentFrame); 
		log.info("CurrentFrame:"+currentFrame);
		/*WebDriverWait wait= new WebDriverWait(driver, 5);
				  wait.until(ExpectedConditions.frameToBeAvail("CurrentFrame:"+currentFrameableAndSwitchToIt(TestData));*/
		/*Thread.sleep(5000);*/
		driver.switchTo().defaultContent();
		String currentFrame1 = (String) jsExecutor.executeScript("return self.name");
		System.out.println("Default Frame:"+currentFrame1); 
		log.info("Default Frame:"+currentFrame1); 
		System.out.println(driver.getTitle());
		driver.switchTo().frame(TestData);
		//Thread.sleep(5000);
		Resultflag="PASS";

	}
	catch (Exception e) {
		Resultflag="FAIL";
		log.debug(e.toString());
		StackStraceflag="STACKTRACE"+e.toString();
	}
	break; 
case "SwitchToFrameObj":
	

	try{


		//Thread.sleep(5000);
		//driver.switchTo().defaultContent();
		System.out.println(driver.getTitle());
		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		String currentFrame = (String) jsExecutor.executeScript("return self.name");
		System.out.println("CurrentFrame:"+currentFrame); 
		log.info("Current Frame:"+currentFrame);
		if(TestData.contains("NULL"))
		{
			driver.switchTo().defaultContent();
			String TestDataval[]=TestData.split("\\|");
			TestData=TestDataval[1];
		}
		{
		WebElement frame_1 = driver.findElement(By.xpath(XPATH_NAV));
		driver.switchTo().frame(frame_1);}

		driver.switchTo().frame(TestData);

		System.out.println("In SwitchToFrameObj");
		//driver.switchTo().frame(TestData);
		Thread.sleep(3000);
		Resultflag="PASS";

	}
	catch (Exception e) {
		Resultflag="FAIL";
		log.debug(e.toString());
		StackStraceflag="STACKTRACE"+e.toString();
	}
	break; 
			case "TriggerSendMoney":				
				try{					
				/*String _JSON_BODY = */UIOperations.APIOperations.TriggerSendMoneyNotifications(TestData);	
				//FileUtilities.StackTraceResult_JSONBODY(Integer.toString(k), _JSON_BODY);				
					Resultflag="PASS";			
				}catch (Exception e) 
				{				
					Resultflag="FAIL";				
					StackStraceflag="STACKTRACE"+e.toString();			
				}			
				break;			
		
			case "GetMessageSeqID":
				try{
					Keywordoperations.GetSubStringofMessageSEQID(TestData);
					Resultflag="PASS";
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			
			case "ESMessageID":
				try{
					Keywordoperations.GetValueFromElasticDB(TestData);
					Resultflag="PASS";
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "FrameDefaultContent":
				try{
					driver.switchTo().defaultContent();
					Resultflag="PASS";
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			
			case "INTERAC_UPDATE_API":
				try{
					String _JSON_BODY = APIOperations.UpdateSTATELESS_API(TestData);
					FileUtilities.StackTraceResult_JSONBODY(Integer.toString(k), _JSON_BODY);
					Resultflag="PASS";
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "INTERAC_PARAM_OVERIDE_API":
				try{
					String _JSON_BODY = APIOperations.UpdateSTATELESSinteracParamOveride_API(TestData);
					FileUtilities.StackTraceResult_JSONBODY(Integer.toString(k), _JSON_BODY);
					Resultflag="PASS";
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			
			case "CustomerAmounts_API":
				try{
					String _JSON_BODY = APIOperations._getCustomerAmounts_API(TestData);
					FileUtilities.StackTraceResult_JSONBODY(Integer.toString(k), _JSON_BODY);
					Resultflag="PASS";
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;

				
	
	
			case "DBConnector":
				try{
					DBConnector DBConnect=new DBConnector();
					String DB_ReturnFlag=DBConnect.DBConnector(TestData);
					Resultflag="PASS";
					String FileworkitemiId=Fileutil.GetFilevalue("VAR_PSHFILEWORKITEMID");
					String TxnId=Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
					StackStraceflag=FileworkitemiId+" :"+TxnId+" ";
				}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "DBConnector2":
				try{
					DBConnector DBConnect=new DBConnector();
					String DB_ReturnFlag=DBConnect.DBConnector2(TestData);
					Resultflag="PASS";
					
				}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "GetRebulkID":
				try{
					XmlParserDOTRDFTR2 re=new XmlParserDOTRDFTR2();
					String TXNREFNUM=re.GetTXNREFNUM(TestData);
					
					Resultflag="PASS";
					StackStraceflag=Fileutil.GetFilevalue("VAR_REBULKID");
				}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
				}
				break;
			case "PSHInterfaceDBConnector":
				try{
					DBConnector DBConnect=new DBConnector();
					String statusreq=null;
					HashMap<String, String> DB_ReturnFlag=DBConnect.PSHInterfaceDB(TestData);
					if (TestData != null) {
						String[] Data = TestData.split("\\|");
						statusreq=Data[4];
						System.out.println("VALUE REQ"+statusreq);
					}
					String flag=DB_ReturnFlag.get("InterfaceStatus");
				    System.out.println("flag VALUE FROM HASHMAP"+flag);
				    if(flag==null)
					{
					flag="NO RECORD";
					System.out.println("NO RECORD");
					}
					if(flag.equals(statusreq))
					{
					Resultflag="PASS";
					System.out.println("Actual value PASS");
					}
					
					else
					{
						Resultflag="FAIL";
						System.out.println("Actual value fail");
					}
				}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
				}
				break;
			case "PSHTXNSTATUSCHECK":
				try{
					DBConnector DBConnect=new DBConnector();
					String TASKID=null;
					String QUEUEID=null;
					String TRNSTATUSCODE=null;
					Resultflag="FAIL";
					HashMap<String, String> DB_ReturnFlag=DBConnect.GetTxnDetails(TestData);

						String[] Data = TestData.split("\\|");
						TASKID=Data[2];
						QUEUEID=Data[3];
						TRNSTATUSCODE=Data[4];
						System.out.println("VALUES FROM LHS"+TASKID+" "+QUEUEID+""+TRNSTATUSCODE);
				        String lhs="VALUES FROM LHS"+TASKID+" "+QUEUEID+""+TRNSTATUSCODE;
					String DTASKID=DB_ReturnFlag.get("TASKID");
					String DQUEUEID=DB_ReturnFlag.get("QUEUEID");
					String DTXNSTATUS=DB_ReturnFlag.get("TXN_STATUS_CODE");
				    System.out.println("VALUES FROM RHS"+DTASKID+" "+DQUEUEID+""+DTXNSTATUS);
				      String rhs="VALUES FROM RHS"+DTASKID+" "+DQUEUEID+""+DTXNSTATUS;
					if(DTASKID.equals(TASKID))
					{
						if(DQUEUEID.equals(QUEUEID))
						{
							if(DTXNSTATUS.equals(TRNSTATUSCODE))
							{
					Resultflag="PASS";
					System.out.println("Actual value PASS");
					Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
					}
							else
							{
								Resultflag="FAIL";
								System.out.println("Actual value FAIL");
								Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
								//StackStraceflag="Txn status does not match "+"Actual:"+DTXNSTATUS;
								StackStraceflag=lhs+"\n"+rhs;
							}
					}
						else
						{
							Resultflag="FAIL";
							System.out.println("Actual value FAIL");
							Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
							//StackStraceflag="Queue does not match "+"Actual:"+DQUEUEID;
							StackStraceflag=lhs+"\n"+rhs;
						}
				    }
					else
					{
						Resultflag="FAIL";
						System.out.println("Actual value fail");
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						//StackStraceflag="Task does not match "+"Actual:"+DTASKID;
						StackStraceflag=lhs+"\n"+rhs;
					}
				}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						log.debug(e.toString());
				}
				break;
			case "PSHFILEPARSINGCHECK":
				try{
					DBConnector DBConnect=new DBConnector();
					String STATUS=null;
					String EXCEPTION_ID=null;
					String EXCEPTION_DESC=null;
					String Ext_EXCEPTION_ID=null;
					String Ext_EXCEPTION_DESC=null;
					String TAG_NAME=null;
					Resultflag="FAIL";
					HashMap<String, String> DB_ReturnFlag=DBConnect.GetTxnDetails2(TestData);

						String[] Data = TestData.split("\\|");
						STATUS=Data[0];
						EXCEPTION_ID=Data[1];
						EXCEPTION_DESC=Data[2];
						Ext_EXCEPTION_ID=Data[3];
						Ext_EXCEPTION_DESC=Data[4];
						TAG_NAME = Data[5];
						
						String flag="N";
						if(Ext_EXCEPTION_ID.equals("-"))
						{
							flag="Y";
						}
						String flag2="N";
						if(TAG_NAME.equals("-"))
						{
							flag2="Y";
						} 
						
						System.out.println("VALUES FROM TESTDATA"+STATUS+" "+EXCEPTION_ID+""+EXCEPTION_DESC+""+Ext_EXCEPTION_ID+""+Ext_EXCEPTION_DESC+""+TAG_NAME);
				
					String DSTATUS=DB_ReturnFlag.get("FILE_STATUS");
					String DEXCEPTION_ID=DB_ReturnFlag.get("EXCEPTION_ID");
					String DEXCEPTION_DESC=DB_ReturnFlag.get("EXCEPTION_DESC");
					String DExt_EXCEPTION_ID=DB_ReturnFlag.get("EXT_EXCEPTION_ID");
					String DExt_EXCEPTION_DESC=DB_ReturnFlag.get("EXT_EXCEPTION_DESC");
					String DTAG_NAME = DB_ReturnFlag.get("TAG_NAME");
				    System.out.println("VALUE FROM HASHMAP-"+DSTATUS+" "+DEXCEPTION_ID+""+DEXCEPTION_DESC+""+DExt_EXCEPTION_ID+""+DExt_EXCEPTION_DESC+""+DTAG_NAME);
				    
					if(DSTATUS.equals(STATUS))
					{
						System.out.println("staus is passed");
						if(DEXCEPTION_ID.equals(EXCEPTION_ID))
						{
							System.out.println("exception id is passed");
							if(DEXCEPTION_DESC.equals(EXCEPTION_DESC))
							{
								System.out.println("exception desc passed");
								if(((DExt_EXCEPTION_ID== null)&& flag.equals("Y")) ||DExt_EXCEPTION_ID.equals(Ext_EXCEPTION_ID))
								//if(DExt_EXCEPTION_ID.equals(Ext_EXCEPTION_ID))
								{
									System.out.println("external exception id passed");
									if(((DExt_EXCEPTION_DESC== null)&& flag.equals("Y")) ||DExt_EXCEPTION_DESC.equals(Ext_EXCEPTION_DESC))
									//if(DExt_EXCEPTION_DESC.equals(Ext_EXCEPTION_DESC))
									{
										System.out.println("ext exception desc passed");
										if(((DTAG_NAME== null)&& flag2.equals("Y")) ||DTAG_NAME.equals(TAG_NAME))

										{
										Resultflag="PASS";
										System.out.println("Actual value PASS");
										Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
										}
										else
										{
											Resultflag="FAIL";
											System.out.println("Actual value FAIL");
											Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
											StackStraceflag="Txn status does not match "+"Actual:"+DTAG_NAME;
										}
									}
								else
								{
									Resultflag="FAIL";
									System.out.println("Actual value FAIL");
									Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
									StackStraceflag="Queue does not match "+"Actual:"+DExt_EXCEPTION_DESC;
								}
							}
							else
							{
						Resultflag="FAIL";
						System.out.println("Actual value fail");
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						StackStraceflag="Task does not match "+"Actual:"+DExt_EXCEPTION_ID;
							}
				}
						else
						{
					Resultflag="FAIL";
					System.out.println("Actual value fail");
					Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
					StackStraceflag="Task does not match "+"Actual:"+DEXCEPTION_DESC;
						}
					}
					else
					{
				Resultflag="FAIL";
				System.out.println("Actual value fail");
				Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
				StackStraceflag="Task does not match "+"Actual:"+DEXCEPTION_ID;
				log.debug("Task does not match "+"Actual:"+DEXCEPTION_ID);
					}
				}
					else
					{
				Resultflag="FAIL";
				System.out.println("Actual value fail");
				Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
				StackStraceflag="Task does not match "+"Actual:"+DSTATUS;
				log.debug("Task does not match "+"Actual:"+DSTATUS);
					}
				}
						catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE blah blah"+e.toString();
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						log.debug(e.toString());
				}
				break;
			case "PSHFILEPARSINGCHECK2":
				try{
					DBConnector DBConnect=new DBConnector();
					String STATUS=null;
					String EXCEPTION_ID=null;
					String EXCEPTION_DESC=null;
					String Ext_EXCEPTION_ID=null;
					String Ext_EXCEPTION_DESC=null;
					String TAG_NAME=null;
					Resultflag="FAIL";
					HashMap<String, String> DB_ReturnFlag=DBConnect.GetTxnDetails2(TestData);

						String[] Data = TestData.split("\\|");
						STATUS=Data[0];
						EXCEPTION_ID=Data[1];
						EXCEPTION_DESC=Data[2];
						Ext_EXCEPTION_ID=Data[3];
						Ext_EXCEPTION_DESC=Data[4];
						TAG_NAME = Data[5];
						
						String flag="N";
						if(Ext_EXCEPTION_ID.equals("-"))
						{
							flag="Y";
						}
						String flag2="N";
						if(Ext_EXCEPTION_ID.equals("-"))
						{
							flag2="Y";
						} 
						
						System.out.println("VALUES FROM TESTDATA"+STATUS+" "+EXCEPTION_ID+""+EXCEPTION_DESC+""+Ext_EXCEPTION_ID+""+Ext_EXCEPTION_DESC+""+TAG_NAME);
				
					String DSTATUS=DB_ReturnFlag.get("FILE_STATUS");
					String DEXCEPTION_ID=DB_ReturnFlag.get("EXCEPTION_ID");
					String DEXCEPTION_DESC=DB_ReturnFlag.get("EXCEPTION_DESC");
					String DExt_EXCEPTION_ID=DB_ReturnFlag.get("EXT_EXCEPTION_ID");
					String DExt_EXCEPTION_DESC=DB_ReturnFlag.get("EXT_EXCEPTION_DESC");
					String DTAG_NAME = DB_ReturnFlag.get("TAG_NAME");
				    System.out.println("VALUE FROM HASHMAP-"+DSTATUS+" "+DEXCEPTION_ID+""+DEXCEPTION_DESC+""+DExt_EXCEPTION_ID+""+DExt_EXCEPTION_DESC+""+DTAG_NAME);
				    log.info("VALUE FROM HASHMAP-"+DSTATUS+" "+DEXCEPTION_ID+""+DEXCEPTION_DESC+""+DExt_EXCEPTION_ID+""+DExt_EXCEPTION_DESC+""+DTAG_NAME);
					if(DSTATUS.equals(STATUS))
					{
						System.out.println("status is passed");
						 log.info("status is passed");
						if(DEXCEPTION_ID.equals(EXCEPTION_ID))
						{
							System.out.println("exception id is passed");
							 log.info("exception id is passed");
							if(DEXCEPTION_DESC.equals(EXCEPTION_DESC))
							{
								System.out.println("exception desc passed");
								 log.info("exception desc passed");
								if(((DExt_EXCEPTION_ID== null)&& flag.equals("Y")) ||DExt_EXCEPTION_ID.equals(Ext_EXCEPTION_ID))
								//if(DExt_EXCEPTION_ID.equals(Ext_EXCEPTION_ID))
								{
									System.out.println("external exception id passed");
									 log.info("external exception id passed");
									if(((DExt_EXCEPTION_DESC== null)&& flag.equals("Y")) ||DExt_EXCEPTION_DESC.equals(Ext_EXCEPTION_DESC))
									//if(DExt_EXCEPTION_DESC.equals(Ext_EXCEPTION_DESC))
									{
										System.out.println("ext exception desc passed");
								        log.info("ext exception desc passed");
										if(((DTAG_NAME== null)&& flag2.equals("Y")) ||DTAG_NAME.equals(TAG_NAME))

										//if(DTAG_NAME.equals(TAG_NAME))
										{
										Resultflag="PASS";
										System.out.println("Actual value PASS");
										   log.info("Actual value PASS");
										Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
										}
										else
										{
											Resultflag="FAIL";
											System.out.println("Actual value FAIL");
											log.info("Actual value FAIL"+"Txn status does not match "+"Actual:"+DTAG_NAME);
											Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
											StackStraceflag="Txn status does not match "+"Actual:"+DTAG_NAME;
										}
									}
								else
								{
									Resultflag="FAIL";
									System.out.println("Actual value FAIL");
									Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
									StackStraceflag="Queue does not match "+"Actual:"+DExt_EXCEPTION_DESC;
									log.info("Actual value FAIL"+"Queue does not match "+"Actual:"+DExt_EXCEPTION_DESC);
								}
							}
							else
							{
						Resultflag="FAIL";
						System.out.println("Actual value fail");
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						StackStraceflag="Task does not match "+"Actual:"+DExt_EXCEPTION_ID;
						log.info("Actual value FAIL"+"Task does not match "+"Actual:"+DExt_EXCEPTION_ID);
							}
				}
						else
						{
					Resultflag="FAIL";
					System.out.println("Actual value fail");
					Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
					StackStraceflag="Task does not match "+"Actual:"+DEXCEPTION_DESC;
					log.info("Actual value FAIL"+"Task does not match "+"Actual:"+DEXCEPTION_DESC);
						}
					}
					else
					{
				Resultflag="FAIL";
				System.out.println("Actual value fail");
				Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
				StackStraceflag="Task does not match "+"Actual:"+DEXCEPTION_ID;
				log.info("Actual value FAIL"+"Task does not match "+"Actual:"+DEXCEPTION_ID);
					}
				}
					else
					{
				Resultflag="FAIL";
				System.out.println("Actual value fail");
				Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
				StackStraceflag="Task does not match "+"Actual:"+DSTATUS;
				log.info("Actual value FAIL"+"Task does not match "+"Actual:"+DSTATUS);
					}
				}
						catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE blah blah"+e.toString();
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						log.debug(e.toString());
				}
				break;
			case "PSHFIELDCHECK":
				try{
					DBConnector DBConnect=new DBConnector();
					String result=null;
					String field=null;
					Resultflag="FAIL";
					HashMap<String, String> DB_ReturnFlag=DBConnect.GetTxnFieldDetails(TestData);

						String[] Data = TestData.split("\\|");
						result=Data[2];
						field=Data[3];
						System.out.println("VALUES FROM TESTDATA"+result);
						log.info("VALUES FROM TESTDATA"+result);
					String DRESULT=DB_ReturnFlag.get(field);
					System.out.println("VALUE FROM HASHMAP"+DRESULT);
					log.info("VALUES FROM HASHMAP"+DRESULT);
					if(DRESULT.equals(result))
					{
					Resultflag="PASS";
					System.out.println("Actual value PASS");
					log.info("Actual value PASS");
					Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
					}
							else
					{
								Resultflag="FAIL";
								System.out.println("Actual value FAIL");
								Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
								StackStraceflag="Result does not match "+result+":"+DRESULT;
								log.info("Result does not match "+result+":"+DRESULT);
					}
					
				}
				catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						log.debug(e.toString());
				}
				break;
			case "DBConnectorUpdate":
				try{
					DBConnector DBConnector=new DBConnector();
					DBConnector.UpdateDBEnvironment(TestData);
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
			case "DBConnectorUpdateDate":
				try{
					System.out.println("Data from file"+TestData);
					DBConnector DBConnector2=new DBConnector();
					int i=DBConnector2.Execute_Update_PSHQueryDate(TestData);
				
					if(i>=1)
					{
					Resultflag="PASS";
					}
				}
				catch (Exception e) {
					Resultflag="FAIL";
					System.out.println("result fail");
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;	
			case "FAECallIndicator":
				try{
					Resultflag = UIOperations.Keywordoperations.GetValueFromFAEIndicatorElasticDB(TestData);
					if (Resultflag.equals("PASS")) {
						Resultflag="PASS";
					}else{
						Resultflag="FAIL";
					}
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "DBOLIFABRICStatus":
				try{
					String DB_ReturnFlag = DBConnector.DBConnectorStatusCheck(DBConnector.get_DataValue(TestData).get(1),DBConnector.get_DataValue(TestData).get(2),DBConnector.get_DataValue(TestData).get(3),DBConnector.get_DataValue(TestData).get(4),DBConnector.get_DataValue(TestData).get(5));
					System.out.println(DB_ReturnFlag);
					if ((!DB_ReturnFlag.isEmpty()) && (DB_ReturnFlag!=null)) {
						FileUtilities.StackTraceResult_JSONBODY(Integer.toString(k), DB_ReturnFlag);
						StackStraceflag="STACKTRACE:"+DB_ReturnFlag;
						Resultflag="FAIL";
					}else{
						Resultflag="PASS";
					}
				}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "TextBox":
			try{
				
				Keywordoperations.SendKeys(XPATH_NAV,TestData,driver);
				Resultflag="PASS";
			}
			catch (Exception e) {
				Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				log.debug(e.toString());
			}
			break;
			
			case "Clear":
				try{
					
					WebElement element = driver.findElement(By.xpath(XPATH_NAV));
					element.clear();
					
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
			
			case "TextBoxIE":
				try{
				Keywordoperations.SendKeysie(XPATH_NAV,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
				Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				log.debug(e.toString());
				}
				break;

			case "SelectandEnter":
			try{
				Keywordoperations.selectAndEnter(XPATH_NAV,TestData,driver);
				Resultflag="PASS";
			}
			catch (Exception e) {
				Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				log.debug(e.toString());
			}
			break;
			case "TriggerPSH-CBETAPI":
				try{
					String status=Keywordoperations.PSHCBETAPI(TestData);
					if(status=="PASS")
					{
					Resultflag="PASS";
					}
					else
					{Resultflag="FAIL";}
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				
			break;
			case "TriggerPSH-CWIRESAPI":
				try{
					String status=Keywordoperations.PSHCWiresCall(TestData);
					if(status=="PASS")
					{
					Resultflag="PASS";
					}
					else
					{Resultflag="FAIL";}
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				
			break;
			case "TriggerPSH-CC2CALL":
				try{
					log.info("cc2 call started");
					String status=Keywordoperations.PSHCCC2API(TestData);
					if(status=="PASS")
					{
					Resultflag="PASS";
					log.info("PASS:cc2 api");
					}
					else
					{Resultflag="FAIL";
					log.info("fail:CC2 api");}
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;

			case "TriggerPSH-CANCAPI":
				try{
					String status=Keywordoperations.PSHCWiresCancellation(TestData);
					if(status=="PASS")
					{
					Resultflag="PASS";
					log.info("pass:cancellation api");
					}
					else
					{Resultflag="FAIL";
					log.info("fail:cancellation api");}
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				
			break;
			case "TriggerPSH-CWIRESINTERACUPDATE":
				try{
					PSHWiresInteracUpdate P=new PSHWiresInteracUpdate();
					if (TestData != null) {
						String[] Data = TestData.split("\\|");
						String INTERACCODE=  Data[0];
						String TXNREFNUM = Data[1];
					String status=P.getresponse(INTERACCODE, TXNREFNUM);
					if(status=="PASS")
					{
					Resultflag="PASS";
					log.info("pass:interac update");
					}
					else
					{Resultflag="FAIL";
					log.info("fail:interac update");}
					}
				}catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				
			break;
			case "buttonclickie":
	
				try{
					//Thread.sleep(3000);
					WebDriverWait wait = new WebDriverWait(driver, 30);

					WebElement clickable = wait.until(
					        ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));


					JavascriptExecutor js = (JavascriptExecutor) driver;

					((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));

					/*driver.findElement(By.xpath(XPATH_NAV)).click();*/
					Resultflag="PASS";
					}
				catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
				break; 
			case "button":
				try{
				driver.findElement(By.xpath(XPATH_NAV)).click();
				Resultflag="PASS";
				}
			catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;
		
			case "VerifyDisplayed":
				try{
					Resultflag=Keywordoperations.VerifyDisplayed(XPATH_NAV,ElementType,TestData,driver);
			
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
				case "VerifyNotDisplayed":
				try{
				Keywordoperations.VerifyNotDisplayed(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			case "VerifyText":
			case "VerifyTextPartial":
				try{
				Resultflag=Keywordoperations.VerifyText(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
				Resultflag="FAIL";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;
			case "DatePicker":
				try{
				Keywordoperations.selectDate(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;
			case "VerifyDisabledDate":
				try{
				Resultflag=Keywordoperations.VerifyDisabledDate(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Date is not Disabled";
					}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;
			case "HyperLink":
				try{
				driver.findElement(By.xpath(XPATH_NAV)).click();
				Resultflag="PASS";
			}
		catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;
			case "DivSelect":
				try{
				Keywordoperations.SelectDiv(XPATH_NAV,TestData,driver);
				Resultflag="PASS";
				}
			catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;
			
			case "Select":
				try{
				Keywordoperations.SelectIE(XPATH_NAV,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "Screenshot":
				try{
					TakeScreenShot_evidence t=new TakeScreenShot_evidence();
					t.Evidences(driver, TestData);
				
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
				
			case "GetTextQuoteId":
				try{
				DBConnector d=new DBConnector();
				d.GetTextQuoteId(TestData);
				
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;	
			case "AlertAccept":
				try{
				     //Alert alert = driver.switchTo().alert();		
				      //  Thread.sleep(10000);
				        // Capturing alert message.    
				       // String alertMessage= driver.switchTo().alert().getText();		
				        		
				        // Displaying alert message		
				      //  System.out.println(alertMessage);	
				      //  Thread.sleep(5000);
				        		
				        // Accepting alert		
				      //  alert.accept();	
				       //.sleep(5000);
					Set<String> Windows = driver.getWindowHandles();
					System.out.println("handle:"+Windows.toString());
					//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@src='images/indeximages/Login_button.gif']")));
					for (String win : Windows) {
						driver.switchTo().window(win);
						System.out.println(driver.getTitle());
						if (!"Label Entry Window".equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle())) {
							System.out.println("POPUP FX!!");
							driver.switchTo().window(win);
							Thread.sleep(7000);
							System.out.println(driver.findElement(By.xpath("//td/h5")).getText());
							//driver.findElement(By.xpath("//input[@value='OK']")).click();
							driver.findElement(By.xpath("//div//input")).click();
							System.out.println("clicked");
							//((JavascriptExecutor) driver).executeScript("arguments[0].value='"+1000000+"';", driver.findElement(By.id("P524235589-312501775")));	
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//div//input")));
							Thread.sleep(7000);
							break;
						}
					}
					for (String win : Windows) {
						driver.switchTo().window(win);
						System.out.println(driver.getTitle());
						if ("Label Entry Window".equalsIgnoreCase(driver.getTitle()))
						{		
				        driver.switchTo().defaultContent();
				    	
						JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
						String currentFrame1 = (String) jsExecutor.executeScript("return self.name");
						System.out.println("CurrentFrame :"+currentFrame1+driver.getTitle()); 
						break;
						}
					}
				Resultflag="PASS";
	
				}
				catch (Exception e) {
					Resultflag="FAIL";
					e.printStackTrace();
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;	
			case "SelectValidation":
				try{
				String res=Keywordoperations.SelectIEOPTIONVALUES(XPATH_NAV,TestData,driver);
				if(res.equals("PASS"))
				{
					Resultflag="PASS";	
				}
				else
				{
				
						Resultflag="FAIL";	
		
				}
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
				

				
			case "ValidateDropdownValues":
				Resultflag	=Keywordoperations.ValidateDropdownValues(XPATH_NAV,ElementType,TestData,driver);
				try{
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Values Dont match";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
				
			case "ValidateSelectValues":
				try{
				Resultflag	=Keywordoperations.ValidateSelectValues(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Values Dont match";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;	
			case "SelectFromContactLookup":
				try{
					Keywordoperations.SelectFromContactLookup(XPATH_NAV,ElementType,TestData,driver);
					Resultflag="PASS";
						
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
				break;
			case "getRow":
			case "getDataRow":
				try{
				Resultflag=Keywordoperations.GetRow(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Row not found";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;
			case "ClickCell":
			case "ClickDataCell":
				try{
				Keywordoperations.ClickCell(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;
			case "VerifyDataCellPartial":
			case "VerifyCellPartial":
			case "VerifyCell":
				try{
				Resultflag=Keywordoperations.VerifyCell(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Values Dont match";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;
			case "VerifySortDates":
				try{
				Resultflag=Keywordoperations.VerifySortDates(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Dates are not sorted";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;	
			case "VerifySortAlpha":
				try{
					Resultflag=Keywordoperations.VerifySortAlpha(XPATH_NAV,ElementType,TestData,driver);
					if(Resultflag.equals("FAIL"))
						Resultflag="FAIL:Values  are not sorted";
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;	
			case "VerifySortNumber":
				try{
					Resultflag=Keywordoperations.VerifySortNumber(XPATH_NAV,ElementType,TestData,driver);
					if(Resultflag.equals("FAIL"))
						Resultflag="FAIL:Values  are not sorted";
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;	
			case "VerifySortValues":
				try{
				Resultflag=Keywordoperations.VerifySortValues(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Values  are not sorted";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;	
			case "OpenURL":
				try{
					Keywordoperations.OpenURL(TestData,driver);
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;
			case "GetText":
				try{
				Keywordoperations.GetText(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "UpdateFX_ID":
				try{
				DBConnector d=new DBConnector();
				d.Update_FXID(TestData);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "VerifyDisabled":
				try{
				Keywordoperations.VerifyDisabled(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "VerifyEnabled":
				try{
				Keywordoperations.VerifyEnabled(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "Wait":
				try{
				  Thread.sleep(Long.parseLong(TestData));
			    Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			   break;
				case "ClearTextBox":
				try{
					Keywordoperations.ClearTextBox(XPATH_NAV,TestData,driver);
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
			case "ActionButton":
				try{
				Keywordoperations.SuccessfullClickActions(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
			}
			catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
		      break;
			case "VerifyColumnValues":
				try{
				Resultflag=Keywordoperations.VerifyColumnValues(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Column Values are not matching";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "GetCellText":
				try{
				Keywordoperations.GetCellText(XPATH_NAV,ElementType,TestData,driver);
				
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "VerifyCellButton":
				try{
				Resultflag=Keywordoperations.VerifyCellButton(XPATH_NAV,ElementType,TestData,driver);
				if(Resultflag.equals("FAIL"))
					Resultflag="FAIL:Column Buttons not Displayed";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "Quit":
				try{
				Keywordoperations.QuitDriver(driver);
				Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
				Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe /T");
				Resultflag="PASS";
				}catch (Exception e) {
				Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				}
				break;	
			case "DriverClose":
				try{
				driver.close();	
				Resultflag="PASS";
				}catch (Exception e) {
				Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				}
				break;	
			case "Concat"	:
				try
				{
					Keywordoperations.concatenateString(TestData);
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break; 
			case "VerifyValue":
			case "VerifyValuePartial":
				try{
				Resultflag=Keywordoperations.VerifyValue(XPATH_NAV,ElementType,ElementName,TestData,driver);
				if(Resultflag.equals("FAIL"))
				Resultflag="FAIL";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			break;	
			
			case "API":
				try{
			       String status=Keywordoperations.STEVEAPI(TestData);
			      if(status=="true")
			      {
			    	  Resultflag="PASS";  
			      }
			      else if(status=="false")
			      {
			    	  Resultflag="FAIL";   
			      }
				
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
				
			case "PSHInqSystem":
				//WebDriver driver1 = null;
				//FileUtilities FileUtilities1=new FileUtilities();
				//String PSHworkitemid = FileUtilities1.GetFilevalue("PSHWORKITEMID");
//				PSH_Execute.Execute_PSH(driver1, "BCCMKR1", PSHworkitemid);
				try
				{
				PSH_Inqcontroller.ExecutePSH_InquirySystem(driver,TestData);
				Resultflag="PASS";
				}
				catch (Exception e)
				{
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
				
				
				
			case "SearchWorkitemID":
				try{
				PSH_Inqcontroller.SearchWorkitemID(driver,TestData,XPATH_NAV);
				 Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;	

			case "SearchWorkitemIDEFT":
				try{
				PSH_Inqcontroller.SearchWorkitemIDEFT(driver,TestData,XPATH_NAV);
				 Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;	
			
			case "SearchWorkitemIDActionClick":
				try{
				PSH_Inqcontroller.SearchWorkitemIDActionClick(driver,TestData,XPATH_NAV);
				 Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
			break;	

			case "GetColumnValues":
				try{
				Keywordoperations.GetColumnValues(XPATH_NAV,ElementType,TestData,driver);
				Resultflag="PASS";
				}
				
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;
		
			case "GetElasticValuesPayFrom":
				try{
				Resultflag=Keywordoperations.GetElasticValuesPayFrom(TestData);
				if(Resultflag!=null)
				{
				Resultflag="PASS";
				System.out.println("pass");
				}
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;	
				
			case "ComparePaytoValues":
				
				try{
					String var1=null;
					String var2=null;
					String Elasticpayto=null;
					if(TestData!=null)
					{
						String[] Data = TestData.split("\\|");
						var1=  Data[0];
						var2=  Data[1];
					}
					String UIpayto=Fileutil.GetFilevalue(var1);
					Elasticpayto=Fileutil.GetFilevalue(var2);
					System.out.println("first value:"+UIpayto);
					System.out.println("var2:"+var2);
					
				if(UIpayto.equals(Elasticpayto))
				{
				Resultflag="PASS";
				System.out.println("PASS");
				}
				else if(UIpayto.equals(var2))
				{
					Resultflag="PASS";
					StackStraceflag="Values match";
					System.out.println("Matched");
				}
				else
				{
					Resultflag="FAIL";
					StackStraceflag="Values do not match with elastic search";
				}
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;	
					
			case "GetElasticValuesPayTo":
				try{
				Resultflag=Keywordoperations.GetElasticValuesPayTo(TestData);
				Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
				break;	
				
			case "DOWNLOADPSHLOGS":
				try{
				Resultflag=Keywordoperations.GetPSHLogs(TestData);
		        System.out.println("File downloaded:"+Resultflag);
		        log.info("File downloaded:"+Resultflag);
				}
				catch (Exception e) {
					
					Resultflag="FAIL";
					System.out.println("File not downloaded:"+Resultflag);
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;	
			case "ReadPSHLOG":
				try{
				Resultflag=Keywordoperations.ReadPSHLogs(TestData);
		        System.out.println("MATCHED:"+Resultflag);
		        log.info("MATCHED:"+Resultflag);
				}
				catch (Exception e) {
					
					Resultflag="FAIL";
					System.out.println("No match found:"+Resultflag);
					StackStraceflag="No match found:"+e.toString();
					log.debug(e.toString());
				}
				break;	
			case "RandomClickCell":
				try {
					Keywordoperations.RandomClickCell(XPATH_NAV, ElementType, TestData, driver);
					Resultflag = "PASS";
				} catch (Exception e) {
					Resultflag = "FAIL";
					StackStraceflag = "STACKTRACE" + e.toString();
				} 
				break;
			case "DynamicClick":
				try{
					if (TestData.contains("VAR_"))
						TestData = fileutil.GetFilevalue(TestData);
					
					XPATH_NAV=	XPATH_NAV.replace("VAR", TestData);
					System.out.println(XPATH_NAV);
					
					JavascriptExecutor js = (JavascriptExecutor) driver;
					
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));
					
					
					//driver.findElement(By.xpath(XPATH_NAV));
					 Resultflag="PASS";
					
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
				}
			
			break;
			case "SendKeys":
				try{
					Keywordoperations.SendSequenceofKeys(XPATH_NAV,TestData,driver);
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
			case "SaveFile":

				try
				{    //driver.findElement(By.xpath(XPATH_NAV));

				JavascriptExecutor js = (JavascriptExecutor) driver;

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));
				     Robot robot = new Robot();
				     robot.setAutoDelay(250);
				     robot.mouseMove(100,200);  
				     robot.keyPress(KeyEvent.VK_ALT);
				     Thread.sleep(1000);
				     robot.keyPress(KeyEvent.VK_S);
				     robot.keyRelease(KeyEvent.VK_ALT);
				     robot.keyRelease(KeyEvent.VK_S);
				}
				catch (AWTException e)
				{
				    e.printStackTrace();
				    log.debug(e.toString());
				}
				break; 
				case "EnterKeyword":
				try{
					Robot r = new Robot();
					r.keyPress(KeyEvent.VK_ENTER);
					r.keyRelease(KeyEvent.VK_ENTER);				
					Resultflag="PASS";
				}
				catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
				}
				break;
				
				case "RespRebulkACH":
					try{

					System.out.println("respRebulk_CSV WILL BE LAUNCHED");
					log.info("ACH respRebulk_CSV WILL BE LAUNCHED");
					Action_Keywords.respRebulk_CSV_ACH(TestCaseNo,TestData);

					Resultflag="PASS";
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString());
					}
					break;
					
				case "RespRebulkEFT":
					try{

					System.out.println("respRebulk_CSV WILL BE LAUNCHED");
					log.info("EFT respRebulk_CSV WILL BE LAUNCHED");
					Action_Keywords.respRebulk_CSV_EFT(TestCaseNo,TestData);

					Resultflag="PASS";
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString(),e.getCause());
					}
					break;
					
				case "RespRebulkSAR":
					try{

					System.out.println("respRebulk_CSV WILL BE LAUNCHED");
					log.info("SAR respRebulk_CSV WILL BE LAUNCHED");
					Action_Keywords.respRebulk_CSV_SAR(TestCaseNo,TestData);

					Resultflag="PASS";
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString(),e.getCause());
					}
					break;
					
				case "RespRebulkCNR":
					try{

					System.out.println("respRebulk_CSV WILL BE LAUNCHED");
					Action_Keywords.respRebulk_CSV_CNR(TestCaseNo,TestData);
					log.info("CNR respRebulk_CSV WILL BE LAUNCHED");
					Resultflag="PASS";
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag="STACKTRACE"+e.toString();
					log.debug(e.toString(),e.getCause());
					}
					break;

				case "AsyncMessageVaildation":			
					try{	
					Resultflag=Keywordoperations.AsyncMessageComparison(TestData);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();	
						log.debug(e.toString());
					}			
					break;
				case "WiresMT103ACK":			
					try{	
					Resultflag=Keywordoperations.WiresMT103ACK(TestData);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}			
					break;
				case "WiresMT199":			
					try{	
					Resultflag=Keywordoperations.WiresMT199(TestData);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();	
						log.debug(e.toString());
					}			
					break;
				case "WiresMT900":			
					try{	
					Resultflag=Keywordoperations.WiresMT900(TestData);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();	
						log.debug(e.toString());
					}			
					break;
				case "IWSFeedUpload":			
					try{	
					Resultflag=Keywordoperations.IWSFeedUpload(TestData);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}			
					break;
				case "ClickManualActionQ":			
					try{	
					Resultflag=Keywordoperations.ClickManualActionQ(TestData,driver);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();	
						log.debug(e.toString());
					}			
					break;
				case "SearchAccWarehouseGow":			
					try{	
					Resultflag=Keywordoperations.SearchAccWarehouseGow(TestData,driver);	
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();	
						log.debug(e.toString());
					}			
					break;
					
				case "GOWNotification":			
					try{	
					Resultflag=Keywordoperations.GowNotification(TestData,driver);
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag=e.toString();	
						log.debug(e.toString());
					}			
					break;
				case "ValidateTxnBV":			
					try{
					HashMap<String,String> Result1=new HashMap<String,String>();   
					DBConnector dv=new DBConnector();					
					String ACTFILESTATUS=dv.GetTXNWORKITEMID();					
					
				
					 boolean flag=true;
					 if(TestData.startsWith("!"))
					 {
						 flag=false;
					 }


					 if(ACTFILESTATUS.equals(TestData) && flag)
					 {
					 Resultflag="PASS";
					 System.out.println("Actual value PASS");
					 log.info("pass");
		
					 }
					 if(!(ACTFILESTATUS.equals(TestData)) && flag)
					 {
					 Resultflag="FAIL";
					 System.out.println("Actual value FAIL");
				
					 StackStraceflag="Txn status does not match "+"Actual:"+ACTFILESTATUS;
			
					 }
					 if(flag==false)
					 {
						 String status=TestData.substring(1);
						 System.out.println("mewo:"+status);
						 if (ACTFILESTATUS.equals(status))
						 {
					 Resultflag="FAIL";
					 System.out.println("Actual value FAIL");
					 StackStraceflag="Txn status id does not match "+"Actual:"+ACTFILESTATUS;
					 log.info("fail");
				

					 }
					 else
					 {
						 Resultflag="PASS";
						 System.out.println("Actual value PASS");
						 log.info("pass");
					
					 }
					 }
					 }catch (Exception e) 
						{			
							Resultflag="FAIL";				
							StackStraceflag="File got rejected";
							log.debug(e.toString());
						}	
			
					break;
				 case "ValidateFileBV":
					 try{
					 HashMap<String,String> Result1=new HashMap<String,String>();  
					 DBConnector dv=new DBConnector();
					 Result1=dv.ValidateFileBV();
					 String ACTFILESTATUS=Result1.get("FILE_STATUS");
					 System.out.println(ACTFILESTATUS);
					 boolean flag=true;
					 if(TestData.startsWith("!"))
					 {
						 flag=false;
					 }


					 if(ACTFILESTATUS.equals(TestData) && flag)
					 {
					 Resultflag="PASS";
					 System.out.println("Actual value PASS");
					 log.info("pass");
					 Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
					 System.out.println("1");
					 }
					 if(!(ACTFILESTATUS.equals(TestData)) && flag)
					 {
					 Resultflag="FAIL";
					 System.out.println("Actual value FAIL");
					 Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
					 StackStraceflag="File status id does not match "+"Actual:"+ACTFILESTATUS;
					 System.out.println("2");
					 }
					 if(flag==false)
					 {
						 String status=TestData.substring(1);
						 System.out.println("mewo:"+status);
						 if (ACTFILESTATUS.equals(status))
						 {
					 Resultflag="FAIL";
					 System.out.println("Actual value FAIL");
					 StackStraceflag="File status id does not match "+"Actual:"+ACTFILESTATUS;
					 log.info("fail");
					 Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
					 System.out.println("3");

					 }
					 else
					 {
						 Resultflag="PASS";
						 System.out.println("Actual value PASS");
						 log.info("pass");
						 Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
						 System.out.println("4");
					 }
					 }
					 }catch (Exception e) 
						{			
							Resultflag="FAIL";				
							StackStraceflag=e.toString();
							log.debug(e.toString());
						}	
					 break;
				case "ValidateFileBVFromFileName":			
					try{
					HashMap<String,String> Result1=new HashMap<String,String>();   
					DBConnector dv=new DBConnector();					
					Result1=dv.ValidateFileBVFromFileName();					
					String ACTFILESTATUS=Result1.get("FILE_STATUS");
				
					
					if(ACTFILESTATUS.equals(TestData))
					{
						Resultflag="PASS";	
				
					}
					else
					{
						Resultflag="FAIL";	
						StackStraceflag="File status id does not match "+"Actual:"+ACTFILESTATUS;
						log.info("File status id does not match "+"Actual:"+ACTFILESTATUS);
					}
						
			
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag=e.toString();	
						log.debug(e.toString());
					}			
					break;
				case "ValidateBV":			
					try{	
					HashMap<String,String> Result1=new HashMap<String,String>();   
					DBConnector dv=new DBConnector();
					Resultflag="PASS";	
					StackStraceflag="";
					String ExceptionID="";
					String ExceptionDesc="";
					String ExternalExceptionID="";
					String ExternalExceptionDesc="";
					String varid=null;
					String[] Data = TestData.split("\\|");
					String determine="";
					if (Data.length>2) {
						ExceptionID =  Data[0];
						ExceptionDesc = Data[1];
						ExternalExceptionID = Data[2];						
						ExternalExceptionDesc = Data[3];
						varid = Data[4];
					}
					if (Data.length==2) {
						varid = Data[1];
						determine= Data[0];
					}
					
					
					String flag="N";
					if(ExternalExceptionID.equals("-"))
					{
						flag="Y";
					}
					Result1=dv.ValidateBV(varid);
					String ACTFILESTATUS=Result1.get("FILE_STATUS");
					String ACEXCEPTIONID=Result1.get("EXCEPTION_ID");
					String ACEXCEPTIONDESC=Result1.get("EXCEPTION_DESC");
					String ACEXTEXCEPTIONID=Result1.get("EXT_EXCEPTION_ID");
					String ACEXTEXCEPTIONDESC=Result1.get("EXT_EXCEPTION_DESC");
					System.out.println("here3");
					if(determine.equals("NA"))
					{
						Resultflag="FAIL";	
						StackStraceflag=ACEXCEPTIONID+"|"+ACEXCEPTIONDESC+"|"+ACEXTEXCEPTIONID+"|"+ACEXTEXCEPTIONDESC;
					
						break;
					}
			
					if(ACEXCEPTIONID.equals(ExceptionID))
					{
			
					}
					else
					{
						Resultflag="FAIL";	
						StackStraceflag="Exception ID mismatch:"+ACEXTEXCEPTIONID;
					}
						if(ACEXCEPTIONDESC.equals(ExceptionDesc))
						{
						}
						else
						{
							Resultflag="FAIL";	
							StackStraceflag=StackStraceflag+"\n"+"Exception desc Mismatch:"+ACEXTEXCEPTIONDESC;
						}
							if(((ACEXTEXCEPTIONID== null)&&flag.equals("Y")) ||ACEXTEXCEPTIONID.equals(ExternalExceptionID))
							{
							}
							else
							{
								Resultflag="FAIL";	
								StackStraceflag=StackStraceflag+"\n"+"External Exid Mismatch:"+ACEXTEXCEPTIONID;
							}
								if(((ACEXTEXCEPTIONDESC==null)&&flag.equals("Y"))||ACEXTEXCEPTIONDESC.equals(ExternalExceptionDesc))
								{
								
								}
								else
								{
									Resultflag="FAIL";	
									StackStraceflag=StackStraceflag+"\n"+"External Exdesc Mismatch:"+ACEXTEXCEPTIONDESC;
								}
							
					
					}
		
					
					catch (Exception e) 
					{			
						Resultflag="FAIL";	
						e.printStackTrace();
						StackStraceflag=e.toString();
						log.debug(e.toString());
					}			
					break;	
				case "ValidateBVwithClause":			
					try{	
					HashMap<String,String> Result1=new HashMap<String,String>();   
					DBConnector dv=new DBConnector();
			
					String ExceptionID=null;
					String ExceptionDesc=null;
					String ExternalExceptionID=null;
					String ExternalExceptionDesc=null;
					String Clause = null;
					String varid=null;
					
					
					if (TestData != null) {
						String[] Data = TestData.split("\\|");
					
						ExceptionID =  Data[0];
						ExceptionDesc = Data[1];
						ExternalExceptionID = Data[2];						
						ExternalExceptionDesc = Data[3];
						Clause = Data[4];
						varid = Data[5];
					}
					String flag="N";
					if(ExternalExceptionID.equals("-"))
					{
						flag="Y";
					}
					Result1=dv.ValidateBVwithClause(varid,Clause);
					String ACTFILESTATUS=Result1.get("FILE_STATUS");
					System.out.println("RHS File Status is :"+ACTFILESTATUS);
					String ACEXCEPTIONID=Result1.get("EXCEPTION_ID");
					System.out.println("RHS Exception ID is :"+ACEXCEPTIONID);
					String ACEXCEPTIONDESC=Result1.get("EXCEPTION_DESC");
					System.out.println("RHS Exception Desc is :"+ACEXCEPTIONDESC);
					String ACEXTEXCEPTIONID=Result1.get("EXT_EXCEPTION_ID");
					System.out.println("RHS Ext Exception ID is :"+ACEXTEXCEPTIONID);
					String ACEXTEXCEPTIONDESC=Result1.get("EXT_EXCEPTION_DESC");
					System.out.println("RHS Ext Exception Desc is :"+ACEXTEXCEPTIONDESC);
					System.out.println("here3");
			
						if(ACEXCEPTIONID.equals(ExceptionID))
						{
							System.out.println("Internal Exception ID passed");
							if(ACEXCEPTIONDESC.equals(ExceptionDesc))
							{
								System.out.println("Internal Exception Desc passed");
								if(((ACEXTEXCEPTIONID== null)&&flag.equals("Y")) ||ACEXTEXCEPTIONID.equals(ExternalExceptionID))
								{
									System.out.println("Ext Exception ID passed");
									if(((ACEXTEXCEPTIONDESC==null)&&flag.equals("Y"))||ACEXTEXCEPTIONDESC.equals(ExternalExceptionDesc))
									{
										Resultflag="PASS";	
										System.out.println("Ext Exception Desc passed");
									}
									else
									{
										Resultflag="FAIL";	
										StackStraceflag="External Desc does not match "+"Actual:"+ACEXTEXCEPTIONDESC;
									}
								}
								else
								{
									Resultflag="FAIL";	
									StackStraceflag="External id does not match "+"Actual:"+ACEXTEXCEPTIONID;
								}
							}
							else
							{
								Resultflag="FAIL";	
								StackStraceflag="Internal Exception desc does not match "+" Actual:"+ACEXCEPTIONDESC;
							}
						}
						else
						{
							Resultflag="FAIL";	
							StackStraceflag="Exception id does not match "+"Actual:"+ACEXCEPTIONID;
						}
					}
		
					
					catch (Exception e) 
					{			
						Resultflag="FAIL";	
						e.printStackTrace();
						StackStraceflag=e.toString();			
					}			
					break;	
				case "GetBatchWorkitemid":			
					try{	
					HashMap<String,String> Result1=new HashMap<String,String>();   
					DBConnector dv=new DBConnector();
					Result1=dv.GetBatchWorkitemid("VAR_PSHFILEWORKITEMID");
					if(Result1.get("BatchStatus").equals(TestData))
					{	
					Resultflag="PASS";	
					log.info("pass");
					}
					else
					{
						Resultflag="FAIL";	
						StackStraceflag="Batch Status does not match. Actual is:"+Result1.get("BatchStatus");
						log.info("Batch Status does not match. Actual is:"+Result1.get("BatchStatus"));
					}
					
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag=e.toString();
						log.debug(e.toString());
					}			
					break;	
					
				case "VerifyAttribute":
					try{
					boolean b=driver.findElement(By.xpath(XPATH_NAV)).isSelected();
					if(b)
					{
					Resultflag="PASS";
					System.out.println("IS SELECTED:"+Resultflag);
					log.info("IS SELECTED:"+Resultflag);
					}
					else
					{	Resultflag="FAIL";
					}
					
					}
				catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
				break;
				
				case "TriggerPSH-CC2BVMAPI":
					try{
						log.info("cc2 BVM call started");
						String status=Keywordoperations.PSHCCC2BVMAPI(TestCaseNo,TestData);
						if(status=="PASS")
						{
						Resultflag="PASS";
						}
						else
						{Resultflag="FAIL";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;
				case "TriggerPSH-CC2-FILEBVMAPI":
					try{
						log.info("cc2 BVM call started");
						String status=Keywordoperations.PSHCCC2BVMAPI(TestCaseNo,TestData);
						if(status=="FAIL")
						{
						Resultflag="PASS";
						}
						else
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;	
				case "FGTUpload":
					try{
						log.info("FGT Upload");	
						FGTExecution fe= new FGTExecution();
						fe.runFGT(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;	
				case "FGTDataMigration":
					try{
						log.info("FGT data migration");	
						FGTExecution fe= new FGTExecution();
						fe.runDataMigration(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "GetFilename":
					try{
						log.info("GetFilename");	
						FGTExecution fe= new FGTExecution();
						String filee =fe.GetFilename(TestCaseNo);
						if(filee!=null)
						{	
						Resultflag="PASS";
						StackStraceflag=filee;}
						else
						{
							Resultflag="FAIL";
							StackStraceflag="Filename not found in file_proc_opn check if file got uploaded correctly or txn stuck in physical file details1";	
						}
						Thread.sleep(1000);
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "SearchClientFileID":
					try{
					PSH_Inqcontroller.SearchClientFileID(driver,TestData,XPATH_NAV);
					 Resultflag="PASS";
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
				break;	
				case "GetFileWorkitemid":
					try{
						log.info("GetFilename");	
						FGTExecution fe= new FGTExecution();
						Resultflag=fe.GetFileWorkitemid();
						if(Resultflag.equalsIgnoreCase("PASS"))
						{
						StackStraceflag=Fileutil.GetFilevalue("VAR_PSHFILEWORKITEMID");
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
						}
						else{
							StackStraceflag="File Rejected by system";
							Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						}
					Thread.sleep(1000);
					}catch (Exception e) {
						Resultflag="FAIL";
						Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
						StackStraceflag="No fileworkiemid available in system";
						log.debug("No fileworkiemid available in system");
					}
					break;
				case "GetCurrentFileWorkitemid":
					try{
						log.info("GetFilename");	
						FGTExecution fe= new FGTExecution();
						fe.GetCurrentFileWorkitemid(TestData);
						StackStraceflag=Fileutil.GetFilevalue("VAR_PSHFILEWORKITEMID");
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "GetTxnWorkitemid":
					try{
						System.out.println("GetTxnWorkitemid");
						DBConnector fe=new DBConnector();	
						String FileworkitemiId=Fileutil.GetFilevalue("VAR_PSHFILEWORKITEMID");
						String status=fe.GetTXNWORKITEMID();
						if(status.equalsIgnoreCase(TestData))
						{
						StackStraceflag=Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
						Resultflag="PASS";
						}
					
					Thread.sleep(1000);
					}catch (Exception e) {
						Resultflag="FAIL";
						
						StackStraceflag="No txnworkiemid available in system";
						log.debug("No txnworkiemid available in system");
					}
					break;
				case "AsyncAction":
					try{
						log.info("GetFilename");	
						WiresAckMT103MT199.AsyncAction(TestData);

						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "GetallFileWorkitemids":
					try{
						log.info("GetFilename");	
						FGTExecution fe= new FGTExecution();
						fe.GetallFileWorkitemids();
						StackStraceflag=Fileutil.GetFilevalue("VAR_PSHFILEWORKITEMID");
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;
				case "DOTR":
					try{
						log.info("DOTR");
						String Custid="17000003";
						String txnrefnum1="BULK2";
						String txnrefnum2="NA";
						XmlParserDOTRDFTR2 fe= new XmlParserDOTRDFTR2();
						if(TestData!=null)
						{
							String[] Data = TestData.split("\\|");
							Custid=Data[0];
							txnrefnum1=Data[1];
							txnrefnum2=Data[2];
						}
						fe.runDOTR(Custid, txnrefnum1, txnrefnum2);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "DFTR":
					try{
						log.info("DFTR");
						String Custid="17000003";
						String txnrefnum1="BULK1";
						String Action="INFO-CANCELLED-1";
						XmlParserDOTRDFTR2 fe= new XmlParserDOTRDFTR2();
						if(TestData!=null)
						{
							String[] Data = TestData.split("\\|");
							Custid=Data[0];
							txnrefnum1=Data[1];
							Action=Data[2];
						}
						fe.runDFTR(Custid, txnrefnum1, Action);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "EMTACK":
					try{
						log.info("EMTACK");
						String txnrefnum="BULK2";
						String txnrefnum1="BULK1";
						String Action="0-0-0";
						XmlParserDOTRDFTR2 fe= new XmlParserDOTRDFTR2();
						if(TestData!=null)
						{
							String[] Data = TestData.split("\\|");
							txnrefnum=Data[0];
							txnrefnum1=Data[1];
							Action=Data[2];
						}
						fe.runACK(txnrefnum, txnrefnum1, Action);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
				case "ExecutePreQueries":
					try{
						log.info("ExecutePreQueries");	
						DBConnector fe= new DBConnector();
				        fe.PREExecute_QueriesCC2(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
					
				case "SWIFTFINMSG":
					try{
						System.out.println("Executing swiftfinmsg");	
						WiresAckMT103MT199 fe= new WiresAckMT103MT199();
				        fe.mt101Resp(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
					
				case "ACKMESSAGE":
					try{
						log.info("ExecutePreQueries");	
						WiresAckMT103MT199 fe= new WiresAckMT103MT199();
				        fe.ackMessagePSR(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;		
				case "InsertHoliday":
					try{
						log.info("ExecutePreQueries");	
						DBConnector fe= new DBConnector();
				        fe.InsertHoliday(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;	
					
				case "SystemParam":
					try
					{
						driver.switchTo().defaultContent();
					driver.switchTo().frame("SearchFrame").switchTo().frame("IwTopFrame").switchTo().frame("IwEntityTopFrame").switchTo().frame("IwMiddleFrame");
					 // System.out.println("Navigated to frame with element "+ "SearchFrame");	     
					  JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
						String currentFrame = (String) jsExecutor.executeScript("return self.name");
						System.out.println("Navigatd to frame:"+currentFrame); 
						//driver.findElement(By.id("P524235589-328392175")).sendKeys("EMT_BATCH_MAX_AMOUNT");
						((JavascriptExecutor) driver).executeScript("arguments[0].value='"+"EMT_BATCH_MAX_AMOUNT"+"';", driver.findElement(By.id("P524235589-328392175")));
		       
		    } catch (Exception e) {
		    	System.out.println("Unable to navigate to frame with element: "+e.toString());
		    	Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				log.debug(e.toString());
		    } 
				case "SystemParamOut":
					try
					{

						JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
						String currentFrame = (String) jsExecutor.executeScript("return self.name");
						System.out.println("CurrentFrame in systemparamout:"+currentFrame); 
						driver.switchTo().parentFrame().switchTo().frame("IwBottomFrame");
						String currentFrame1 = (String) jsExecutor.executeScript("return self.name");
						System.out.println("CurrentFrame in systemparamout1:"+currentFrame1); 
						
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//table[@id='buttonPanel']//span[contains(text(),'Search')]")));
						driver.switchTo().parentFrame().switchTo().parentFrame().switchTo().frame("IwEntityBottomFrame");
						String currentFrame2 = (String) jsExecutor.executeScript("return self.name");
						System.out.println("CurrentFrame in systemparamout2:"+currentFrame2); 
						Thread.sleep(5000);
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//tbody[@id='tbbody']//a[contains(text(),'CONFIGURATION')]")));
						Thread.sleep(5000);
						String parent_handle= driver.getWindowHandle();
						System.out.println(parent_handle);
						
						Set<String> Windows = driver.getWindowHandles();
						System.out.println("handle:"+Windows.toString());
						//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@src='images/indeximages/Login_button.gif']")));
						for (String win : Windows) {
							driver.switchTo().window(win);
							System.out.println(driver.getTitle());
							if ("IntellectFlow".equalsIgnoreCase(driver.getTitle())) {
								System.out.println("Automation Begins!!"+driver.getTitle());
								driver.switchTo().window(win);
								break;
							}
						}
						driver.switchTo().defaultContent();
						driver.switchTo().frame("IwMiddleFrame");

						((JavascriptExecutor) driver).executeScript("arguments[0].value='"+1000000+"';", driver.findElement(By.id("P524235589-312501775")));	
						
						driver.switchTo().parentFrame().switchTo().frame("IwBottomFrame");
						String currentFrame3 = (String) jsExecutor.executeScript("return self.name");
						System.out.println("CurrentFrame in systemparamout3:"+currentFrame3); 
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//div[@class='ST186']//span[contains(text(),'Save')]")));
							
		    } catch (Exception e) {
		    	System.out.println("Exception"+e.toString());
		    	Resultflag="FAIL";
				StackStraceflag="STACKTRACE"+e.toString();
				log.debug(e.toString());
		    } 
			
					break;	
				case "CheckGOWStatus":
					try{
						log.info("CheckGOWStatus");	
						String custrefnum=null;
						String expectedstatus=null;
						if(TestData!=null)
						{
							String[] Data = TestData.split("\\|");
							custrefnum=Data[0];
							expectedstatus=Data[1];
						}
						DBConnector fe= new DBConnector();
				        String status=fe.CheckGOWStatus(custrefnum);
				        if(expectedstatus.equals(status))
				        {Resultflag="PASS";}
				        else
				        {Resultflag="FAIL";}	
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;		
				case "BulkStatusCheck":
					DataComparision DC = new DataComparision();
					try{
					String returns=null;
					String[] Data = TestData.split("\\|");
					String TYPE=Data[0];
					returns = DC.performPrePostcomp(TYPE);
					if(returns.equalsIgnoreCase("MATCH"))
					{
					Resultflag="PASS";
					Keywordoperations.StoreValue("VAR_CHECKFLAG", "PASS");
					}
					else
					{
					Resultflag="FAIL";
					Keywordoperations.StoreValue("VAR_CHECKFLAG", "FAIL");
					StackStraceflag = returns;
					}
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag=e.toString();
					log.debug(e.toString());
					}
					break;
				case "PrePostStatusCheck":
					DataComparision DC2 = new DataComparision();
					try{
					String returns=null;
					String[] Data = TestData.split("\\|");
					String TYPE=Data[0];
					returns = DC2.PrePostcompUsingFullQuery(TYPE);
					if(returns.equalsIgnoreCase("MATCH"))
					{
					Resultflag="PASS";
					}
					else
					{
					Resultflag="FAIL";
					StackStraceflag = returns;
					}
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag=e.toString();
					log.debug(e.toString());
					}
					break;
				case "DBConnectorsetDate":
					try{
					System.out.println("Data from file"+TestData);
					DBConnector DBConnector2=new DBConnector();
					int i=DBConnector2.Set_PSH_Dates(TestData);

					if(i>=1)
					{
					Resultflag="PASS";
					}
					}
					catch (Exception e) {
					Resultflag="FAIL";
					System.out.println("result fail");
					StackStraceflag="STACKTRACE"+e.toString();
					}
					break;
				case "BulkStatusCheckBVM":
					DataComparision DC1 = new DataComparision();
					try{
					String returns=null;
					String[] Data = TestData.split("\\|");
					String TYPE=Data[0];
					returns = DC1.performPrePostcompBVM(TYPE);
					if(returns.equalsIgnoreCase("MATCH"))
					{
					Resultflag="PASS";
					}
					else
					{
					Resultflag="FAIL";
					StackStraceflag = returns;
					}
					}
					catch (Exception e) {
					Resultflag="FAIL";
					StackStraceflag=e.toString();
					}
					break;
				case "TriggerPSH-BTR-Structure-BVMAPI":
					try{
						log.info("cc2 BVM call started");
						String status=Keywordoperations.PSHBTRBVMAPI2(TestCaseNo,TestData);
						if(status=="PASS")
						{
						Resultflag="PASS";
						}
						else
						{Resultflag="FAIL";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;	
				case "TriggerPSH-BTR-FILEBVMAPI":
					try{
						log.info("cc2 BVM call started");
						String status=Keywordoperations.PSHBTRBVMAPI(TestCaseNo,TestData);
						if(status=="FAIL")
						{
						Resultflag="PASS";
						}
						else
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;
					
				case "SwitchToAnyWindow":
					try {
						Object[] strAllWindows = driver.getWindowHandles().toArray();
						Set<String> window = driver.getWindowHandles();
						log.info("total no of windows:-->" + window.size());
						System.out.println("total no of windows:-->" + window.size());

						Iterator<String> itr = window.iterator();
						while (itr.hasNext())
						{
							driver.switchTo().window(itr.next());
							if (TestData.equalsIgnoreCase(driver.getTitle()))
							{

								break;
							}
						}

						log.debug("You are on right window" + driver.getTitle());
						System.out.println("You are on right window" + driver.getTitle());
						Resultflag="PASS";
					}
					catch (Exception ex)
					{
						Resultflag="FAIL";
						log.fatal("Exception in Nfocusonwindow: ", ex);
						System.out.println("Exception in Nfocusonwindow: "+ ex);
						throw ex;
					}
					break;
				case "PageRefresh":
					try {
						driver.navigate().refresh();
						driver.manage().window().maximize();
						Resultflag="PASS";
					}
					catch(Exception ex)
					{
						Resultflag="FAIL";	
						StackStraceflag=ex.getLocalizedMessage();
					}
					break;
				case "SearchClientID":			
					try{	
					Keywordoperations.SearchClientID(XPATH_NAV,TestData,driver);	
					Resultflag = "PASS";
					}catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag="STACKTRACE"+e.toString();		
						log.debug(e.toString());
					}			
					break;
				
				case "SwitchingFrames":
					try{
						
						driver.switchTo().frame(TestData);
						JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
						String currentFrame = (String) jsExecutor.executeScript("return self.name");
						System.out.println("Navigatd to frame:"+currentFrame); 
						Resultflag="PASS";
					}
					
					catch (Exception e) {
				    	System.out.println("Unable to navigate to frame with element: "+e.toString());
				    	Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
				    } 
					break;
				case "switchToFramesByName":
					try {
						driver.switchTo().defaultContent();
						StringTokenizer frameTokens = new StringTokenizer(TestData, "#");
						while (frameTokens.hasMoreTokens())
						{
							driver.switchTo().frame(frameTokens.nextToken());

						}
						Resultflag="PASS";
						JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
						String currentFrame = (String) jsExecutor.executeScript("return self.name");
						System.out.println("CurrentFrame in systemparamout:"+currentFrame); 
					}
					catch(Exception ex)
					{
						Resultflag="FAIL";	
						StackStraceflag=ex.getLocalizedMessage();
						log.debug(ex.getMessage());
					}
					break;
				case "EnterTab":
					try{
						Robot r = new Robot();
						r.keyPress(KeyEvent.VK_TAB);
						r.keyRelease(KeyEvent.VK_TAB);				
						Resultflag="PASS";
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
						log.debug(e.toString());
					}
					break;
				case "SelectByVisibleText":
					try{
					Keywordoperations.SelectByVisibleText(XPATH_NAV,TestData,driver);
					Resultflag="PASS";
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;
					
				case "FGTBatchExecution":
					try{
						FGTBatchExecution f=new FGTBatchExecution();
						String[] Data = TestData.split("\\|");
						Keywordoperations.StoreValueg("db_suite",Data[2]);
						f.fgtexecute(Data[0],Data[1],Data[2]);
						
					Resultflag="PASS";
					}
					catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;
				case "RandomNumber":
					try {
						String[] Data = TestData.split("\\|");
						String Flag=Data[0];
						String numLength = Data[1];
						int _NumLength = Integer.parseInt(numLength);
						String VAR_MESSAGEID = Data[2];
						String result = Keywordoperations.getRandomNumber(Flag,_NumLength,VAR_MESSAGEID);
						Keywordoperations.SendKeys(XPATH_NAV,result,driver);
						Resultflag="PASS";
						}
					catch(Exception e){
						Resultflag="FAIL";
						StackStraceflag=e.toString();
					}
					break;
				case "ClickButtonAcceptAlert":
					try{
						//Thread.sleep(3000);
						WebDriverWait wait = new WebDriverWait(driver, 30);
						
						WebElement clickable = wait.until(
						        ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));
						clickable.click();
						//driver.findElement(By.xpath(XPATH_NAV)).click();
						/*JavascriptExecutor js = (JavascriptExecutor) driver;

						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));*/
						System.out.println("clicked on delete button");
						Thread.sleep(5000);
						
						Alert alert = driver.switchTo().alert();
						alert.accept();
						Thread.sleep(2000);
						Alert alert2 = driver.switchTo().alert();
		               // String alertText = alert.getText();
		               // System.out.println("Alert data: " + alertText);
		                alert2.accept();
						
						}
					catch (Exception e) {
							Resultflag="FAIL";
							StackStraceflag="STACKTRACE"+e.toString();
						}
					break;
				case "GetFilenamethroughTestData":
					try{
						log.info("GetFilename");	
						FGTExecution fe= new FGTExecution();
						fe.GetFilenamethroughTestData(TestData);
					
						{Resultflag="PASS";}
					}catch (Exception e) {
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE"+e.toString();
					}
					break;	
				case "SimpleAlertAccept":
					try{
					     Alert alert = driver.switchTo().alert();		
					      Thread.sleep(3000);
					        // Capturing alert message.    
					       String alertMessage= driver.switchTo().alert().getText();		
					        		
					        // Displaying alert message		
					      System.out.println(alertMessage);	
					           // Accepting alert		
					      alert.accept();
					      { Resultflag="PASS";}
						}
					catch (Exception e)
					{
						Resultflag="FAIL";
						StackStraceflag="STACKTRACE:"+e.toString();
			
					}
					break;
				case "GetCorrFeeDetails":			
					try{	
					HashMap<String,String> Result1=new HashMap<String,String>();   
					DBConnector dv=new DBConnector();
					Result1=dv.GetCorrFeeDetails();
					String TxnCorrFileStatus1=null;  
					String TxnCorrFeeStatusCd1=null;  
					String TxnCorrFeeStatusDesc1=null; 
					String TxnCorrStatusCd1=null;  
					String TxnCorrStatusDesc1=null; 
					String TxnCorrFileStatus=Result1.get("TxnCorrFileStatus");  
					String TxnCorrFeeStatusCd=Result1.get("TxnCorrFeeStatusCd"); 
					String TxnCorrFeeStatusDesc=Result1.get("TxnCorrFeeStatusDesc"); 
					String TxnCorrStatusCd=Result1.get("TxnCorrStatusCd");  
					String TxnCorrStatusDesc=Result1.get("TxnCorrStatusDesc");  
					
					if (TestData != null) {
						String[] Data = TestData.split("\\|");
					
						TxnCorrFileStatus1 =  Data[0];
						TxnCorrFeeStatusCd1 = Data[1];
						TxnCorrFeeStatusDesc1 = Data[2];
						TxnCorrStatusCd1 = Data[3];
						TxnCorrStatusDesc1 = Data[4];
					}
		
					
			
						if(TxnCorrFileStatus1.equals(TxnCorrFileStatus))
						{
							if(TxnCorrFeeStatusCd1.equals(TxnCorrFeeStatusCd))
							{
								if(TxnCorrFeeStatusDesc1.equals(TxnCorrFeeStatusDesc))
								{
									if(TxnCorrStatusCd1.equals(TxnCorrStatusCd))
									{
										if(TxnCorrStatusDesc1.equals(TxnCorrStatusDesc))
										{
											Resultflag="PASS";	
										}
										else
										{
											Resultflag="FAIL";	
											StackStraceflag="TxnCorrStatusDesc does not match "+"Actual:"+TxnCorrStatusDesc;
										}
										
									}
								
									else
									{
										Resultflag="FAIL";	
										StackStraceflag="TxnCorrStatusCd does not match "+"Actual:"+TxnCorrStatusCd;
									}
								}
								else
								{
									Resultflag="FAIL";	
									StackStraceflag="TxnCorrFeeStatusDesc does not match "+"Actual:"+TxnCorrFeeStatusDesc;
								}
							}
							else
							{
								Resultflag="FAIL";	
								StackStraceflag="TxnCorrFeeStatusCd does not match "+"Actual:"+TxnCorrFeeStatusCd;
							}
						}
						else
						{
							Resultflag="FAIL";	
							StackStraceflag="TxnCorrFileStatus does not match "+"Actual:"+TxnCorrFileStatus;
						}
					}
		
					
					catch (Exception e) 
					{			
						Resultflag="FAIL";				
						StackStraceflag=e.toString();			
					}			
					break;			
		
			default:
				System.out.println("No function found");
				break;
			}
			}
			}
			catch (Exception e)
			{
				Resultflag="FAIL";
				StackStraceflag="STACKTRACE:"+e.toString();
	
				
			}
			if(Resultflag.equalsIgnoreCase("PASS"))
			{
				logger.log(LogStatus.PASS,KeywordOperation, StackStraceflag);
			}
			if(Resultflag.equalsIgnoreCase("FAIL"))
			{
				logger.log(LogStatus.FAIL, KeywordOperation, StackStraceflag);
			}
			if(Resultflag.equalsIgnoreCase("SKIPPED"))
			{
				logger.log(LogStatus.SKIP,KeywordOperation,StackStraceflag);
			}
			
			Result.put(String.valueOf(k), Resultflag);
			FileUtilities fileutil= new FileUtilities();
			fileutil.UpdateResult(String.valueOf(k),Result,suite);
			
			Result_StackStrace.put(String.valueOf(k), StackStraceflag);
			fileutil.StackTraceResult(String.valueOf(k),Result_StackStrace,suite);
			
		
	}
}
		//catch (Exception e)
	//	{
		//e.printStackTrace();
			
		//}
		finally
		{
			connection.close();
			extent.endTest(logger);
		}
		return extent;
	}
}
	
			
