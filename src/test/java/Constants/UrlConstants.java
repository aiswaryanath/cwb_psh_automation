package Constants;

import java.io.InputStream;
import java.util.Properties;

public class UrlConstants {

	private static UrlConstants instance = new UrlConstants();
	static Properties properties;

	private UrlConstants() {
		init();
	}

	public static UrlConstants getInstance() {
		return instance;
	}

	private void init() {

		try {
			InputStream inputStream = UrlConstants.class.getClassLoader().getResourceAsStream("constants.properties");

			properties = new Properties();
			properties.load(inputStream);

		} catch (Exception e) {
			//e.printStackTrace();
			try {
				InputStream inputStream = UrlConstants.class.getClassLoader().getResourceAsStream("resources/constants.properties");

				properties = new Properties();
				properties.load(inputStream);

			} catch (Exception ex) {
				ex.printStackTrace();
				
			}
		}

	}

	// properties from file
	public static String Login_api;
	public static String Add_api;
	public static String Modifi_api;
	public static String Inquiry_api;
	public static String Delete_api;
	public static String _Excel_TestData;
	public static String _OUTPT_json_console;
	
	static {
		Login_api = properties.getProperty("Login_api");
		Add_api = properties.getProperty("Add_api");
		Modifi_api = properties.getProperty("Modifi_api");
		Inquiry_api = properties.getProperty("Inquiry_api");
		Delete_api = properties.getProperty("Delete_api");
		
		//File Path
		_Excel_TestData  = properties.getProperty("_Excel_TestData");
		_OUTPT_json_console  = properties.getProperty("_OUTPT_json_console");
		
	}
}
